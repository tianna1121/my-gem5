% plotCPIStack.m
function accuAllCycles = plotCPIStack(sMatsI, FileName, FileNum, maxFile)
defines

[rowMat, colMat] = size(sMatsI);
%divider = ceil(rowMat / 3);

accuSampleSMatsI = [];
SerialStall = sMatsI(:,i_SerialStall)./sMatsI(:,i_NumCycles);
StructStall = (sMatsI(:,i_RenameROBFull)+sMatsI(:,i_IQFull)+sMatsI(:,i_LQFull)+sMatsI(:,i_SQFull))./sMatsI(:,i_NumCycles);
Others      = sMatsI(:,i_NumCycles)./sMatsI(:,i_NumCycles) -sMatsI(:,i_SerialStall)./sMatsI(:,i_NumCycles) - StructStall;
AllCycles   = [SerialStall, StructStall, Others];

%for i = 0:length(sMatsI)/divider-1
%    accuAllCycles(i+1,:) = sum(AllCycles(i*divider+1:(i+1)*divider,:));
%end

accuAllCycles = sum(AllCycles',2)./length(AllCycles);

