% Input parameters:
% 1. FileName: the filename of selected benchmark. for example: singleCore-default-bbench
% 2. ModelName: the selected analytical model. for example: TOCS09, ISPASS11, mech+ANN and pureANN
% 3. mostExecutedContext: the most executed N threads. for example: 4
% 4. accuracy: the accuracy of model. for example: detailed, 1in10, 1in20...
% 5. startPoints: 
% 6. points:
% Demo1: CPUAnaModel('singleCore-a15+prefetch-basicmath', 'TOCS09', 1, 'detailed',200, 7000)
% Demo2: CPUAnaModel('singleCore-a15-bbench', 'mech+ANN', 1, 'detailed', 200, 1500)
% Demo3: CPUAnaModel('singleCore-a15-bbench', 'thesis', 1, 'detailed', 200, 1500)
% 2014-01-17 v1.0: zhangyang, startup
% 2014-02-21 v2.0: zhangyang, combine several configurations
% 2014-04-06 v3.0: zhangyang, add PCA analysis, for singleCore only 
% 2014-06-28 v4.0: zhangyang, add the MLP, pCD and avgMem prediction according to ISPASS11
% 2014-09-01 v4.1: zhangyang, add results plot for paper 
% 2015-01-05 v4.2: zhangyang, Fix some bugs, and add the nlinfit and NNukf model
% 2015-01-17 v4.3: zhangyang, Change some params of Model
% 2015-01-19 v4.4: zhangyang, Add new nlinfit model to regress different params independently
% 2015-02-28 v4.5: zhangyang, Add Microarch dependent data analysis

function simuTime = CPUAnaModel(FileName, ModelName, mostExecutedContext, accuracy, startPoints, points)
% Define some macroes
DEBUG         = 1;
ANN_ENABLED   = 1;
PLOT_ENABLED  = 1;

% Declear some global vars
pAdjust  = 10000;                                                                   % Exetime<1/pAdjust points
training = 1000;                                                                    % ANN training points
t1=clock;
format shortg;
PreFix = pwd;
sp = startPoints;
ep = startPoints+points;

%set(0, 'defaultUicontrolFontName', 'Tomano');
%set(0, 'defaultUitableFontName',   'Tomano');
%set(0, 'defaultAxesFontName',      'Tomano');
%set(0, 'defaultTextFontName',      'Tomano');
%set(0, 'defaultUipanelFontName',   'Tomano');

% Open simulation output file, and importdata to Matlab workspace
if (~DEBUG)
  FilePath= strcat(PreFix, '/newStats-', FileName, '.mat');
  if (exist(FilePath, 'file'))
      load(FilePath);
  else
      strcat(PreFix, '/newStats-', FileName, '.txt')
      rawData = importdata(strcat(PreFix, '/newStats-', FileName, '.txt'));
      save(FilePath, 'rawData');
  end
else
  strcat(PreFix, '/newStats-', FileName, '.txt');
  rawData = importdata(strcat(PreFix, '/newStats-', FileName, '.txt'));
end

% Raw data processing
[rowRawData, colRawData] = size(rawData);                                           % The size of rawData
i_PID = colRawData - 2;

% Add the duration ticks of consecutive threads into the rawData
durationTicks = 10000;                                                              % First thead exe 1ticks
for ii = 2:rowRawData
    durationTicks(1,ii) = rawData(ii,end) - rawData(ii-1,end);                      % Add the duration ticks
    if(DEBUG)
        if(rawData(ii,end)-rawData(ii-1, end) < 0)
            fprintf('S');
        end
    end
end
rawData(:,end+1) = durationTicks';
if(DEBUG)
    for ii=2:length(rawData)
        if (rawData(ii,end-1)-rawData(ii,end) > rawData(ii-1,end-1)) fprintf('O'); end
    end
end

% Sort data based on thread's PIDs
sortedData = sortrows(rawData, [i_PID, i_PID+1]);                                   % Sorting data
[~, I] = unique(sortedData(:, i_PID));                                              % Find the first row 
[rowI, ~] = size(I);

% Declare the CPU Hardware Microarchitectural Parameters
pFEDp = 6;                                                                          % Frontend Pipeline depth 
pFEWd = 3;                                                                          % Backend Pipeline width
pCDR = 20; MLP = 8; avgMem = 400; pSerial = 40;                                     % Estimated params
ROBEntry = 40;

% Microarchitecture parameters, include L1 cache/Branch/TLB/structure hazards
paramMatC = { ...
    'L1ICacheHit'; 'L1ICacheMiss'; 'L1ICMSHRHit'; 'L1ICMSHRMiss';
    'L1DCacheHit'; 'L1DCacheMiss'; 'L1DCMSHRHit'; 'L1DCMSHRMiss';
    'CommittedInsts'; 'CommitSquashedInsts'; 
    'NumCycles'; 'IdleCycles'; 'IPC_Total';
    'BrMiss';
    'ITLBHit'; 'ITLBMiss'; 'DTLBHit'; 'DTLBMiss';
    'CommitLoads'; 'CommitMembars'; 'CommitBranches'; 'CommitFPInst'; 'CommitIntInsts';
    'IntRFReads'; 'IntRFWrites'; 'MISCRFReads'; 'MISCRFWrites';
    'RenameROBFull'; 'IQFull'; 'LQFull'; 'SQFull'; 'FullRegisterEvents'; 'SerialStall'
    };

% FORMAT: i_L1ICacheHit
[m,n] = size(paramMatC);
for j=1:m
    eval(['i_', paramMatC{j}, ...
       '=find(not(cellfun(''isempty'', strfind(paramMatC,''', paramMatC{j},'''))));']);
end

pMatMicro = [ ...
    1, 12, 1, 1, 2, 12, 1, 1, ... % L1ICacheHit, L1ICacheMiss, L1ICMSHRHit, L1ICMSHRMiss, L1DCacheHit, L1DCacheMiss, L1DCMSHRHit, L1DCMSHRMiss
    1, 1, 1, 1, 1, ...      % CommittedInst, CommitSquashedInsts, NumCycles, IdleCycles, IPC_Total
    1, ...                  % BrMiss
    1, 12, 1, 12, ...       % ITLBHit, ITLBMiss, DTLBHit, DTLBMiss
    1, 1, 1, 1, 1, ...      % CommitLoads, CommitMembars, CommitBranches, CommitFPInst, CommitIntInsts
    1, 1, 1, 1, ...         % IntRFReads, IntRFWrites, MiscRFReads, MiscRFWrites
    1, 1, 1, 1, 1, 1, ...      % RenameROBFull, IQFull, LQFull, SQFull, FullRegisterEvents, SerialCycle
    ];
[~, colM] = size(pMatMicro);

% Instruction parameters, include the latency of different instruction
paramMatI = { ...
    'No_OpClass'; 'IntAlu'; 'IntMult'; 'IntDiv';
    'FloatAdd'; 'FloatCmp'; 'FloatCvt'; 'FloatMult'; 'FloatDiv'; 'FloatSqrt';
    'SimdAdd'; 'SimdAddAcc'; 'SimdAlu'; 'SimdCmp'; 'SimdCvt'; 'SimdMisc';
    'SimdMult'; 'SimdMultAcc'; 'SimdShift'; 'SimdShiftAcc'; 'SimdSqrt';
    'SimdFloatAdd'; 'SimdFloatAlu'; 'SimdFloatCmp'; 'SimdFloatCvt'; 'SimdFloatDiv';
    'SimdFloatMisc'; 'SimdFloatMult'; 'SimdFloatMultAcc'; 'SimdFloatSqrt';
    'MemRead'; 'MemWrite'; 'IprAccess'; 'InstPrefetch'; 'serialInst'
    };

[m,n] = size(paramMatI);
for j=1:m
    eval(['i_', paramMatI{j}, ...
       '=find(not(cellfun(''isempty'', strfind(paramMatI,''', paramMatI{j},'''))))+colM;']);
end

pMI = [ ...
    1, 1, 3, 12, ...        % No_OpClass, IntAlu, IntMult, IntDiv
    5, 5, 5, 4, 9, 33, ...  % FloatAdd, FloatCmp, FloatCvt, FloatDiv, FloatSqrt
    4, 4, 4, 4, 3, 3, ...   % SimdAdd, SimdAddAcc, SimdAlu, SimdCmp, SimdCvt, SimdMisc
    5, 5, 3, 3, 9, ...      % SimdMult, SimdMultAcc, SimdShift, SimdShiftAcc, SimdSqrt
    5, 5, 3, 3, 3, ...      % SimdFloatAdd, SimdFloatAlu, SimdFloatCmp, SimdFloatCvt, SimdFloatDiv
    3, 3, 3, 9, ...         % SimdFloatMisc, SimdFloatMult, SimdFloatMultAcc, SimdFloatSqrt
    2, 2, 3, 1, 1, ...      % MemRead, MemWrite, IprAccess, InstPrefetch, serialInst
    ];
[~, colI] = size(pMI);

% System parameters include L2 caches and memory access
paramMatSys = { ...
    'L2CacheHit'; 'L2CacheMissAll'; 'L2CacheMissInst'; 'L2CacheMissData';
    'L2CacheMSHRMissInst'; 'L2CacheMSHRMissPre'; 'L2CacheMSHRMissData';
    'L2CacheMissILatency'; 'L2CacheMissDLatency';
    'PHYMEMRead'; 'PHYMEMWrite'
    };

[m,n] = size(paramMatSys);
for j=1:m
    eval(['i_', paramMatSys{j}, ...
       '=find(not(cellfun(''isempty'', strfind(paramMatSys,''', paramMatSys{j},'''))))+colM+colI;']);
end

pMatSys = [ ...
    1, 1, 1, 1, ...         % L2CacheHit, L2CacheMissAll, L2CacheMissInst, L2CacheMissData
    1, 1, 1, ...            % L2CacheMSHRMissInst, L2CacheMSHRMissPre, L2CacheMSHRMissData
    1, 1, ...               % L2CacheMissInstLatency, L2CacheMissDataLatency
    1, 1, ...               % PHYMEMRead, PHYMEMWrite
    ];

pMat = [pMatMicro, pMI, pMatSys];

% Split the raw data into different sMatsI, according to thread's pid num.
threadContextSwitchNums = [];
for i = 1 : (rowI-1)
    threadContextSwitchNums(i,1:2) = [I(i+1)-I(i), sortedData(I(i), i_PID)];
end
threadContextSwitchNums(end+1,1:2) = [I(end)-I(i), sortedData(I(end), i_PID)];      % Need fix this

sortedContextSwitch = sortrows(threadContextSwitchNums, [-1]);
selectedThread = sortedContextSwitch(1:mostExecutedContext, :);

for i = 1:(rowI-1)
if(find(I(i+1)-I(i) == selectedThread(:,1)))
    sMatsI = sortedData(I(i):I(i+1),:);                                             % Thread's matrix 

    PID                 = sMatsI(1, end-3);
    Duration            = sMatsI(:, end);
    [~, i_Duration]     = size(sMatsI);
    i_Ticks             = i_Duration - 1;

    iNum     = sum(sMatsI(:,i_No_OpClass : i_InstPrefetch),  2);                    % All Inst.
    iNumALU  = sum(sMatsI(:,i_IntAlu     : i_IntDiv),        2);                    % ALU Inst.
    iNumFP   = sum(sMatsI(:,i_FloatAdd   : i_FloatSqrt),     2);                    % FP Inst.
    iNumSIMD = sum(sMatsI(:,i_SimdAdd    : i_SimdFloatSqrt), 2);                    % SIMD Inst.
    iNumMEM  = sum(sMatsI(:,i_MemRead    : i_MemWrite),      2);                    % MEM Inst.
    iNumNOP  = sMatsI(:,i_No_OpClass);                                              % NOP Inst.
    iNumPRE  = sMatsI(:,i_InstPrefetch);                                            % PREFETCH Inst.
    iNumIPR  = sMatsI(:,i_IprAccess);                                               % IPRAcess Inst.

    sMatsI(:,end+1) = sum(sMatsI(:,i_No_OpClass:i_serialInst)*pMI',2)/pFEWd;        % Base1: weighted    
    sMatsI(:,end+1) = sMatsI(:, i_L1ICacheMiss) .* pMat(i_L1ICacheMiss);            % L1ICache miss penalty
    sMatsI(:,end+1) = sMatsI(:, i_ITLBMiss) .* pMat(i_ITLBMiss);                    % L1ITLB miss penalty
    sMatsI(:,end+1) = sMatsI(:, i_DTLBMiss) .* pMat(i_DTLBMiss);                    % D1ITLB miss penalty
    
    switch ModelName
    case 'PCA'
        figure;
        boxplot(sMatsI(:,i_L1ICacheHit:i_SQFull),...
            paramMatC(i_L1ICacheHit:i_SQFull,1));
        grid on;
        [rowSel, ~] = size(sMatsI(:,i_L1ICacheHit:i_SQFull));
        stdr = std(sMatsI(:,i_L1ICacheHit:i_SQFull));
        sr = sMatsI(:,i_L1ICacheHit:i_SQFull)./repmat(stdr, rowSel, 1);
        [coef,score,latent,t2] = princomp(sr);
        % t2:329*1
        % 多元统计距离,记录的是每一个观察量到中心的距离
        % 通过latent,可以知道提取前几个主成分就可以了.
        figure;
        percent_explained = 100*latent/sum(latent);
        pareto(percent_explained);
        xlabel('Principal Component');
        ylabel('Variance Explained (%)');
    case 'TOCS09'
        sMatsI(:,end+1) = (pFEWd-1)/2/pFEWd .*((sMatsI(:,i_L1ICacheMiss)) ...
            + sMatsI(:, i_L2CacheMissData) ...
            + sMatsI(:, i_BrMiss) ...
            + sMatsI(:, i_L2CacheMissInst) / MLP);                              % Base2：dispatch ineff
        sMatsI(:,end+1) = sMatsI(:, i_L2CacheMissInst)*avgMem/MLP;              % L2ICache miss penalty
        sMatsI(:,end+1) = sMatsI(:, i_L2CacheMissData)*avgMem/MLP;              % L2DCache miss penalty
        sMatsI(:,end+1) = sMatsI(:, i_BrMiss).*(pCDR+pFEDp);                    % Branch miss penalty
        sMatsI(:,end+1) = sMatsI(:, i_IdleCycles);                              % Idle Cycle
        sMatsI(:,end+1) = sMatsI(:, i_SerialStall);                             % Serial Cycle
        sMatsI(:,end+1) = sum(sMatsI(:,i_RenameROBFull:i_SQFull),2);            % Stucture hazards
        sMatsI(:,end+1) = sum(sMatsI(:,end-10:end),2);                          % Total Cycles
       
        % Plot the overhead introduced by structure hazards
        figure(); 
        bar([sMatsI(:,i_RenameROBFull), sMatsI(:,i_IQFull), ...
          sMatsI(:,i_LQFull), sMatsI(:,i_SQFull)], 'stacked');
        title('Structure Hazards: ROB/IQ/LQ/SQ'); 
        
        % Delete several simulation points that have very large cycles
        adjust=find(sMatsI(:,i_NumCycles) < mean(sMatsI(:,i_NumCycles))/pAdjust);

        % Calculate the CPI and compare to the simulation results
        GEM5_CPI   = sMatsI(1:startPoints,i_NumCycles)./iNum(1:startPoints,:);
        TOCS09_CPI = sMatsI(1:startPoints,end)./iNum(1:startPoints,:);

        figure(); plot(GEM5_CPI, 'b'); hold on; plot(TOCS09_CPI, 'r'); title('SIMU(b) vs TOCS09(r)');

        % Plot the Calculated CPIStacks of the selected simulation points with GEM5 CPI's
        temp_results=[sMatsI(startPoints:startPoints+points,end-11:end-1)]; 
        figure(); bar(temp_results ./repmat(iNum(startPoints:startPoints+points,:),1,11), 'stacked');
        legend('Base1', 'L1ICache', 'L1ITLB', 'L1DTLB','Base2','L2ICache', ...
          'L2DCache', 'MissPredict', 'Idle', 'SerialInsts','SHazards');
        hold on; plot(1000./(sMatsI(startPoints:startPoints+points,i_IPC_Total)),'*-');
        title('The Calculated CPIStacks of selected simulation points with GEM5 CPI''s.');
        
        csvwrite('test.csv', sMatsI(adjust,end-11:end-1)./repmat(iNum(adjust,:),1,11));
        corrCoef = corrcoef(sMatsI(adjust,end), sMatsI(adjust,7))

    case 'ISPASS11'
        sMatsI(:,end+1) = (pFEWd-1)/2/pFEWd .*((sMatsI(:,i_L1ICacheMiss)) + ...
          sMatsI(:,i_L2CacheMissData) + ...
          sMatsI(:,i_L1ICacheMiss) + ...
          sMatsI(:, i_BrMiss)/MLP);                                             % Base2：dispatch ineff
        sMatsI(:,end+1) = sMatsI(:, i_L2CacheMissInst)*avgMem/MLP;              % L2ICache miss penalty
        sMatsI(:,end+1) = sMatsI(:, i_L2CacheMissData)*avgMem/MLP;              % L2DCache miss penalty
        sMatsI(:,end+1) = sMatsI(:, i_BrMiss).*(pCDR+pFEDp);                    % Branch miss penalty
        sMatsI(:,end+1) = sMatsI(:, i_IdleCycles);                              % Idle Cycle
        sMatsI(:,end+1) = sMatsI(:, i_SerialStall);                             % Serial Cycle
        sMatsI(:,end+1) = sum(sMatsI(:,i_RenameROBFull:i_SQFull),2)             % Structure Hazards
        sMatsI(:,end+1) = sum(sMatsI(:,end-10:end),2);                          % Total Cycles
        
        % Delete several simulation points that have very large cycles
        adjust=find(sMatsI(:,i_NumCycles) < mean(sMatsI(:,i_NumCycles))/pAdjust);

        TOCS09_CPI = sMatsI(1:startPoints,end)./iNum(1:startPoints,:);
        GEM5_CPI   = sMatsI(1:startPoints,i_NumCycles)./iNum(1:startPoints,:);
    
        % Regression the pCDR, MLP and cStall
        % pCDR = b1 .* (max(ROBEntry, 1/mpubr)).^b2 * (1+b3*fp) .* (1+b4*mpuDL1)

        1.*(sMatsI(:,i_BrMiss)./iNum)
        %sMatsI(:,i_BrMiss)
        %iNum
        %1./(sMatsI(:,i_BrMiss)./iNum)
        pCDR = ['b(1)'...
          '.*(max(ROBEntry,1/(sMatsI(:,i_BrMiss)./iNum))).^b(2)'...
          '*(1+b(3).*iNumFP./iNum)'...
          '.*(1+b(4).*sMatsI(:,i_L1DCacheMiss)./iNum)'];

        % MLP = b5 .* (mpuDL2).^b6 .* (mpuDTLB).^b7
        MLP = ['(b(5)'...
          '.*((sMatsI(:,i_L2CacheMissData)./iNum).^b(6))'...
          '.*((sMatsI(:,i_DTLBMiss)./iNum).^b(7)))'];

        % cStall = max(0, 1-cMiss./(iNum/D)+b8*(1+b9*fp)*(1+b10*mpuDL1))
        cM = '100';
        cStall_t = ['(b(8)'...
          '.*(1+b(9) .* iNumFP./iNum)'...
          '.*(1+b(10).*(sMatsI(:,i_L1DCacheMiss)./iNum)))'];
        cStall = strcat('(max(0,1-',cM,'./(iNum/pFEDp+',cStall_t,')).*',cStall_t,')');
        
        % Use function points to avoid multiple functions 
        beta0 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]';                                % Init the paramters 
        pDispatch=['(pFEWd-1)/2/pFEWd.*((sMatsI(:,i_L1ICacheMiss))'...
          '+sMatsI(:,i_L2CacheMissData)'...
          '+sMatsI(:,i_BrMiss)'...
          '+sMatsI(:,i_L2CacheMissInst)./'];                    % MLP);         % Base2: Dispatch ineff
        pL2I  = 'sMatsI(:,i_L2CacheMissInst).*avgMem./';        % MLP;          % L2ICache miss penalty
        pL2D  = 'sMatsI(:,i_L2CacheMissData).*avgMem./';        % MLP;          % L2DCache miss penalty
        pBr   = 'sMatsI(:,i_BrMiss).*(pFEDp+';                  % pCDR);        % Branch miss penalty
        pIdle = 'sMatsI(:,i_IdleCycles)';                                       % Idle Cycle
        pSerl = 'sMatsI(:,i_serialInst)';                                       % Serial Cycles
                                                                % cStall        % Stucture hazards
        eval(['[b,r,j,covb,mse] = nlinfit(sMatsI, sMatsI(:,i_NumCycles), @(b,m)', ...
            pDispatch,MLP,')+',pL2I,MLP,'+',pL2D,MLP,'+',pBr,pCDR,')+',...
            pIdle, '+', pSerl, '+', cStall,',beta0);']);
        b                                                                       % Show b1~b10 params 

        % Use the regressioned b1~b10 params to reCalculate the CPIStack 
        MLP = b(5).*((sMatsI(:,colM+colI+4)./iNum).^b(6)).*((sMatsI(:,14)./iNum).^b(7));
        pCDR = b(1).*(max(40,1/(sMatsI(:,10)./iNum))).^b(2)*(1+b(3).*iNumFP./iNum)* ...
            (1+b(4).*sMatsI(:,colM+colI+1)./iNum);
        
        sMatsI(:,end+1) = (pFEWd-1)/2/pFEWd.*((sMatsI(:,2))+sMatsI(:,  ...
            (colM+colI+3))+sMatsI(:,10)+sMatsI(:, (colM+colI+2))./MLP);         % Base2: Dispatch ineff
        sMatsI(:,end+1) = sMatsI(:,(colM+colI+3))*avgMem./MLP;                  % L2ICache
        sMatsI(:,end+1) = sMatsI(:,(colM+colI+4))*avgMem./MLP;                  % L2DCache
        sMatsI(:,end+1) = sMatsI(:,8);                                          % Idle Cycles
        sMatsI(:,end+1) = sMatsI(:,colM+35);                                    % Serial Cycles
        sMatsI(:,end+1) = sMatsI(:,10) .* (pCDR+pFEDp);                         % Branch miss penalty
        sMatsI(:,end+1) = eval(['(max(0,1-',cM,'./(iNum/pFEDp+',cStall_t,')).*',cStall_t,')']);
        sMatsI(:,end+1) = sum(sMatsI(:,end-10:end), 2);                         % Total cycles
        
        adjust=find(sMatsI(:,7) > mean(sMatsI(:,7))/pAdjust);                   % Delete too small points
        
        ISPASS11_CPI = sMatsI(1:startPoints,end)./iNum(1:startPoints,:);
        bar3([GEM5_CPI, TOCS09_CPI, ISPASS11_CPI], .8, 'detached');
        colormap([1 0 0;0 1 0;0 0 1]);
        
        figure(); bar(sMatsI(1:startPoints,end-10:end-1)./repmat(iNum(1:startPoints,:),1,10), 'stacked');
        legend('Base1','L1ICache Miss','L1ITLB','L1DTLB','dispatch','L2ICache','L2DCache', ...
          'MissPrediction','Idle','Serial','Conflicts');
        corrCoef = corrcoef(sMatsI(:,end), sMatsI(:,7))

    case 'mech+ANN'
        sMatsI(sp:ep,end+1) = (pFEWd-1)/2/pFEWd.*((sMatsI(sp:ep,i_L1ICacheMiss)) ...
          + sMatsI(sp:ep, i_L2CacheMissData) ...
          + sMatsI(sp:ep, i_BrMiss) ...
          + sMatsI(sp:ep, i_L2CacheMissInst)/MLP);                              % Base2: Dispatch ineff
        sMatsI(sp:ep,end+1) = sMatsI(sp:ep,i_L2CacheMissInst)*MLP/MLP;          % L2ICache miss penalty
        sMatsI(sp:ep,end+1) = sMatsI(sp:ep,i_L2CacheMissData)*avgMem/MLP;       % L2DCache miss penalty
        sMatsI(sp:ep,end+1) = sMatsI(sp:ep,i_BrMiss).*(pCDR+pFEDp);             % Branch miss penalty
        sMatsI(sp:ep,end+1) = sMatsI(sp:ep,i_IdleCycles);                       % Idle cycle
        sMatsI(sp:ep,end+1) = sMatsI(sp:ep,i_SerialStall);                      % Serial Cycle
        sMatsI(sp:ep,end+1) = sum(sMatsI(sp:ep,i_RenameROBFull:i_SQFull),2);    % Structure hazards
        sMatsI(sp:ep,end+1) = sum(sMatsI(sp:ep,end-10:end), 2);                 % Total Cycles

        if(PLOT_ENABLED)
            figure();
            tooSmallCycles    = [0; 0];                                         % Init tooSmallCycles
            heightOfRectangle = (sMatsI(:,end-11:end-1)+0.001)./repmat(iNum,1,11);
            cumsumYaxis       = cumsum(heightOfRectangle, 2);
            Yaxis             = cumsumYaxis - heightOfRectangle;
            Xaxis             = sMatsI(:,i_Ticks) - Duration;
            simuYaxis         = sMatsI(:,i_NumCycles)./iNum;

            %for k = 1:length(sMatsI(sp:ep,:))-1
            for k = sp:ep -1
                if(sMatsI(k, i_NumCycles) > mean(sMatsI(:,i_NumCycles)/pAdjust))
                    for j = 1:11
                        plotRec = rectangle('position', [...
                            Xaxis(k), ...                                       % X axis
                            Yaxis(k,j), ...                                     % Y axis
                            Duration(k),...                                     % delta X (width)
                            heightOfRectangle(k,j)]);                           % delta Y (height)
                        set(plotRec, 'FaceColor', [0.07*(j+3), 0.07*j, 0.06*(j+1)]);  % fill color
                    end
                    if (DEBUG) 
                        if(Duration(k) < 0) fprintf('D'); end
                        if(sMatsI(k+1,i_Ticks)-Duration(k+1) < sMatsI(k,i_Ticks)) 
                            fprintf('M\n'); 
                        end
                    end
                else tooSmallCycles(:,end+1) = [k, Duration(k)];
                end
            end
            hold on;

            if(DEBUG)
                fprintf(strcat('Average ticks of contexts are:', num2str(mean(Duration(k))), '.'));
                fprintf(strcat('These ticks are less than 1/', num2str(pAdjust),'. Omitted!\n'));
                tooSmallCycles(:,:)
            end

            plot(Xaxis(sp:ep), simuYaxis(sp:ep), 'r*-'); hold on;
            title(strcat('Nlinfited Model''CPIStack vs Gem5''CPI(r), PID = ', num2str(PID)));
        end

        % Show the corrcoef between the Mechanics Model and Gem5 simulation
        weightOfCPI          = sMatsI(:, end)./sum(sMatsI(:, i_NumCycles), 2); 
        weightedMechCPI      = sMatsI(:, end)             ./iNum .* weightOfCPI;
        weightedGem5CPI      = sMatsI(:, i_NumCycles) ./iNum .* weightOfCPI;
        weightedCorrCoef     = corrcoef(weightedMechCPI, weightedGem5CPI)
        
        fprintf('meanOfwMechCPI meanOfwGem5CPI stdOfwMechCPI stdOfwGem5CPI\n');
        outputMech = [ mean(weightedMechCPI), mean(weightedGem5CPI), ... 
                       std(weightedMechCPI),  std(weightedGem5CPI)];
        disp(outputMech);
        
        % Now use ANN to improve the model's accuracy
        if(ANN_ENABLED)
            % Delete several simulation points that have very large cycles
            adjust=find(sMatsI(:,i_NumCycles) > mean(sMatsI(:,i_NumCycles))/pAdjust);

	        testR = 0;  
	        level1Node = 0; 
	        level2Node = 0;
	        level3Node = 0;

            % Construct the P/T Matrix: P->input training data; T->target params
            trainingData = adjust(1:training, 1);                               % size(adjust): 2189:1
            Ps = sMatsI(trainingData, end-11:end-1) ./repmat(iNum(trainingData,:),1,11);
            Ts = sMatsI(trainingData, i_NumCycles)./iNum(trainingData,:);

            [~, Pcol] = size(Ps);
            Px = sMatsI(adjust, end-11:end-1)./repmat(iNum(adjust,:),1,11);
            Tx = sMatsI(adjust, i_NumCycles)./iNum(adjust,:);

            mt = max(max(Ps)); nt = max(max(Ts));                               % Find the max in Ps/Ts
            mx = max(max(Px)); nx = max(max(Tx));                               % Find the max in Px/Tx  
            Pt = Ps'/mt; Tt = Ts'/nt;                                           % Normalize Pt/Tt
            Px = Px'/mx; Tx = Tx'/nx; 
            pr(1:Pcol,1)=0; pr(1:Pcol,2)=1;                                     % the input params scope
	        size(pr)
            warning off all                                                     % Turnoff the waring info

	        while( abs(testR-1) > 0.2 )
	          % samplePoints = samplePoints + 1
	          level1Node = level1Node + 1;
	          level2Node = level2Node + 1;
	          level3Node = level3Node + 1;

              % Construct the Neural Network Model
              %bpnet=newff(pr,[1 1],{'purelin', 'purelin'}, 'traingdx', 'trainbfg');
              bpnet = newff(pr,[level1Node level2Node 1],...
                {'logsig', 'logsig', 'purelin'}, 'trainlm');

              bpnet.trainParam.epochs     = 3000;                               % Set the max training step
              bpnet.trainParam.goal       = 1e-5;                               % Set the target error
              bpnet.trainParam.show       = 100;                                % Show results in xx step
              bpnet.trainParam.lr         = 0.15;                               % Set the learning params
              bpnet.trainParam.showWindow = false;                              % Open/Close the graphics
              %matlabpool open;
              [bpnet]=train(bpnet,Pt,Tt, 'useParallel', 'yes');                 % Start training
              % bpnet.iw{1,1};                                                  % weight of hidden layer
              % bpnet.b{1};                                                     % hidden layer's thresh
              % bpnet.iw{2,1};                                                  % weight of output layer
              % bpnet.b{2};                                                     % output params
              %matlabpool close;

              R=(sim(bpnet,Px))'*nx;                                            % Simulation Process
              weightedR = R .* weightOfCPI;
	          testR = mean(weightedR./weightedGem5CPI)
	        end

            % Plot the results
            figure(); plot(R,'b*-'); hold on;                                   % mech+ANN's CPIStack
            plot(sMatsI(adjust,i_NumCycles)./iNum(adjust,:),'r'); hold on;      % Gem5's CPIStack
            plot(sMatsI(adjust, end)./iNum(adjust,:),'go-');                    % MechModel's CPIStack
            title('Compare the Gem5(red), mech+ANN(blue) and mech(green)');

            corrCoef_MechANN = corrcoef(R, sMatsI(adjust,i_NumCycles)./iNum(adjust,:))
            figure(); bar(sMatsI(adjust,end-11:end-1)./repmat(iNum(adjust,:),1,11), 'stacked');
            figure(); bar(sMatsI(startPoints:startPoints+points,end-10:end-1)...
              ./repmat(iNum(startPoints:startPoints+points,:),1,10), 'stacked');
            legend('Base1','L1ICache Miss','L1ITLB','L1DTLB','dispatch','L2ICache','L2DCache', ...
                'MissPrediction','Idle','Serial','Conflicts');
        end

    case 'pureANN'
        P=[sMatsI(:,i_L1ICacheMiss), ...
           sMatsI(:,i_BrMiss), ...
           sMatsI(:,i_ITLBMiss), ...
           sMatsI(:,i_DTLBMiss), ...
           sMatsI(:,i_L2CacheMissInst), ...
           sMatsI(:,i_L2CacheMissData)];

        % Delete several simulation points that have very large cycles
        adjust=find(sMatsI(:,i_NumCycles) < mean(sMatsI(:,i_NumCycles))/pAdjust);
        P = P(adjust,:)./repmat(iNum(adjust,:),1,6);
        T = sMatsI(adjust,i_NumCycles)./iNum(adjust,:);
        
        [~, Pcol] = size(P);
        m=max(max(P)); n=max(max(T));                                           % Find the max P/T
        P=P'/m; T=T'/n;                                                         % Normalize the P/T
        pr(1:Pcol,1)=0; pr(1:Pcol,2)=1;                                         % the input params scope
        warning off all                                                         % Turnoff the waring info

        % Construct the Neural Network Model
        bpnet=newff(pr,[20 1],{'purelin', 'purelin'}, 'traingdx', 'trainbfg');
        bpnet.trainParam.epochs=10000;                                          % Set the max training step
        bpnet.trainParam.goal=1e-5;                                             % Set the target error
        bpnet.trainParam.show=100;                                              % Show results in xx step
        bpnet.trainParam.lr=0.05;                                               % Set the learning params
        bpnet.trainParam.showWindow = false;                                    % Open/Close the Graphics
        
        [bpnet]=train(bpnet,P,T);                                               % Start training
        r=sim(bpnet,P); R=r'*n;                                                 % Simulation Process
        
        % Plot the results
        figure(); plot(R); hold on; plot(sMatsI(adjust,7)./iNum(adjust,:),'r');
        corrCoef_pureANN = corrcoef(R, sMatsI(adjust,7)./iNum(adjust,:))

    case 'testNlinfit'
        myfunc = @(X, c) (( ...
            sum(sMatsI(sp:ep,i_No_OpClass:i_serialInst)*pMI',2)/pFEWd  ... 
                + (pFEWd-1)/2/pFEWd .*((sMatsI(sp:ep,i_L1ICacheMiss)) ...
                + sMatsI(sp:ep, i_L2CacheMissData) ...
                + sMatsI(sp:ep, i_BrMiss) ...
                + sMatsI(sp:ep, i_L2CacheMissInst) / abs(X(1,1)))       + ...
            sMatsI(sp:ep, i_L1ICacheMiss).*pMat(i_L1ICacheMiss)                 + ...
            sMatsI(sp:ep, i_ITLBMiss).*pMat(i_ITLBMiss)                 + ...
            sMatsI(sp:ep, i_DTLBMiss).*pMat(i_DTLBMiss)                 + ...
            sMatsI(sp:ep, i_L2CacheMissInst)*abs(X(1,3))/abs(X(1,1))    + ...
            sMatsI(sp:ep, i_L2CacheMissData)*abs(X(1,3))/abs(X(1,1))    + ...
            sMatsI(sp:ep, i_BrMiss).*(X(1,2)+pFEDp)                     + ...
            sMatsI(sp:ep, i_IdleCycles)                                 + ...
            sMatsI(sp:ep, i_SerialStall)                                + ...
            sum(sMatsI(sp:ep, i_RenameROBFull:i_SQFull),2))./iNum(sp:ep,:));    % Total Cycles

        c = sMatsI(sp:ep, i_NumCycles);
        p0 = [10,30,400];
        options = optimset('TolFun', 1e-1, 'TolX', 1e-3);

        [beta,r,j,covb,mse]=nlinfit(eye,sMatsI(sp:ep,i_NumCycles)./iNum(sp:ep,:),myfunc,p0,options);
        beta

        figure();
        plot(sMatsI(sp:ep,i_NumCycles)./iNum(sp:ep,:), 'b'); hold on;
        plot(myfunc(beta, c), 'r'); hold on;
        title(strcat('nlinfited MLP:', num2str(beta(1)), ', pCDR:', num2str(beta(2)), ...
            ', avgMem:',num2str(beta(3)),'. Gem5(b), Nlinfit(r)'));

        sMatsI(sp:ep,end+1) = (pFEWd-1)/2/pFEWd.*((sMatsI(sp:ep,i_L1ICacheMiss)) ...
          + sMatsI(sp:ep, i_L2CacheMissData) ...
          + sMatsI(sp:ep, i_BrMiss) ...
          + sMatsI(sp:ep, i_L2CacheMissInst)/beta(1));                          % Base2: Dispatch ineff
        sMatsI(sp:ep,end+1) = sMatsI(sp:ep,i_L2CacheMissInst)*beta(3)/beta(1);  % L2ICache miss penalty
        sMatsI(sp:ep,end+1) = sMatsI(sp:ep,i_L2CacheMissData)*beta(3)/beta(1);  % L2DCache miss penalty
        sMatsI(sp:ep,end+1) = sMatsI(sp:ep,i_BrMiss).*(beta(2)+pFEDp);          % Branch miss penalty
        sMatsI(sp:ep,end+1) = sMatsI(sp:ep,i_IdleCycles);                       % Idle cycle
        sMatsI(sp:ep,end+1) = sMatsI(sp:ep,i_SerialStall);                      % Serial Cycle
        sMatsI(sp:ep,end+1) = sum(sMatsI(sp:ep,i_RenameROBFull:i_SQFull),2);    % Structure hazards
        sMatsI(sp:ep,end+1) = sum(sMatsI(sp:ep,end-10:end), 2);                 % Total Cycles

        if(PLOT_ENABLED)
            figure();
            tooSmallCycles    = [0; 0];                                         % Init tooSmallCycles
            heightOfRectangle = (sMatsI(:,end-11:end-1)+0.001)./repmat(iNum,1,11);
            cumsumYaxis       = cumsum(heightOfRectangle, 2);
            Yaxis             = cumsumYaxis - heightOfRectangle;
            Xaxis             = sMatsI(:,i_Ticks) - Duration;
            simuYaxis         = sMatsI(:,i_NumCycles)./iNum;

            %for k = 1:length(sMatsI(sp:ep,:))-1
            for k = sp:ep -1
                if(sMatsI(k, i_NumCycles) > mean(sMatsI(:,i_NumCycles)/pAdjust))
                    for j = 1:11
                        plotRec = rectangle('position', [...
                            Xaxis(k), ...                                       % X axis
                            Yaxis(k,j), ...                                     % Y axis
                            Duration(k),...                                     % delta X (width)
                            heightOfRectangle(k,j)]);                           % delta Y (height)
                        set(plotRec, 'FaceColor', [0.07*(j+3), 0.07*j, 0.06*(j+1)]);  % fill color
                    end
                    if (DEBUG) 
                        if(Duration(k) < 0) fprintf('D'); end
                        if(sMatsI(k+1,i_Ticks)-Duration(k+1) < sMatsI(k,i_Ticks)) 
                            fprintf('M\n'); 
                        end
                    end
                else tooSmallCycles(:,end+1) = [k, Duration(k)];
                end
            end
            hold on;

            if(DEBUG)
                fprintf(strcat('Average ticks of contexts are:', num2str(mean(Duration(k))), '.'));
                fprintf(strcat('These ticks are less than 1/', num2str(pAdjust),'. Omitted!\n'));
                tooSmallCycles(:,:)
            end

            plot(Xaxis(sp:ep), simuYaxis(sp:ep), 'r*-'); hold on;
            title(strcat('Nlinfited Model''CPIStack vs Gem5''CPI(r), PID = ', num2str(PID)));
        end

        % fprintf('Most of high CPI context may severely influenced by serial inst.');
        CorrCoef = corrcoef(myfunc(beta,c),sMatsI(sp:ep,i_NumCycles)./iNum(sp:ep,:))
        errMeanRate = mean((myfunc(beta,c)-sMatsI(sp:ep,i_NumCycles)./iNum(sp:ep,:))./myfunc(beta,c))

    case 'thesis'
        SERIALCYCLES  = 0;
        IDLECYCLES    = 0;
        STRUCTHAZARDS = 0;
        INSTMIX       = 0;
        CACHE         = 0;
        TLB           = 0;
        BRANCH        = 0;
        MSHR          = 1;

        if(SERIALCYCLES)
            TRAINING = 0;

            sumOfSerial = sum(sMatsI(sp:ep,i_SerialStall))./sum(sMatsI(sp:ep,i_NumCycles))

            figure();
            plot(sMatsI(sp:ep,i_SerialStall), 'r'); hold on;
            plot(sMatsI(sp:ep,i_NumCycles), 'b');
            title({strcat('Total cycles(b) and Serial cycles(r)'), ...
                strcat('Serial cycles:',num2str(sumOfSerial*100),'%')});


            if(TRAINING)
            sumOfStructHazards = sum(sMatsI(sp:ep, i_RenameROBFull:i_FullRegisterEvents),2);
            % Overhead introduced by serialize insts. with nlinfit model

            serialCycleFunc = @(X,c)( ...
                sMatsI(sp:ep, i_serialInst) .* X(1,1));                                 % SerialInst

            serialCycleFunc_all = @(X,c)( ...
                sMatsI(sp:ep, i_serialInst) .* X(1,1) ...                               % SerialInst
             .* (1+sMatsI(sp:ep, i_L1DCacheMiss)./iNum(sp:ep) .* X(1,2)) ... 
             .* (1+sMatsI(sp:ep, i_BrMiss)./iNum(sp:ep) .* X(1,3)) ... 
             .* (1+sumOfStructHazards./iNum(sp:ep) .* X(1,4)) ... 
            );
            
            c = sMatsI(sp:ep, i_SerialStall);
            p0 = [20];
            p0_all = [20, 10, 10, 10];
            options = optimset('TolFun', 1e-1, 'TolX', 1e-3);

            [beta,r,j,covb,mse]=nlinfit(eye,sMatsI(sp:ep,i_SerialStall),...
                serialCycleFunc,p0,options);
            [beta_all,r,j,covb,mse]=nlinfit(eye,sMatsI(sp:ep,i_SerialStall), ...
                serialCycleFunc_all,p0_all,options);

            results  = sMatsI(sp:ep, i_serialInst).*beta;
            corrCoef = corrcoef(sMatsI(sp:ep, i_SerialStall),results);
            meanErr  = mean((sMatsI(sp:ep,i_SerialStall)-results)./sMatsI(sp:ep,i_SerialStall));

            results_all  = serialCycleFunc_all(beta_all, c);
            corrCoef_all = corrcoef(sMatsI(sp:ep, i_SerialStall),results_all);
            meanErr_all  = mean((sMatsI(sp:ep,i_SerialStall)-results_all)./sMatsI(sp:ep,i_SerialStall));

            % plot the different of gem5 simulation and nlinfit model
            figure();
            plot(sMatsI(sp:ep, i_SerialStall), 'r'); hold on;
            plot(results_all, 'b'); hold on;
            plot(results,     'm'); hold on;
            title('SerialStall from Gem5(r), nlinfit-all(b), nlinfit(m), ANN(c)');
            text((ep-sp)/2, max(sMatsI(sp:ep, i_SerialStall))*1.02, strcat('The corrcoef:', ...
                num2str(corrCoef(1,2)),'; The meanErr:', num2str(meanErr)));
            beta
            text((ep-sp)/2, max(sMatsI(sp:ep, i_SerialStall))*0.96, strcat('The corrcoef-all:', ...
                num2str(corrCoef_all(1,2)),'; The meanErr-all:', num2str(meanErr_all)));
            beta_all

            % Overhead introduced by serialize insts. with ANN model
            tp = sp+000;
            mp = ep-500;

            P =[
                sMatsI(tp:mp, i_L1DCacheMiss), ...
                sMatsI(tp:mp, i_serialInst), ...
                sMatsI(tp:mp, i_ITLBMiss), ...
                sMatsI(tp:mp, i_CommitFPInst), ...
                sMatsI(tp:mp, i_RenameROBFull), ...
                sMatsI(tp:mp, i_IQFull), ...
                sMatsI(tp:mp, i_LQFull), ...
                sMatsI(tp:mp, i_SQFull) ...
                sMatsI(tp:mp, i_FullRegisterEvents) ...
            ];
            T = sMatsI(tp:mp, i_SerialStall);

            Ptest =[
                sMatsI(sp:ep, i_L1DCacheMiss), ...
                sMatsI(sp:ep, i_serialInst), ...
                sMatsI(sp:ep, i_ITLBMiss), ...
                sMatsI(sp:ep, i_CommitFPInst), ...
                sMatsI(sp:ep, i_RenameROBFull), ...
                sMatsI(sp:ep, i_IQFull), ...
                sMatsI(sp:ep, i_LQFull), ...
                sMatsI(sp:ep, i_SQFull) ...
                sMatsI(sp:ep, i_FullRegisterEvents) ...
            ];
            Ttest = sMatsI(sp:ep, i_SerialStall);

            [~, Pcol] = size(P);
            m=max(max(P)); n=max(max(T));                                           % Find the max P/T
            P=P'/m; T=T'/n;                                                         % Normalize the P/T
            mt=max(max(Ptest)); nt=max(max(Ttest));                                 % Find the max P/T
            Ptest=Ptest'/mt; Ttest=Ttest'/nt;                                       % Normalize the P/T


            pr(1:Pcol,1)=0; pr(1:Pcol,2)=1;                                         % the input params scope
            warning off all                                                         % Turnoff the waring info

            % Construct the Neural Network Model
            bpnet = newff(pr,[20 20 10 1],{'logsig', 'logsig','logsig', 'purelin'}, 'trainlm');
            bpnet.trainParam.epochs     = 10000;                                    % Set max training step
            bpnet.trainParam.goal       = 1e-5;                                     % Set the target error
            bpnet.trainParam.show       = 100;                                      % Show results in xx step
            bpnet.trainParam.lr         = 0.15;                                     % Set the learning params
            %bpnet.trainParam.showWindow = false                                    % Open/Close the graphics
   
            bpnet=train(bpnet,P,T);                                                 % Start training

            r=sim(bpnet,Ptest);                                                     % Simulation Process
            R=r'*nt; Ttest=Ttest'.*nt;                                              % The simulation results

            plot(R, 'c'); hold on;
            corrCoef_ANN = corrcoef(Ttest, R);
            meanErr_ANN  = mean((Ttest-R)./Ttest);
            text((ep-sp)/2, max(Ttest)*0.90, strcat('The corrcoef-ANN:', ...
                num2str(corrCoef_ANN(1,2)),'; The meanErr-ANN:', num2str(meanErr_ANN)));

            % Error analysis
            getHighSIrow = find(sMatsI(sp:ep, i_SerialStall)>10.5e6)
            highSImat = sMatsI(getHighSIrow+sp-1,:);

            coef_N=(0);
            for jj=1:length(paramMatC)
                eval(['M = corrcoef(highSImat(:,i_',paramMatC{jj}, '), highSImat(:,i_SerialStall));']);
                eval(['coef_N(end+1) = M(1,2);']);
            end
            coef_N(2:end);
            figure();
            plot(highSImat(:, i_SerialStall), 'r'); hold on;
            plot(highSImat(:, i_NumCycles), 'b'); hold on;
            
            text(4, max(highSImat(:, i_NumCycles))*0.96, ...
                strcat('The sampling points:', num2str(getHighSIrow)));
            end
        end


        % Idle cycles
        if(IDLECYCLES)
            sumOfIdle = sum(sMatsI(sp:ep,i_IdleCycles))./sum(sMatsI(sp:ep,i_NumCycles))

            figure();
            plot(sMatsI(sp:ep,i_IdleCycles), 'r'); hold on;
            plot(sMatsI(sp:ep,i_NumCycles), 'b');
            title({strcat('Total cycles(b) and Idle cycles(r)'), ...
                strcat('Idle cycles:',num2str(sumOfIdle*100),'%')});

        end

        % Structure Hazards
        if(STRUCTHAZARDS)
            meanROBFull = mean(sMatsI(sp:ep,i_RenameROBFull)./ sMatsI(sp:ep,i_NumCycles));
            meanIQFull  = mean(sMatsI(sp:ep,i_IQFull)./ sMatsI(sp:ep,i_NumCycles));
            meanLQFull  = mean(sMatsI(sp:ep,i_LQFull)./ sMatsI(sp:ep,i_NumCycles));
            meanSQFull  = mean(sMatsI(sp:ep,i_SQFull)./ sMatsI(sp:ep,i_NumCycles));
            meanREGFull = mean(sMatsI(sp:ep,i_FullRegisterEvents)./ sMatsI(sp:ep,i_NumCycles));
            meanALLFull = mean(sum(sMatsI(sp:ep,i_RenameROBFull:i_FullRegisterEvents),2) ...
                ./ sMatsI(sp:ep,i_NumCycles));

            figure();
            plot(sMatsI(sp:ep,i_RenameROBFull), 'r'); hold on;
            plot(sMatsI(sp:ep,i_IQFull), 'm'); hold on;
            plot(sMatsI(sp:ep,i_LQFull), 'y'); hold on;
            plot(sMatsI(sp:ep,i_SQFull), 'g'); hold on;
            plot(sMatsI(sp:ep,i_FullRegisterEvents), 'c'); hold on;
            plot(sum(sMatsI(sp:ep,i_RenameROBFull:i_FullRegisterEvents),2), 'k'); hold on;
            plot(sMatsI(sp:ep,i_NumCycles), 'b');

            title({strcat('TotalCycles(b),ROBFull(r),IQFull(m),LQFull(y)',...
                ',SQFull(g),RegFull(c),allStructHazards(k),PID=', num2str(PID)),...
                strcat('ROB:', num2str(meanROBFull), '%,IQ:', num2str(meanIQFull), '%;', ...
                'LQ:', num2str(meanLQFull), '%,SQ:', num2str(meanSQFull), ...
                 '%Reg:', num2str(meanREGFull), '%,ALL:', num2str(meanALLFull), '%.')});
        end

        % Instruction Mix
        if(INSTMIX)
            figure(); 
            bar([iNumALU, iNumFP, iNumSIMD, iNumMEM, iNumNOP, iNumPRE], 'stacked');
            title(strcat('ALU:', num2str(mean(iNumALU./iNum)*100), '%,', ...
                'FP:', num2str(mean(iNumFP./iNum)*100), '%,', ...
                'SIMD:', num2str(mean(iNumSIMD./iNum)*100), '%,', ...
                'MEM:', num2str(mean(iNumMEM./iNum)*100), '%,', ...
                'NOP:', num2str(mean(iNumNOP./iNum)*100), '%,', ...
                'PREFETCH:', num2str(mean(iNumPRE./iNum)*100), '%,', ...
                'IPRAcess:', num2str(mean(iNumIPR./iNum)*100), '%,' ...
                ));
        end

        % Cache 
        if(CACHE)
            L1IMissR=sMatsI(sp:ep,i_L1ICacheMiss)./sum(sMatsI(sp:ep,i_L1ICacheHit:i_L1ICacheMiss),2);
            L1DMissR=sMatsI(sp:ep,i_L1DCacheMiss)./sum(sMatsI(sp:ep,i_L1DCacheHit:i_L1DCacheMiss),2);
            L2IMissR=sMatsI(sp:ep,i_L2CacheMissInst)./sum(sMatsI(sp:ep,i_L2CacheHit:i_L2CacheMissAll),2);
            L2DMissR=sMatsI(sp:ep,i_L2CacheMissData)./sum(sMatsI(sp:ep,i_L2CacheHit:i_L2CacheMissAll),2);

            figure();
            bar([sMatsI(sp:ep,i_L1ICacheHit:i_L1DCacheMiss), ...
                sMatsI(sp:ep,i_L2CacheHit),sMatsI(sp:ep,i_L2CacheMissInst:i_L2CacheMissData)], ...
                'stacked');
            title(strcat( ...
                'L1IMiss:', num2str(mean(L1IMissR)*100), '%;', ...
                'L1DMiss:', num2str(mean(L1DMissR)*100), '%;', ...
                'L2IMiss:', num2str(mean(L2IMissR)*100), '%;', ...
                'L2DMiss:', num2str(mean(L2DMissR)*100), '%;' ...
            ));
        end

        % TLB
        if(TLB)
            ITLBMissR = sMatsI(sp:ep,i_ITLBMiss)./sum(sMatsI(sp:ep,i_ITLBHit:i_ITLBMiss),2);
            DTLBMissR = sMatsI(sp:ep,i_DTLBMiss)./sum(sMatsI(sp:ep,i_DTLBHit:i_DTLBMiss),2);

            figure();
            bar(sMatsI(sp:ep, i_ITLBHit:i_DTLBMiss), 'stacked');
            title(strcat( ...
                'ITLBMiss:', num2str(mean(ITLBMissR)*100), '%;', ...
                'DTLBMiss:', num2str(mean(DTLBMissR)*100), '%;' ...
            ));
        end

        % MSHR
        if(MSHR)
            L1ICMSHRMissR=sMatsI(sp:ep,i_L1ICMSHRMiss)./sum(sMatsI(sp:ep,i_L1ICMSHRHit:i_L1ICMSHRMiss),2);
            L1DCMSHRMissR=sMatsI(sp:ep,i_L1DCMSHRMiss)./sum(sMatsI(sp:ep,i_L1DCMSHRHit:i_L1DCMSHRMiss),2);

            figure();
            bar([sMatsI(sp:ep, i_L1ICMSHRMiss),sMatsI(sp:ep, i_L1DCMSHRMiss),...
                sMatsI(sp:ep, i_L2CacheMSHRMissInst), sMatsI(sp:ep, i_L2CacheMSHRMissPre), ...
                sMatsI(sp:ep, i_L2CacheMSHRMissData)
            ],'stacked');
            title(strcat( ...
                'L1ICMSHRMissRation:', num2str(mean(L1ICMSHRMissR)*100), '%; ', ...
                'L1DCMSHRMissRation:', num2str(mean(L1DCMSHRMissR)*100), '%; ', ...
                'L2CMSHRMissInst:', num2str(mean(sMatsI(sp:ep,i_L2CacheMSHRMissInst))), '; ', ...
                'L2CMSHRMissPre:',  num2str(mean(sMatsI(sp:ep,i_L2CacheMSHRMissPre ))), '; ',...
                'L2CMSHRMissData:', num2str(mean(sMatsI(sp:ep,i_L2CacheMSHRMissData))), '; ' ...
            ));

        end

        
    case 'testNNukf'
        % Input:
        %   theta: initial guess of MLP NN parameter. The network structure is
        %   determined by the number of parameters, ns, the number of inputs (size of
        %   x),nx and the number of output (size of y), ny. The euqation of the NN
        %   is: y = W2 * tanh( W1 * x + b1) + b2, and theta = [W1(:);b1;W2(:);b2].
        %   Therefore, ns = nx * nh + nh + nh * ny + ny, which gives the number of
        %   hidden nodes is nh = (ns - ny) / (nx + ny + 1);
        %   P: the covariance of the initial theta. Needs to be tuned to get good
        %   training performance.
        %   x and y: input and output data for training. For batch training, x and
        %   y should be arranged in such a way that each observation corresponds to 
        %   a column.
        %   Q: the virtual process covariance for theta, normally set to very small
        %   values.
        %   R: the measurement covariance, dependen on the noise level of data, tunable. 

        rand('state',0)
        randn('state',0)
        N=20;
        Ns=100;
        x=1.2*randn(N,Ns);
        y=sin(x)+0.1*randn(N,Ns);
        z=y;
        nh=4;
        ns=nh*2+nh+1;
        theta=randn(ns,1);
        P=diag([100*ones(1,nh*2) 1000*ones(1,nh+1)]);
        Q=0.0001*eye(ns);
        R=100*eye(Ns);
        % alpha=0.8;
        T1=1:N/2;
        for k=T1
            [theta,P,z(k,:)]=nnukf(theta,P,x(k,:),y(k,:),Q,R);
        end
        W1=reshape(theta(1:nh*2),nh,[]);
        W2=reshape(theta(nh*2+1:end),1,[]);
        T2=N/2+1:N;
        for k=T2
            z(k,:)=W2(:,1:nh)*tanh(W1(:,1)*x(k,:)+W1(:,2+zeros(1,Ns)))+W2(:,nh+ones(1,Ns));
        end
        subplot(211)
        plot(x(T1,:),y(T1,:),'ob',x(T1,:),z(T1,:),'.r')
        title('training results')
        subplot(212)
        plot(x(T2,:),y(T2,:),'ob',x(T2,:),z(T2,:),'.r')
        title('testing results')
       
    case 'paper'
        %%%%%% TOCS09 part: Mechanic Model%%%%%%%%%%%
        sMatsI(:,end+1) = (pFEWd-1)/2/pFEWd .*((sMatsI(:,2)) + ...
            sMatsI(:, (colM+colI+3)) + sMatsI(:,10) + ...
            sMatsI(:, (colM+colI+2))/MLP);                                      % Base2：Dispatch ineff
        sMatsI(:,end+1) = sMatsI(:,(colM+colI+3))*avgMem/MLP;                   % L2ICache
        sMatsI(:,end+1) = sMatsI(:,(colM+colI+4))*avgMem/MLP;                   % L2DCache
        sMatsI(:,end+1) = sMatsI(:, 10).*(pCDR+pFEDp);                          % Branch miss penalty
        sMatsI(:,end+1) = sum(sMatsI(:,end-7:end),2);                           % Total Cycle
        
        adjust=find(sMatsI(:,7) < mean(sMatsI(:,7))/pAdjust);                   % Delete too small points
        TOCS09_CPI = sMatsI(startPoints:startPoints+points,end)./iNum(startPoints:startPoints+points,:);
        GEM5_CPI = sMatsI(startPoints:startPoints+points,7)./iNum(startPoints:startPoints+points,:);
        sMatsI(:,end-4:end) = [];
        
        % pCDR = b1 * (max(40, 1/mpubr)).^b2 * (1+b3*fp) * (1+b4*mpuDL1)
        pCDR = ['b(1).*(max(40,1/(sMatsI(:,10)./iNum))).^b(2)*(1+b(3).*iNumFP./iNum)*' ...
            '(1+b(4).*sMatsI(:,colM+colI+1)./iNum)'];
        % MLP = b5 * (mpuDL2).^b6 * (mpuDTLB).^b7
        MLP = '(b(5).*((sMatsI(:,colM+colI+4)./iNum).^b(6)).*((sMatsI(:,14)./iNum).^b(7)))';
        % cStall = max(0, 1-cMiss./(iNum/D)+b8*(1+b9*fp)*(1+b10*mpuDL1))
        cM = '100';
        cStall_t = '(b(8).*(1+b(9).*iNumFP./iNum).*(1+b(10).*(sMatsI(:,colM+colI+1)./iNum)))';
        cStall =strcat('(max(0,1-',cM,'./(iNum/pFEDp+',cStall_t,')).*',cStall_t,')');
        
        if(~exist('ispass11-b.mat','file'))
            % 为了避免多个文件，我这里用匿名的函数指针方式做拟合
            beta0 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]'; % 初始化拟合权重
            pDispatch=['(pFEWd-1)/2/pFEWd.*((sMatsI(:,2))+sMatsI(:,(colM+colI+3))'...
                '+sMatsI(:,10)+sMatsI(:, (colM+colI+2))./'];        % MLP);     % Base2: Dispatch ineff
            pL2I = 'sMatsI(:,(colM+colI+3)).*avgMem./';             % MLP;      % L2ICache miss penalty
            pL2D = 'sMatsI(:,(colM+colI+4)).*avgMem./';             % MLP;      % L2DCache miss penalty
            pBr = 'sMatsI(:,10).*(pFEDp+';                          %pCDR);     % Branch miss penalty
            eval(['[b,r,j,covb,mse] = nlinfit(sMatsI,sMatsI(:,7), @(b,m)', ...
                pDispatch,MLP,')+',pL2I,MLP,'+',pL2D,MLP,'+',pBr,pCDR,'+',cStall,')',',beta0);']);
            b;                                                                  % show b1~b7
            save('ispass11-b.mat', 'b');
        else
            load('ispass11-b.mat', 'b');
        end
        % 利用拟合出来的参数，重新计算得到CPI的值
        MLP = b(5).*((sMatsI(:,colM+colI+4)./iNum).^b(6)).*((sMatsI(:,14)./iNum).^b(7));
        pCDR = b(1).*(max(40,1/(sMatsI(:,10)./iNum))).^b(2)*(1+b(3).*iNumFP./iNum)* ...
            (1+b(4).*sMatsI(:,colM+colI+1)./iNum);
        
        sMatsI(:,end+1) = (pFEWd-1)/2/pFEWd.*((sMatsI(:,2))+sMatsI(:,  ...
            (colM+colI+3))+sMatsI(:,10)+sMatsI(:, (colM+colI+2))./MLP);         % Base2: Dispatch ineff
        sMatsI(:,end+1) = sMatsI(:,(colM + colI + 3)) * avgMem./MLP;            % L2ICache miss penalty
        sMatsI(:,end+1) = sMatsI(:,(colM + colI + 4)) * avgMem./MLP;            % L2DCache miss penalty
        sMatsI(:,end+1) = sMatsI(:,10) .* (pCDR + pFEDp);                       % Branch miss penalty
        sMatsI(:,end+1) = eval(['(max(0,1-',cM,'./(iNum/pFEDp+', ...
            cStall_t,')).*',cStall_t,')']);                                     % Structure harzards 
        sMatsI(:,end+1) = sum(sMatsI(:, end-5: end), 2);                        % Total cycls
        
        adjust=find(sMatsI(:,7) < mean(sMatsI(:,7))*pAdjust);                   % Delete too small points
        
        ISPASS11_CPI = sMatsI(startPoints:startPoints+points,end)./iNum(startPoints:startPoints+points,:);
        sMatsI(:,end-5:end) = [];
        
        %%%%%%%%%%%%%%%%%%%%%%%%%
        MLP = 3; pCDR = 5;
        if(~exist('bpData.mat', 'file'))
            sMatsI(:,end+1) = (pFEWd-1)/2/pFEWd.*((sMatsI(:,2))+sMatsI(:,(colM+colI+3)) ...
                +sMatsI(:,10)+sMatsI(:,(colM+colI+2))/MLP);                     % Base2: Dispatch ineff
                sMatsI(:,end+1) = sMatsI(:,(colM+colI+3))*avgMem/MLP;           % L2ICache miss penalty
            sMatsI(:,end+1) = sMatsI(:,(colM+colI+4))*avgMem/MLP;               % L2DCache miss penalty
            sMatsI(:,end+1) = sMatsI(:,10).*(pCDR+pFEDp);                       % Branch miss penalty
            sMatsI(:,end+1) = sMatsI(:,i_SerialStall);                          % Serialize instruction
            sMatsI(:,end+1) = sum(sMatsI(:,24:26),2);                           % Structure harzards
            sMatsI(:,end+1) = sMatsI(:,8);                                      % Idle cycles
            sMatsI(:,end+1) = sum(sMatsI(:,end-10:end), 2);                     % Total cycles
            figure(); plot(sMatsI(:,end)./iNum); hold on; plot(sMatsI(:,7)./iNum,'r');
            corrCoef_Mech = corrcoef(sMatsI(:,end)./iNum, sMatsI(:,7)./iNum)
            
            adjust=find(sMatsI(:,7) < mean(sMatsI(:,7))*pAdjust);
            P=sMatsI(adjust,end-11:end-1)./repmat(iNum(adjust,:),1,11);
            T=sMatsI(adjust,7)./iNum(adjust,:);
            [~, Pcol] = size(P);
            m=max(max(P)); n=max(max(T));
            P=P'/m; T=T'/n;                                                     % Normalize P/T
            pr(1:Pcol,1)=0; pr(1:Pcol,2)=1;                                     % input param's scope
            warning off all                                                     % Close Warning
            bpnet=newff(pr,[1 1],{'purelin', 'purelin'}, 'traingdx', 'trainbfg');
            bpnet.trainParam.epochs=10000;                                      % 允许最大训练步数2000步
            bpnet.trainParam.goal=1e-5;                                         % 训练目标最小误差0.001
            bpnet.trainParam.show=100;                                          % 每间隔100步显示一次训练结果
            bpnet.trainParam.lr=0.05;                                           % 学习速率0.05
            %bpnet.trainParam.showWindow = false;% 关闭训练过程的GUI
            [bpnet]=train(bpnet,P,T);                                           % 开始训练网络
            bpnet.iw{1,1}                                                       % 隐藏层权重值
            bpnet.b{1}                                                          % 隐藏层阈值
            bpnet.iw{2,1}                                                       % 输出层权重值
            bpnet.b{2}                                                          % 输出层阈值
            save('bpData.mat','bpnet','P','T','n','adjust');                    % 保存
        else
            load('bpData.mat');
            R=(sim(bpnet,P))'*n;                                                % 仿真网络
            %figure(); plot(R,'g'); hold on; plot(sMatsI(adjust,7)./iNum(adjust,:),'r');
            WScope_CPI = R(startPoints:startPoints+points,:);
            %bar3([GEM5_CPI, WScope_CPI, TOCS09_CPI, ISPASS11_CPI], 'detached');
            %bar3([GEM5_CPI, WScope_CPI, ISPASS11_CPI], 'detached');

            results_CPI = [GEM5_CPI, ISPASS11_CPI, TOCS09_CPI, WScope_CPI];
            for ii = 1:points
                if isnan(results_CPI(ii, 2))
                    fprintf('F')
                    results_CPI(ii,2) = (results_CPI(ii, 1)+ results_CPI(ii, 3))/2;
                end
            end
            GEM5_CPI = results_CPI(:,1);
            ISPASS11_CPI = results_CPI(:,2);
            TOCS09_CPI = results_CPI(:,3);
            WScope_CPI = results_CPI(:,4);
            plot(GEM5_CPI, 'r-*', 'LineWidth', 1.0);
            hold all;
            plot(ISPASS11_CPI, 'g-+', 'LineWidth', 1.0);
            hold all;
            plot(TOCS09_CPI, 'b-o', 'LineWidth', 1.0);
            hold all;
            plot(WScope_CPI, 'k-square',  'LineWidth', 1.0);
            legend('GEM5 SIMULATION', 'MECH+REGRESSION', 'MECHANICS', 'OURS');
            csvwrite('results_CPI.csv', results_CPI);
            corrCoef_MechANN = corrcoef(R, sMatsI(adjust,7)./iNum(adjust,:))
            %figure(); bar(sMatsI(adjust,end-10:end-1)./repmat(iNum(adjust,:),1,10), 'stacked');
            figure(); bar(sMatsI(startPoints:startPoints+points, end-10:end-1)./repmat(iNum(startPoints:startPoints+points,:),1,10), 'stacked');
            legend('Base1','L1ICache Miss','L1ITLB','L1DTLB','dispatch','L2ICache','L2DCache', ...
                'MissPrediction','Idle','Serial','Conflicts');
        end
    otherwise
        fprintf('Unkonwn Model!\n');
    end
end
end
ExecutionTime = etime(clock, t1)                                                % Total execution time

end
% EOF



