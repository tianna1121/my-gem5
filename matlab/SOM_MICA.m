%% The whole Analytical Model of ARM processor 
clc;
clear all;
close all;

% Input params
ModelName       = 2;
mostExecutedContext = 4;
accuracy        = 1;
ANNBatch        = 1;
HPCvsMICA       = 2;
bigDataSet      = 1;
if (bigDataSet == 0)
    FileName        = 'singleCore-a15-mica-bbench';
else
    FileName        = 'singleCore-a15-mica-bbench-large';
    %FileName        = 'singleCore-a15-mica-wps';
end

% Define some macroes
DEBUG           = 1;
ANN_ENABLED     = 1;
PLOT_ENABLED    = 1;



% Declear some global vars
pAdjust  = 10000;                                                                   % Exetime<1/pAdjust points
training = 1000;                                                                    % ANN training points
t1=clock;
format shortg;
PreFix = pwd;

% Open simulation output file, and importdata to Matlab workspace
if (~DEBUG)
  FilePath= strcat(PreFix, '/newStats-', FileName, '.mat');
  if (exist(FilePath, 'file'))
      load(FilePath);
  else
      strcat(PreFix, '/newStats-', FileName, '.txt')
      rawData = importdata(strcat(PreFix, '/newStats-', FileName, '.txt'));
      save(FilePath, 'rawData');
  end
else
  strcat(PreFix, '/newStats-', FileName, '.txt');
  rawData = importdata(strcat(PreFix, '/newStats-', FileName, '.txt'));
end

% Raw data processing
[rowRawData, colRawData] = size(rawData);                                           % The size of rawData
i_PID = colRawData - 2;

% Add the duration ticks of consecutive threads into the rawData
durationTicks = 10000;                                                              % First thead exe 1ticks
for ii = 2:rowRawData
    durationTicks(1,ii) = rawData(ii,end) - rawData(ii-1,end);                      % Add the duration ticks
    if(DEBUG)
        if(rawData(ii,end)-rawData(ii-1, end) < 0)
            fprintf('S');
        end
    end
end
rawData(:,end+1) = durationTicks';
if(DEBUG)
    for ii=2:rowRawData
        if (rawData(ii,end-1)-rawData(ii,end) > rawData(ii-1,end-1)) fprintf('O'); end
    end
end
rawData(1,:) = [];
cycleAll = sum(rawData(:,11)); % the all time of the application

% Sort data based on thread's PIDs
% sortedData = sortrows(rawData, [i_PID, i_PID+1]);                                   % Sorting data
% [~, I] = unique(sortedData(:, i_PID));                                              % Find the first row 
% [rowI, ~] = size(I);

% Declare the CPU Hardware Microarchitectural Parameters
pFEDp = 6;                                                                          % Frontend Pipeline depth 
pFEWd = 3;                                                                          % Backend Pipeline width
pCDR = 20; MLP = 8; avgMem = 400; pSerial = 40;                                     % Estimated params
ROBEntry = 40;

% Microarchitecture parameters, include L1 cache/Branch/TLB/structure hazards
paramMatC = { ...
    'L1ICacheHit'; 'L1ICacheMiss'; 'L1ICMSHRHit'; 'L1ICMSHRMiss';
    'L1DCacheHit'; 'L1DCacheMiss'; 'L1DCMSHRHit'; 'L1DCMSHRMiss';
    'CommittedInsts'; 'CommitSquashedInsts'; 
    'NumCycles'; 'IdleCycles'; 'IPC_Total';
    'BrMiss';
    'ITLBHit'; 'ITLBMiss'; 'DTLBHit'; 'DTLBMiss';
    'CommitLoads'; 'CommitMembars'; 'CommitBranches'; 'CommitFPInst'; 'CommitIntInsts';
    'IntRFReads'; 'IntRFWrites'; 'MISCRFReads'; 'MISCRFWrites';
    'RenameROBFull'; 'IQFull'; 'LQFull'; 'SQFull'; 'FullRegisterEvents'; 'SerialStall'
    };

% FORMAT: i_L1ICacheHit
[m,n] = size(paramMatC);
for j=1:m
    eval(['i_', paramMatC{j}, ...
       '=find(not(cellfun(''isempty'', strfind(paramMatC,''', paramMatC{j},'''))));']);
end

pMatMicro = [ ...
    1, 12, 1, 1, 2, 12, 1, 1, ... % L1ICacheHit, L1ICacheMiss, L1ICMSHRHit, L1ICMSHRMiss, L1DCacheHit, L1DCacheMiss, L1DCMSHRHit, L1DCMSHRMiss
    1, 1, 1, 1, 1, ...      % CommittedInst, CommitSquashedInsts, NumCycles, IdleCycles, IPC_Total
    1, ...                  % BrMiss
    1, 12, 1, 12, ...       % ITLBHit, ITLBMiss, DTLBHit, DTLBMiss
    1, 1, 1, 1, 1, ...      % CommitLoads, CommitMembars, CommitBranches, CommitFPInst, CommitIntInsts
    1, 1, 1, 1, ...         % IntRFReads, IntRFWrites, MiscRFReads, MiscRFWrites
    1, 1, 1, 1, 1, 1, ...      % RenameROBFull, IQFull, LQFull, SQFull, FullRegisterEvents, SerialCycle
    ];
[~, colM] = size(pMatMicro);

%% Generate the paramMatMICA
if (bigDataSet == 0)
    eval(['paramMatMICA={};']);
    for i = 1:12  
        eval(['paramMatMICA(i,1)={''fetchAddrDist', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    for i = 1:19
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''cPathLength', num2str(i), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
else
    eval(['paramMatMICA={};']);
    for i = 1:12  
        eval(['paramMatMICA(i,1)={''fetchAddrDist', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:11
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''fetchReuseDist', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:7
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''dispatchRateDist', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:11
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''lqGlobalDistance', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:11
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''lqLocalDistance', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:11
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''stGlobalDistance', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:11
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''stLocalDistance', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:9
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''BasicBlockSize', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:20
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''depGraphDist', num2str(i), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:1
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''bwBranches', '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:1
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''notTakenBranches', '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:1
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''fwBranches', '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:1
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''takenChanged', '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:20
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''cPathLength', num2str(i), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:25
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''pCDRDist', num2str(i), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:40
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''serialDrainInst', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
end


%[~, colC] = size(pMatMicro);
[m,n] = size(paramMatMICA);
for j=1:m
    eval(['i_', paramMatMICA{j}, ...
       '=find(not(cellfun(''isempty'', strfind(paramMatMICA,''', paramMatMICA{j},'''))))+colM;']);
end

if (bigDataSet == 0)
    pMICA = [ ...
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
        1, 1, 1, ...
    ];
else
    pMICA = [ ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                            % fetchAddrDist
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                               % fetchReuseDist
    1, 1, 1, 1, 1, 1, 1,  ...                                          % dispatchRateDist
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                               % lqGlobalDistance
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                               % lqLocalDistance
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,...                                % stGlobalDistance
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,...                                % stLocalDistance
    1, 1, 1, 1, 1, 1, 1, 1, 1,  ...                                    % BasicBlockSize
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                                  % depGraphDist
    1,  ...                                                            % bwBranches
    1,  ...                                                            % notTakenBranches
    1,  ...                                                            % fwBranches
    1,  ...                                                            % takenChanged
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                                  % cPathLengthROBx1
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, ...                                                 % pCDRDist
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                                  % serialDrainInst
];
end

% Instruction parameters, include the latency of different instruction
paramMatI = { ...
    'No_OpClass'; 'IntAlu'; 'IntMult'; 'IntDiv';
    'FloatAdd'; 'FloatCmp'; 'FloatCvt'; 'FloatMult'; 'FloatDiv'; 'FloatSqrt';
    'SimdAdd'; 'SimdAddAcc'; 'SimdAlu'; 'SimdCmp'; 'SimdCvt'; 'SimdMisc';
    'SimdMult'; 'SimdMultAcc'; 'SimdShift'; 'SimdShiftAcc'; 'SimdSqrt';
    'SimdFloatAdd'; 'SimdFloatAlu'; 'SimdFloatCmp'; 'SimdFloatCvt'; 'SimdFloatDiv';
    'SimdFloatMisc'; 'SimdFloatMult'; 'SimdFloatMultAcc'; 'SimdFloatSqrt';
    'MemRead'; 'MemWrite'; 'IprAccess'; 'InstPrefetch'; 'serialInst'
    };

[~, colC] = size(pMICA);
[m,n] = size(paramMatI);
for j=1:m
    eval(['i_', paramMatI{j}, ...
       '=find(not(cellfun(''isempty'', strfind(paramMatI,''', paramMatI{j},'''))))+colM+colC;']);
end

pMI = [ ...
    1, 1, 3, 12, ...        % No_OpClass, IntAlu, IntMult, IntDiv
    5, 5, 5, 4, 9, 33, ...  % FloatAdd, FloatCmp, FloatCvt, FloatDiv, FloatSqrt
    4, 4, 4, 4, 3, 3, ...   % SimdAdd, SimdAddAcc, SimdAlu, SimdCmp, SimdCvt, SimdMisc
    5, 5, 3, 3, 9, ...      % SimdMult, SimdMultAcc, SimdShift, SimdShiftAcc, SimdSqrt
    5, 5, 3, 3, 3, ...      % SimdFloatAdd, SimdFloatAlu, SimdFloatCmp, SimdFloatCvt, SimdFloatDiv
    3, 3, 3, 9, ...         % SimdFloatMisc, SimdFloatMult, SimdFloatMultAcc, SimdFloatSqrt
    2, 2, 3, 1, 1, ...      % MemRead, MemWrite, IprAccess, InstPrefetch, serialInst
    ];
[~, colI] = size(pMI);

% System parameters include L2 caches and memory access
paramMatSys = { ...
    'L2CacheHit'; 'L2CacheMissAll'; 'L2CacheMissInst'; 'L2CacheMissData';
    'L2CacheMSHRMissInst'; 'L2CacheMSHRMissPre'; 'L2CacheMSHRMissData';
    'L2CacheMissILatency'; 'L2CacheMissDLatency';
    'PHYMEMRead'; 'PHYMEMWrite'
    };

[m,n] = size(paramMatSys);
for j=1:m
    eval(['i_', paramMatSys{j}, ...
       '=find(not(cellfun(''isempty'', strfind(paramMatSys,''', paramMatSys{j},'''))))+colM+colC+colI;']);
end

pMatSys = [ ...
    1, 1, 1, 1, ...         % L2CacheHit, L2CacheMissAll, L2CacheMissInst, L2CacheMissData
    1, 1, 1, ...            % L2CacheMSHRMissInst, L2CacheMSHRMissPre, L2CacheMSHRMissData
    1, 1, ...               % L2CacheMissInstLatency, L2CacheMissDataLatency
    1, 1, ...               % PHYMEMRead, PHYMEMWrite
    ];

pMat = [pMatMicro, pMICA, pMI, pMatSys];

STD = 1;
if STD
    fetchAddrDistAll    = sum(rawData(:, i_fetchAddrDist0:i_fetchAddrDist11), 2);
    fetchReuseDistAll   = sum(rawData(:, i_fetchReuseDist0:i_fetchReuseDist10), 2);
    dispatchRateDistAll = sum(rawData(:, i_dispatchRateDist0:i_dispatchRateDist6), 2);
    lqGlobalDistanceAll = sum(rawData(:, i_lqGlobalDistance0:i_lqGlobalDistance10), 2);
    lqLocalDistanceAll  = sum(rawData(:, i_lqLocalDistance0:i_lqLocalDistance10), 2);
    stGlobalDistanceAll = sum(rawData(:, i_stGlobalDistance0:i_stGlobalDistance10), 2);
    stLocalDistanceAll  = sum(rawData(:, i_stLocalDistance0:i_stLocalDistance10), 2);
    BasicBlockAll       = sum(rawData(:, i_BasicBlockSize0:i_BasicBlockSize8), 2);
    depGraphDistAll     = sum(rawData(:, i_depGraphDist1:i_depGraphDist20), 2);
    branchesAll         = sum(rawData(:, [i_bwBranches, i_fwBranches, i_notTakenBranches]), 2);
    takenBranchesAll    = sum(rawData(:, [i_bwBranches,i_fwBranches]), 2);
    cPathLengthROBx1All = sum(rawData(:, i_cPathLength1:i_cPathLength20), 2);
    pCDRDistAll         = sum(rawData(:, i_pCDRDist1:i_pCDRDist25), 2);
    serialDrainInstAll  = sum(rawData(:, i_serialDrainInst0:i_serialDrainInst39), 2);

    rawData(:, i_fetchAddrDist0:i_fetchAddrDist11)   = rawData(:, i_fetchAddrDist0:i_fetchAddrDist11) ./ repmat(fetchAddrDistAll,1,12);
    rawData(:, i_fetchReuseDist0:i_fetchReuseDist10)  = rawData(:, i_fetchReuseDist0:i_fetchReuseDist10)./ repmat(fetchReuseDistAll,1,11);
    rawData(:, i_dispatchRateDist0:i_dispatchRateDist6) =rawData(:, i_dispatchRateDist0:i_dispatchRateDist6)./repmat(dispatchRateDistAll,1,7);
    rawData(:, i_lqGlobalDistance0:i_lqGlobalDistance10) = rawData(:, i_lqGlobalDistance0:i_lqGlobalDistance10)./repmat(lqGlobalDistanceAll,1,11);
    rawData(:, i_lqLocalDistance0:i_lqLocalDistance10) = rawData(:, i_lqLocalDistance0:i_lqLocalDistance10) ./repmat(lqLocalDistanceAll,1,11);
    rawData(:, i_stGlobalDistance0:i_stGlobalDistance10) = rawData(:, i_stGlobalDistance0:i_stGlobalDistance10) ./repmat(stGlobalDistanceAll,1,11);
    rawData(:, i_stLocalDistance0:i_stLocalDistance10) = rawData(:, i_stLocalDistance0:i_stLocalDistance10) ./repmat(stLocalDistanceAll,1,11);
    rawData(:, i_BasicBlockSize0:i_BasicBlockSize8) = rawData(:, i_BasicBlockSize0:i_BasicBlockSize8) ./repmat(BasicBlockAll,1,9);
    rawData(:, i_depGraphDist1:i_depGraphDist20) = rawData(:, i_depGraphDist1:i_depGraphDist20)./repmat(depGraphDistAll,1,20);
    rawData(:, [i_bwBranches, i_fwBranches]) = rawData(:, [i_bwBranches, i_fwBranches]) ./repmat(takenBranchesAll,1,2);
    rawData(:, [i_takenChanged]) = rawData(:, [i_takenChanged]) ./repmat(branchesAll,1,1);
    rawData(:, i_cPathLength1:i_cPathLength20) = rawData(:, i_cPathLength1:i_cPathLength20)./repmat(cPathLengthROBx1All,1,20);
    rawData(:, i_pCDRDist1:i_pCDRDist25) = rawData(:, i_pCDRDist1:i_pCDRDist25) ./repmat(pCDRDistAll,1,25);
    rawData(:, i_serialDrainInst0:i_serialDrainInst39) = rawData(:, i_serialDrainInst0:i_serialDrainInst39) ./ repmat(serialDrainInstAll,1,40);
end




% Split the raw data into different sMatsI, according to thread's pid num.
% threadContextSwitchNums = [];
% for i = 1 : (rowI-1)
    % threadContextSwitchNums(i,1:2) = [I(i+1)-I(i), sortedData(I(i), i_PID)];
% end
% threadContextSwitchNums(end+1,1:2) = [I(end)-I(i), sortedData(I(end), i_PID)];      % Need fix this

% sortedContextSwitch = sortrows(threadContextSwitchNums, [-1]);
% selectedThread = sortedContextSwitch(1:mostExecutedContext, :);

% globalTag = 0;
[rowRawData, colRawData] = size(rawData);                                           % The size of rawData

% for ii = 1:rowRawData
% if(find(I(ii+1)-I(ii) == selectedThread(:,1)))
    % globalTag = globalTag + 1;
    % sMatsI = sortedData(I(ii):I(ii+1),:);                                             % Thread's matrix 

    % PID                 = sMatsI(1, end-3);
    % Duration            = sMatsI(:, end);
    % [~, i_Duration]     = size(sMatsI);
    % i_Ticks             = i_Duration - 1;
    
    switch ModelName
        case HPCvsMICA % hardward performance counters distance vs microarchitecture-independent workload characteristics
            fprintf('HPC vs MICA Distance!\n');
            rows = [1:5:13000];
            % HPC = rawData(rows,[i_L1ICacheHit:i_SerialStall,i_L2CacheHit:i_PHYMEMWrite]);
            HPC        = 1000./ rawData(rows, i_IPC_Total);
            % % HPC        = rawData(rows, i_IPC_Total);
            HPC(:,end+1) = rawData(rows, i_L1ICacheMiss) ./ sum(rawData(rows, [i_L1ICacheHit i_L1ICacheMiss]),2);
            HPC(:,end+1) = rawData(rows, i_L1DCacheMiss) ./ sum(rawData(rows, [i_L1DCacheHit i_L1DCacheMiss]),2);
            HPC(:,end+1) = rawData(rows, i_ITLBMiss) ./ sum(rawData(rows, [i_ITLBHit i_ITLBMiss]),2);
            HPC(:,end+1) = rawData(rows, i_DTLBMiss) ./ sum(rawData(rows, [i_DTLBHit i_DTLBMiss]),2);
            % HPC(:,end+1) = sum(rawData(rows, [i_L1ICacheMiss,i_L1DCacheMiss]),2) ./ sum(rawData(rows, [i_L1ICacheHit i_L1ICacheMiss i_L1DCacheHit i_L1DCacheMiss]),2);
            % HPC(:,end+1) = sum(rawData(rows, [i_ITLBMiss,i_DTLBMiss]),2) ./ sum(rawData(rows, [i_ITLBHit i_ITLBMiss i_DTLBHit i_DTLBMiss]),2);

             % HPC = rawData(rows,[i_IPC_Total,i_L1ICacheHit,i_L1ICacheMiss,i_L1DCacheHit,i_L1DCacheMiss,i_ITLBHit,i_ITLBMiss,...
             % i_DTLBHit,i_DTLBMiss]);
             HPC = zscore(HPC,1,1);
             HPC_dist = pdist(HPC, 'euclidean');        
            MICA = rawData(rows, i_fetchAddrDist1:i_serialDrainInst39);
            % MICA(:,find(sum(abs(MICA),1)<10000))=[];
            % MICA = rawData(rows, i_fetchAddrDist1:i_fetchReuseDist10);
            % MICA = rawData(rows, i_fetchAddrDist1);

            % the PCA analysis
            % X = MICA(:,i_fetchAddrDist1:i_serialDrainInst39);
            MICA(:,[1 2 27 28 29 38 39 40 51 62 73 81 82 94:1:102 107 108 116:1:126 134:1:151 134:1:151]) = [];
            X = MICA;
             % stdr = std(X);
             % X = X./repmat(stdr,length(X),1);
            % [coeffX, scoreX, latentX, tsquaredX, explainedX] = princomp(zscore(X,1,1));
            % latentX1 = 100*(latentX)./sum(latentX);
            % pareto(latentX1);
            % useful = find(cumsum(latentX1)>95,1,'first');
            % fprintf(strcat('Useful PCA Dimention:', num2str(useful), '\n'));
            % X = scoreX(:,5);

            % MICA_dist = pdist(zscore(X,1,1), 'euclidean');
            X = zscore(X,1,1);
            MICA_dist = pdist(X, 'euclidean');
            figure();
            plot(MICA_dist, HPC_dist, 'k.');
            xlabel('distance in normalized MICA space');
            ylabel('distance in normalized HPC space');
        case ANNBatch
            fprintf('\nStart\n');
            % fprintf(strcat('The Analytical Model, processing Thread: 1-', num2str(globalTag), '\n'));

            % Calculate the inst. mix
            % iNum     = sum(sMatsI(:,i_No_OpClass : i_InstPrefetch),  2);                    % All Inst.
            % iNumALU  = sum(sMatsI(:,i_IntAlu     : i_IntDiv),        2);                    % ALU Inst.
            % iNumFP   = sum(sMatsI(:,i_FloatAdd   : i_FloatSqrt),     2);                    % FP Inst.
            % iNumSIMD = sum(sMatsI(:,i_SimdAdd    : i_SimdFloatSqrt), 2);                    % SIMD Inst.
            % iNumMEM  = sum(sMatsI(:,i_MemRead    : i_MemWrite),      2);                    % MEM Inst.
            % iNumNOP  = sMatsI(:,i_No_OpClass);                                              % NOP Inst.
            % iNumPRE  = sMatsI(:,i_InstPrefetch);                                            % PREFETCH Inst.
            % iNumIPR  = sMatsI(:,i_IprAccess);                                               % IPRAcess Inst.
            % fprintf('Step 1: The Inst.Mix has been calculated\n');

            % Construct PMUData dataSet
            % PMUData = [];
            % PMUData(:,end+1) = sum(sMatsI(:,i_No_OpClass:i_serialInst)*pMI',2)/pFEWd;        % Base1: weighted    
            % PMUData(:,end+1) = sMatsI(:, i_L1ICacheMiss) .* pMat(i_L1ICacheMiss);            % L1ICache miss penalty
            % PMUData(:,end+1) = sMatsI(:, i_ITLBMiss) .* pMat(i_ITLBMiss);                    % L1ITLB miss penalty
            % PMUData(:,end+1) = sMatsI(:, i_DTLBMiss) .* pMat(i_DTLBMiss);                    % D1ITLB miss penalty
    
            % PMUData(:,end+1) = (pFEWd-1)/2/pFEWd .*((sMatsI(:,i_L1ICacheMiss)) ...
                % + sMatsI(:, i_L2CacheMissData) ...
                % + sMatsI(:, i_BrMiss) ...
                % + sMatsI(:, i_L2CacheMissInst) / MLP);                               % Base2：dispatch ineff
            % PMUData(:,end+1) = sMatsI(:, i_L2CacheMissInst)*avgMem/MLP;              % L2ICache miss penalty
            % PMUData(:,end+1) = sMatsI(:, i_L2CacheMissData)*avgMem/MLP;              % L2DCache miss penalty
            % PMUData(:,end+1) = sMatsI(:, i_BrMiss).*(pCDR+pFEDp);                    % Branch miss penalty
            % PMUData(:,end+1) = sMatsI(:, i_IdleCycles);                              % Idle Cycle
            % PMUData(:,end+1) = sMatsI(:, i_SerialStall);                             % Serial Cycle
            % PMUData(:,end+1) = sum(sMatsI(:,i_RenameROBFull:i_SQFull),2);            % Stucture hazards
            % PMUData(:,end+1) = sum(PMUData(:,end-10:end),2);                          % Total Cycles
            % fprintf(strcat('Step 2: Constructing PMUData completed: ', num2str(length(PMUData)), ' entries\n'));
            
            % The TOCS09 results
            % figure();
            % suptitle(strcat('TOCS09, MLP=', num2str(MLP), ';pCDR=', num2str(pCDR), ';avgMem=', num2str(avgMem)));
            % subplot(2,1,1);
            % bar(PMUData(:,1:end-1), 'stacked');
            % GEM5_CPI   = sMatsI(:,i_NumCycles)./iNum;
            % TOCS09_CPI = PMUData(:,end)./iNum;
            % subplot(2,1,2);
            % plot(GEM5_CPI, 'b'); hold on; plot(TOCS09_CPI, 'r'); title('SIMU(b) vs TOCS09(r)');
            % fprintf(strcat('Step 3: Compare the accuracy of Mechanical Model\n'));

            % Setup the MICAData, target data
            if(bigDataSet == 0)
                MICAData = rawData(:, i_cPathLength1:i_cPathLength12);
            else
                MICAData = rawData(:, i_fetchAddrDist1:i_serialDrainInst39);
            end
            [~,colMICAData] = size(MICAData);
            % targetCol = 'i_RenameROBFull';
            % targetCol = 'i_IPC_Total';
            targetCol = 'i_IPC_Total';

            % if (globalTag == 1)
                % eval(['P', num2str(globalTag),' = [MICAData];']);
                % eval(['T', num2str(globalTag),' = rawData(:, ', targetCol, ');']);
            % else
                % eval(['P', num2str(globalTag), '= [P',num2str(globalTag-1),'; MICAData];']);
                % eval(['T', num2str(globalTag), '= [T',num2str(globalTag-1),'; rawData(:, ',targetCol,')];']);
            % end
            P = [MICAData];
            T = [rawData(:,targetCol)];
            % fprintf(strcat('Step 4: Constructing MICAData completed: ', num2str(colMICAData), ' characters\n'));
            

        % for ANN Models
        % eval(['[Ptrain',num2str(globalTag),'] = (mapminmax(P',num2str(globalTag),'''))'';']);
        % eval(['[Ttrain',num2str(globalTag),'] = mapminmax(T',num2str(globalTag),''');']);
        % eval(['[Ptest',num2str(globalTag),'] = (mapminmax(P',num2str(globalTag),'''))'';']);
        % eval(['[Ttest',num2str(globalTag),'] = mapminmax(T',num2str(globalTag),''')'';']);
        Ptrain = mapminmax(P')';
        Ttrain = mapminmax(T')';

        % SOM clustering and choose the useful testing point
        % eval(['x = Ptrain',num2str(globalTag),''';']);
        x = Ptrain';
        % Create a Self-Organizing Map
     for j = 10
        dimensionX = j;
        dimensionY = j;
        dimension1 = dimensionX * dimensionY;
        dimension = [j j];
        % Note: selforgmap(dimensions,coverSteps,initNeighbor,topologyFcn,distanceFcn)
        net = selforgmap(dimension, 100, 3, 'gridtop', 'linkdist');
        [net,tr] = train(net,x);        % Train the Network
        y = net(x);                     % Test the Network
        sumofY = sum(y,2);
        % clustering the input entries
        z = [];
        for i = 1:length(y)
            z(:,i) = find(y(:,i)==1);
        end
        fprintf(strcat('Step 5: Clusting the sampling entries into:', num2str(dimension1), ' completed\n'));

        % Select the most representative sampling points 
        % eval(['[rowTotal, ~]=size(Ptrain', num2str(globalTag), ');']);
        % rateOfTrain = [];
        figure();               % for center of every cluster
        for k = 1:dimension1
            % Note: Ptrain1_1s = Ptrain1(find(z==1),:);
            % eval(['Ptrain', num2str(globalTag), '_', num2str(k), 's=Ptrain',num2str(globalTag),'(find(z==',num2str(k),'),:);']);
            % Note: position
            eval(['position_', num2str(k), '= find(z==', num2str(k),');']);
            eval(['Ptrain', '_', num2str(k), 's=Ptrain', '(find(z==',num2str(k),'),:);']);
            % Note: Inst_1s_all = rawData(find(z==k),i_CommittedInsts);
            eval(['Inst_', num2str(k), 's_all = sum(rawData(find(z==', num2str(k),'), i_CommittedInsts));']);
            % eval(['Ttrain', num2str(globalTag), '_', num2str(k), 's=Ttrain',num2str(globalTag),'(:,find(z==',num2str(k),'));']);
            % % Note: [rowTrain1_1,~] = size(Ptrain1_1s);
            % eval(['[rowTrain', num2str(globalTag), '_', num2str(k),', ~] = size(Ptrain',num2str(globalTag), '_',num2str(k),'s);']);
            % eval(['rowRate = rowTrain', num2str(globalTag), '_' num2str(k), '/rowTotal;']);

            KMeansEnabled = 1;
            if (KMeansEnabled == 0)
                % % Adjust the trainning ratio based on rowRate
                % if rowRate >= 0.20
                    % trainRatio = 0.1;
                % elseif (rowRate >= 0.10) && (rowRate < 0.2)
                        % trainRatio = 0.2;
                % else
                    % trainRatio = 1.0;
                % end
                % Note: rateOfTrain1_1 = round(trainRatio*rowTrain1_1);
                % eval(['rateOfTrain', num2str(globalTag), '_', num2str(k), '=round(trainRatio*rowTrain', ...
                    % num2str(globalTag), '_', num2str(k), ');']);
                % eval(['rateOfTrain = [rateOfTrain; rateOfTrain',num2str(globalTag), '_', num2str(k),'];']);
                % % Note: Ptrain1_1sed = Ptrain1_1s(1:trateOfTrain1_1,:);
                % eval(['Ptrain', num2str(globalTag),'_',num2str(k),'sed' '=Ptrain', num2str(globalTag), '_', ...
                    % num2str(k), 's(1:rateOfTrain', num2str(globalTag),'_', num2str(k),',:);']);
                % eval(['Ttrain', num2str(globalTag),'_',num2str(k),'sed' '=Ttrain', num2str(globalTag), '_', ...
                    % num2str(k), 's(1:rateOfTrain', num2str(globalTag),'_', num2str(k),');']);
            else
                % Find "selectPoints" entries acroding to the centroid of kmeans
                % selectPoints = 1;
                % if (eval(['rowTrain', num2str(globalTag), '_' num2str(k), '<= selectPoints']))
                    % selectPoints = eval(['rowTrain', num2str(globalTag), '_' num2str(k), ';']);
                % end
                % Note: [Centroid, C, sumd, D] = kmeans(Ptrain1_1s,1);
                if(eval(['size(Ptrain', '_', num2str(k), 's, 1) > 1'])) % if cluster size > 1
                    eval(['[Centroid,C,sumd,D] = kmeans(Ptrain', '_', num2str(k), 's, 1);']);
                    [W, DI] = sort(D);
                    rowD = size(D,1);
                    eval(['Ptrain', '_', num2str(k), 'sed = Ptrain', '_', num2str(k), 's(DI(1), :);']);
                    eval(['IPC_', num2str(k), 'sed = rawData(position_', num2str(k) ,'(DI(1)),i_IPC_Total)./1000;']);
                    % Note: CPI_1sed = 1.0 / rawData(position_1(DI(1)),i_IPC_Total) .* 1000
                    eval(['CPI_', num2str(k), 'sed = 1.0 / rawData(position_', num2str(k) ,'(DI(1)),i_IPC_Total) .* 1000;']);
                    % CPI_1s_all: cluster1's all CPI
                    eval(['CPI_', num2str(k), 's_all = repmat(1.0,rowD,1) ./ rawData(position_', num2str(k) ,',i_IPC_Total) .* repmat(1000,rowD,1);']);
                    % eval(['Ttrain', num2str(globalTag), '_', num2str(k), 'sed = Ttrain', num2str(globalTag), '_', ...
                        % num2str(k), 's(:, I(1:20));']);
                elseif(eval(['size(Ptrain', '_', num2str(k), 's, 1) == 1'])) % if cluster size = 1
                    DI = 1;
                    W = 0;
                    eval(['Ptrain', '_', num2str(k), 'sed = Ptrain', '_', num2str(k), 's(DI(1), :);']);
                    eval(['IPC_', num2str(k), 'sed = rawData(position_', num2str(k) ,'(DI(1)),i_IPC_Total)./1000;']);
                    eval(['CPI_', num2str(k), 'sed = 1.0 / rawData(position_', num2str(k) ,'(DI(1)),i_IPC_Total) .* 1000;']);
                    eval(['CPI_', num2str(k), 's_all = repmat(1.0,1,1) ./ rawData(position_', num2str(k) ,',i_IPC_Total) .* 1000;']);
                else % if cluster is void
                    eval(['Ptrain', '_', num2str(k), 'sed = 0;']);
                    eval(['CPI_', num2str(k), 'sed = 0;']);
                end
                subplot(dimensionX,dimensionY,k);
                hist(W); hold on; title(strcat('Cluster: ', num2str(k)));
            end
            % Note CPIerr_1s_all =  (CPI_1s_1 - CPI_1sed) + (CPI_1s_2 - CPI_1sed) +...
            eval(['CPIerr_', num2str(k), 's_all = 0;']);
            if size(eval(['Ptrain_', num2str(k),'s']),1) == 0 % need to think about the cluster num is zero

            else
                for c = 1:size(eval(['Ptrain_', num2str(k),'s']),1)
                    eval(['CPIerr_',num2str(k),'s_all=CPIerr_',num2str(k),'s_all+ abs(CPI_',num2str(k),'s_all(',num2str(c),')- CPI_',num2str(k),'sed);']);
                end
            end
        end
        simTimeAll = 0;
        CPIerr_all = 0;
        InstAll    = 0;
        for k = 1:dimension1
            % Note: simTimeAll = simTimeAll + Inst_1s_all * CPI_1sed + ...
            eval(['simTimeAll = simTimeAll + Inst_', num2str(k), 's_all * CPI_', num2str(k), 'sed;']);
            % Note: CPIerr_all = CPIerr_1s_all + CPIerr_2s_all + ...
            eval(['CPIerr_all = CPIerr_all + CPIerr_', num2str(k), 's_all;']);
            eval(['InstAll = InstAll + Inst_',num2str(k), 's_all;']);
        end
        % Note: err = (cycleAll - simTimeAll) / cycleAll
        % All Inst num  InstAll = Inst_1s_all + Inst_2s_all +...
        eval(['err_', num2str(j^2), '= (cycleAll - simTimeAll) / cycleAll;'])
        % eval(['CPIerr_', num2str(j^2), '= CPIerr_all / (rowRawData* cycleAll /InstAll);']);




    end
otherwise
    fprintf('Unknown Model! Check your input params\n');
end

    % Note: Ptrain1selected() = [];
    % eval(['Ptrain',num2str(globalTag),'selected = [];']);
    % eval(['Ptrain',num2str(globalTag),'selected = [];']);
    % eval(['Ttrain','selected = [];']);
    % eval(['Ttrain','selected = [];']);
    % for k = 1:dimension1
    % % Note: Ptrain1selected = [Ptrain1selected; Ptrain1_1sed];
    % eval(['Ptrain', num2str(globalTag), 'selected = [Ptrain',num2str(globalTag),'selected;Ptrain', ...
    % num2str(globalTag), '_', num2str(k),'sed];']);
    % eval(['Ttrain', num2str(globalTag), 'selected = [Ttrain',num2str(globalTag),'selected,Ttrain', ...
    % num2str(globalTag), '_', num2str(k),'sed];']);
    % end
    % fprintf(strcat('Step 6: Select representative points, total:', num2str(size(Ptrain1selected)), ' entries\n'));

    % % Construct the ANN for 1/1+2/1+2+3/1+2+3+4/... threads
    % % Note: bpnet = newff(Ptrain1selected', Train1selected, [15, 10], {'logsig', 'logsig', 'purelin'}, 'trainlm');
    % eval(['bpnet = newff(Ptrain',num2str(globalTag),'selected'', Ttrain',num2str(globalTag), 'selected', ...
    % ', [15 10], {''logsig'', ''logsig'', ''purelin''}, ''trainlm'');']);
    % bpnet.trainParam.epochs=1000;                                           % Set the max training step
    % bpnet.trainParam.goal=3e-5;                                             % Set the target error
    % bpnet.trainParam.show=100;                                              % Show results in xx step
    % bpnet.trainParam.lr=0.05;                                               % Set the learning params
    % bpnet.trainParam.max_fail = 20;
    % bpnet.divideFcn = '';
    % % Train the ANN
    % t1=clock;
    % % Note: [bpnet1] = train(bpnet, Ptrain1', Train1);
    % eval(['[bpnet',num2str(globalTag),'] = train(bpnet, Ptrain',num2str(globalTag),''', Ttrain',num2str(globalTag),');']);
    % ExecutionTime = etime(clock, t1);
    % fprintf(strcat('Step 7: Train ANN spend: ', num2str(ExecutionTime), 's\n'));

    % if (globalTag == mostExecutedContext)
    % for j = 1:mostExecutedContext
    % h = figure();
    % eval(['suptitle(strcat(''Trained by Thread 1-'',','num2str(j)','));']);
    % % set(h,'Position',[400,100,1400,1000]);
    % for i = 1:globalTag
    % subplot(2,2,i);
    % % Testing the ANN results
    % % Note: result1 = sim(bpnet1, Ptest1);
    % eval(['result',num2str(j),' =sim(bpnet',num2str(j),',Ptest', num2str(i),''');']); 
    % eval(['plot(result',num2str(j),');']);
    % hold on;
    % eval(['plot(Ttest',num2str(i),''', ''r'');']);
    % eval(['title(strcat(''Testing on Thread 1-'',','num2str(i)','));']);
    % % Note: xlabel(strcat('meanErr=', num2str(mean(result1-Ttest1)./Test1))));
    % eval(['xlabel(strcat(''meanErr='', num2str(mean(result',num2str(j),'-Ttest',num2str(i),'))));']);
    % end
    % end
    % end
    % fprintf(strcat('Step 8: The Analytical Model''s output\n'));
    % end



