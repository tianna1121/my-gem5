%% Branch Miss-penalty

% pCDR, pFE, pSquash

%% simulation for pCDR:cycles
aPCDR = sMatsI(:, i_pCDRDist1:i_pCDRDist9);
bPCDR = aPCDR.*repmat([1.5,3,6,12,24,48,96,196,384], length(aPCDR),1);
cPCDR = sum(bPCDR,2)./sMatsI(:,i_BrMiss);
avgPCDR = mean(cPCDR)

figure();
aFE = repmat(6,length(cPCDR),1);

aSquash = sMatsI(:, i_squashCycles)./sMatsI(:,i_BrMiss);
avgSquash = mean(aSquash)


bar([aFE, cPCDR, aSquash], 'stack');

%% Regression for pCDR
beta0 = [1,1,1,1];
pCDR_11 = ['b(1).*(max(40,1/(sMatsI(:,i_BrMiss)./iNum))).^b(2)*(1+b(3).*iNumFP./iNum)*(1+b(4).*sMatsI(:,i_L1DCacheMiss)./iNum)'];
eval(['[b,r,j,covb,mse] = nlinfit(sMatsI, cPCDR, @(b,m)', pCDR_11,',beta0);']);
b
dPCDR = b(1).*(max(40,1/(sMatsI(:,i_BrMiss)./iNum))).^b(2)*(1+b(3).*iNumFP./iNum)*(1+b(4).*sMatsI(:,i_L1DCacheMiss)./iNum);

%% ANNs for pCDR
figure()
plot(dPCDR, 'k');
hold on;
plot(cPCDR, 'r');


