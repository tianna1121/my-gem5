%% Clear the environment
close all
clear
clc
addpath(genpath(pwd));
tic

%% define some parameters
TEST_PLOT       = 1;
PureNNet        = 0;
PCA_Analysis    = 0;
MECH_ANNet      = 0;
FIRST_TIME      = 0;
SECOND_TIME     = 0;
THIRD_TIME      = 1;
CASE_RTIME      = 1;

composFactor = 100;
if FIRST_TIME
    name = ['tInst1'; 'tInst2'; 'tInst3'; 'tMemor'; 'tPipel'; 'tStall'; 'tL2dRd'; 'tL2dWt'; 'tExcep';...
            'tIcach'; 'tDcach'; 'tL2dWh'; 'tL2iRd'; 'tCoher'; 'tL2pre'; 'tTlbp1'; 'tTlbp2'; 'tBranc'];
end

if SECOND_TIME
    name = ['tIns'; 'tTlb'; 'tCah'];
end

if THIRD_TIME
    name = ['tALU'; 'tFPU'; 'tMEM'];
end
[rName, cName] = size(name)


%% Load every PMU Counter tables
for i = 1:rName
    eval(['load(strcat(name(',num2str(i),',:), ''.mat''))']);
    eval(['mat', name(i,:), '= table2array(', name(i,:),');']);
    eval(['realCPUac=(mat',name(i,:),'(:,2)+mat',name(i,:),'(:,3))./composFactor.*mat',name(i,:),'(:,5);']);

    % Find the start point of Benchmark within those tables
    csCPUactivity = [];
    for j = 0:round(length(realCPUac)/composFactor)-2
        a = (cumsum(realCPUac(j*composFactor+1:(j+1)*composFactor),1))./composFactor;
        csCPUactivity(end+1,:) = a(end);
    end
    eval(['csCPUactivity', name(i,:), '=csCPUactivity;']);
    eval(['I', name(i,:),'=find(csCPUactivity', name(i,:),'>0.96*max(csCPUactivity', name(i,:), '),1)']);
    eval(['startPoint', name(i,:), '=I', name(i,:), '*composFactor;']);
    eval(['matA', name(i,:), '=mat', name(i,:), '(startPoint', name(i,:), ':end,:);']);
end

%% Find the end point of Benchmark within those tables
lengthTable = [];
for i = 1:rName
    eval(['lengthTable(:,end+1)', '=length(matA', name(i,:),');']);
end
endPoint = min(lengthTable);

%% Generate the final tables according to startPoint and endPoint
for i = 1:rName
    eval(['matF', name(i,:), '=matA', name(i,:), '(1:endPoint,:);']);
end
if FIRST_TIME
    matFinal = [matFtInst1(:,2:11), ...     % User,Sys,Content,clkCycle,iEXE,iFlt,iLS,iMain,iNEON,iSec 
            matFtInst2(:,6:8), ...      % iDMB, iDSB, iISB
            matFtInst3(:,6), matFtInst3(:, 9:10), ...   % iContextIDR, iMemRead, iMemWrite
            matFtIcach(:,6:11), ...     % DCacc,DCdepStall,ICdepStall,ITLBref,ICref,TLBStall
            matFtDcach(:,8:11), ...     % DCevic, DClinefill, DCref, DTLBref
            matFtMemor(:,6:11), ...     % clkDataE,clkIntCore,clkNEON,dTLBalloc,iTLBalloc,UnalignedAcc
            matFtPipel(:,12:13), ...    % issueStageEmpty, issueStageNoDispatch
            matFtStall(:,7:12), ...     % dMainTLBMiss,dMicroTLBMiss,DMB,iMainTLBMiss,iMicroTLBMiss,memWrite
            matFtExcep(:,7:10), ...     % CoreFunc, expRet, expTaken, externalIRQ
            matFtL2dRd(:,6:7), ...      % L2CdataReadHit, L2CdataReadRequest
            matFtL2iRd(:,9:10), ...     % L2CinstReadHit, L2CinstReadRequest
            matFtL2dWt(:,9:10), ...     % L2CdataWriteHit, L2CdataWriteRequest
            matFtL2dWh(:,9:10), ...     % L2CcastOUT, L2CdataWriteThroughRequest
            matFtCoher(:,6:7), ...      % CacheCoherentHit, CacheCoherentMiss
            matFtL2pre(:,9:10), ...     % L2CInternalPretchAlloc, L2CPrefetchHintReceived
            matFtBranc(:,5:8), ...      % ImmBranch, branchMissPred, PCchange, potentialPred
            ];
    colName = { 'User'; 'Sys'; 'Contention'; 'clkCycle'; 'iEXE'; 'iFloat'; 'iLoadStore'; 'iMainExe'; ...
            'iNEON'; 'iSecEXE'; ...
            'iDMB'; 'iDSB'; 'iISB'; ...
            'iContextIDR'; 'iMemRead'; 'iMemWrite'; ...
            'DCacc'; 'DCdepStall'; 'ICdepStall'; 'ITLBref'; 'ICref'; 'TLBStall'; ...
            'DCevic'; 'DClinefill'; 'DCref'; 'DTLBref'; ...
            'clkDataE'; 'clkIntCore'; 'clkNEON'; 'dTLBalloc'; 'iTLBalloc'; 'UnalignedAcc'; ...
            'issueStageEmpty'; 'issueStageNoDispatch'; ...
            'dMainTLBMiss'; 'dMicroTLBMiss'; 'DMBStall'; 'iMainTLBMiss'; 'iMicroTLBMiss'; 'memWrite'; ...
            'CoreFunc'; 'expRet'; 'expTaken'; 'externalIRQ'; ...
            'L2CdataReadHit'; 'L2CdataReadRequest'; ...
            'L2CinstReadHit'; 'L2CinstReadRequest'; ...
            'L2CdataWriteHit'; 'L2CdataWriteRequest'; ...
            'L2CcastOUT'; 'L2CdataWriteThroughRequest'; ...
            'CacheCoherentHit'; 'CacheCoherentMiss'; ...
            'L2CInternalPretchAlloc'; 'L2CPrefetchHintReceived'; ...
            'ImmBranch'; 'branchMissPred'; 'PCchange'; 'potentialPred'; ...
            };
end

if SECOND_TIME
    matFinal = [matFtIns(:,2:12), ...   % User,Sys,Content,clkCycle,iEXE,iFlt,iLS,iMain,iNEON,iSec 
                matFtTlb(:,6:11), ...   % branchMissPred,TLBStall,clkDataE,MainTLBMiss,DMBStall,iMainTLBMiss
                matFtCah(:,6:11), ...   % DCdepStall,ICdepStall,clkNEON,issueStageEmpty,issueStageNoDispatch,memWrite
    ];
    colName = { 'User'; 'Sys'; 'Contention'; 'clkCycle'; 'Frequency'; 'clkIntCore'; 'iFloat'; 'iLoadStore'; ...
        'iMainExe'; 'iNEON'; 'iSecEXE';...
        'branchMissPred'; 'TLBStall'; 'clkDataE'; 'dMainTLBMiss'; 'DMBStall'; 'iMainTLBMiss'; ...
        'DCdepStall'; 'ICdepStall'; 'clkNEON'; 'issueStageEmpty'; 'issueStageNoDispatch'; 'memWrite'; ...
    };
end

if THIRD_TIME
    matFinal = [matFtALU(:,2:12), ... % User,Sys,Content,clkCycle,Frequency,clkAUL,iLS,iMainExe,iSecEXE,bMissPred,issNoDis
                matFtFPU(:,6:11), ... % iFloat,iNEON,clkDataE,clkNEON,DMBStall,issueStageEmpty
                matFtMEM(:,6:11), ... % memWrite,TLBStall,dMainTLBMiss,iMainTLBMiss,DCdepStall,ICdepStall
    ];
    colName = {'User'; 'Sys'; 'Contention'; 'clkCycle'; 'Frequency'; 'clkIntCore'; 'iLoadStore'; ... 
        'iMainExe'; 'iSecEXE'; 'branchMissPred'; 'issueStageNoDispatch'; ... 
        'iFloat'; 'iNEON'; 'clkDataE'; 'clkNEON'; 'DMBStall'; 'issueStageEmpty'; ...
        'memWrite'; 'TLBStall'; 'dMainTLBMiss'; 'iMainTLBMiss'; 'DCdepStall'; 'ICdepStall'; ...
    };
end
rColName = length(colName);

%% Clear unused vars
%for i = 1:rName
%    eval(['clear(''', name(i,:), ''');']);
%    eval(['clear(''mat', name(i,:), ''');']);
%    eval(['clear(''matA', name(i,:), ''');']);
%    eval(['clear(''csCPUactivity', name(i,:), ''');']);
%    eval(['clear(''startPoint', name(i,:), ''');']);
%    eval(['clear(''I', name(i,:), ''');']);
%end
clear realCPUac;
clear csCPUactivity;
clear composFactor;

%% Patition the Execution to stages(CPU integer, fload, ..., 2D, 3D)
sRuntime           = round(06.00/100 * endPoint);  %  6.00%    0.00~ 15.66s  0'00.00~0'15.66s
sSingleThreadInt   = round(13.20/100 * endPoint);  %  7.20%   15.67~ 34.45s  0'15.67~0'34.45s
sSingleThreadFloat = round(20.30/100 * endPoint);  %  7.10%   34.46~ 52.98s  0'34.46~0'52.98s
sInteger           = round(35.60/100 * endPoint);  % 15.30%   52.99~ 92.92s  0'52.99~1'32.92s
sFloat             = round(42.80/100 * endPoint);  %  7.40%   92.93~112.23s  1'32.93~1'52.23s
sRAM               = round(46.55/100 * endPoint);  %  3.60%  112.24~121.63s  1'52.24~2'01.63s
sRAMSpeed          = round(47.25/100 * endPoint);  %  1.20%  121.64~124.76s  2'01.64~2'04.76s
sMultithread       = round(58.65/100 * endPoint);  % 14.95%  124.77~163.78s  2'04.77~2'43.78s
sDatabaseIO        = round(63.96/100 * endPoint);  %  1.53%  163.79~167.77s  2'43.79~2'47.77s
sStorageIO         = round(71.62/100 * endPoint);  %  7.66%  167.78~187.76s  2'47.78~3'07.76s
sCASE2D            = round(79.27/100 * endPoint);  %  7.65%  187.77~207.73s  3'07.77~3'27.73s
sCASE3D            = round(99.99/100 * endPoint);  % 20.72%  207.74~261.81s  3'27.74~4'21.81s
sOthers            = round(99.99/100 * endPoint);  % 37.65%
Stage = [1, sRuntime, sSingleThreadInt, sSingleThreadFloat, sInteger, sFloat, sRAM, ...
          sRAMSpeed, sMultithread, sOthers]
nameStage = ['Rtime'; 'STInt'; 'STFlt'; 'Integ'; 'Float'; 'RAMpf'; 'RAMsp'; 'MulTd'; 'Other'];
rNameStage = length(nameStage);

%%
for j = 1:length(Stage)-1
    eval(['matFinal_', nameStage(j,:),'=matFinal(Stage(j):Stage(j+1),:);']);
end

%% Delete the outlet points
%for j = 1:length(Stage)-1
%    for k = Stage(j):Stage(j+1)-1
%        eval(['meanAll = sum(matFinal_',nameStage(j,:),'(Stage(j):Stage(j+1)))/(Stage(j+1)-Stage(j))',';']);
%        for ii = 1:60
%            if(eval(['matFinal_',nameStage(j,:),'(k,ii)>300*(meanAll(ii))']))
%                eval(['matFinal_',nameStage(j,:),'(k,ii)=meanAll(ii);']);
%                fprintf('E');
%            end
%        end
%    end
%    fprintf(strcat('j=',num2str(j),'\n'));
%end

%% Clear unused vars
clear s*;
clear a;
%clear matFinal;
clear endPoint;

%% Generate some useful statistics
for j = 1:length(Stage)-1
    eval(['avgMatFinal_', nameStage(j,:), '=mean(matFinal_', nameStage(j,:), ');']);
    eval(['stdMatFinal_', nameStage(j,:), '=std(matFinal_', nameStage(j,:), ');']);
end

%% Plot the every column of the matFinal 
if TEST_PLOT
for i = 1:rColName
    h = figure(i);
    set(h,'name', strcat('Exynos4412, Cortex-A9 SingleCore: ', colName{i}));
    set (h,'Position',[400,100,1600,1200]);
    for j = 1:rNameStage
        sh = subplot(3,3,j); eval(['plot(matFinal_',nameStage(j,:),'(:,',num2str(i),'));']); 
        hold on; 
        title(nameStage(j,:), 'fontsize', 9);
        xlabel(strcat('Mean=',num2str(eval(['avgMatFinal_',nameStage(j,:),'(1,i)']),'%10.1e'), ...
                '; STD=',num2str(eval(['stdMatFinal_',nameStage(j,:),'(1,i)']),'%10.1e')), ...
                'FontSize',7);
        ylabel('PMU Counter', 'FontSize', 7);
    end
    suptitle(strcat('Exynos4412, Cortex-A9 SingleCore: ', colName{i}));
    saveas(h, strcat('matFinal_', colName{i}), 'pdf');
    saveas(h, strcat('matFinal_', colName{i}), 'png');
end
end

%% Clear unused vars
clear avg*;
clear std*;

%% PCA Analysis
if PCA_Analysis
if TEST_PLOT
    h = figure();
    set(h,'Position',[400,100,1600,1200]);
    set(h, 'name', 'PCA Analysis');
    suptitle('PCA Analysis, Cortex-A9 SingleCore Running Antutu-v5.6');
end

for i = 1:rNameStage
    eval(['x = matFinal_', nameStage(i,:),'(:,5:end-4);']);
    eval(['[coeff_',nameStage(i,:),',score_', nameStage(i,:), ...
        ',latent_',nameStage(i,:),',tsquared, explained] = princomp(x);']);
    eval(['latent_',nameStage(i,:), '=100*latent_',nameStage(i,:),'./sum(latent_',nameStage(i,:),');']);
    if TEST_PLOT
        subplot(3,3,i);pareto(latent);
        title(strcat(nameStage(i,:)), 'FontSize', 9);
    end
end

if TEST_PLOT
    saveas(h, 'PCA_Analysis', 'fig');
    saveas(h, 'PCA_Analysis', 'pdf');
    saveas(h, 'PCA_Analysis', 'png');
end
end

%% Pure Neural Networks Model
if PureNNet
definedEpochs = 10000;
h = figure();
set(h,'Position',[400,100,1600,1200]);
set(h, 'name', 'Pure Neural Networks based on PCA');
suptitle(strcat('Pure Neural Networks based on PCA, A9x1, Antutu-v5.6: PMU(Red)/NNpred(Blue),',...
                'defEpochs=', num2str(definedEpochs),'times'));
for i = 1:rNameStage
    eval(['P = score_', nameStage(i,:),'(:,1:6);']);
    eval(['T = matFinal_', nameStage(i,:),'(:,4);']);
    [rowP, colP] = size(P);
    
    m = max(max(P)); n = max(max(T));
    P=P'/m; T=T'/n;
    pr(1:colP,1) = 0; pr(1:colP,2) = 1;
    warning off all;
        
    % Construct the Neural Network Model
    tic;
    bpnet=newff(pr,[20 10 1],{'purelin', 'logsig', 'purelin'}, 'traingdx', 'trainbfg');
    bpnet.trainParam.epochs=definedEpochs;                                  % Set the max training step
    bpnet.trainParam.goal=1e-4;                                             % Set the target error
    bpnet.trainParam.show=100;                                              % Show results in xx step
    bpnet.trainParam.lr=0.05;                                               % Set the learning params
    bpnet.trainParam.showWindow = true;                                     % Open/Close the Graphics
    
    eval(['[bpnet', nameStage(i,:),']=train(bpnet,P,T);']);                 % Start training
    eval(['r=sim(bpnet',nameStage(i,:),',P); R=r''*n;']);                   % Simulation Process    
    T = T'*n;

    subplot(3,3,i); plot(R); hold on; plot(T,'r');
    a = corrcoef(R, T);
    title(strcat(nameStage(i,:), ',trainTime:', num2str(toc), 's'));
    xlabel(strcat('coef:', num2str(a(1,2)),';meanErr:',num2str(((mean(T)-mean(R))./mean(R)),'%10.1e')));
    ylabel('EXE cycles');
end
saveas(h, 'Pure_NeuralNetwork_BasedOn_PCA', 'fig');
saveas(h, 'Pure_NeuralNetwork_BasedOn_PCA', 'pdf');
saveas(h, 'Pure_NeuralNetwork_BasedOn_PCA', 'png');

end

%% Pure Mechanics Model
% Declare the CPU Hardware Microarchitectural Parameters
%pFEDp = 6;                                                                          % Frontend Pipeline depth 
%pFEWd = 3;                                                                          % Backend Pipeline width
%pCDR = 20; MLP = 8; avgMem = 400; pSerial = 40;                                     % Estimated params
%ROBEntry = 40;
avgALULatency = 0.95;
avgFPLatency = 1.3;
avgNEONLatency = 8;
loadBalanceALUs = 0.33;
bpMissPenalty = 7;

for i = 1:rColName
    eval(['index_', colName{i}, '=find(not(cellfun(''isempty'', strfind(colName,''',colName{i},'''))));']);
end

if TEST_PLOT
    h = figure();
    set(h,'Position',[400,100,1600,1050]);
    set(h, 'name', 'ALUx2 Inst, speculativeEXE Inst(Renaming), FP Inst and sumTotal Inst');
    suptitle('ALU Inst(Blue), speculativeEXE Inst(Renaming,Red), FP Inst(Magenta) and sumTotal Inst(Cyan)');
end
for i = 1:rNameStage
    eval(['aluExe_',nameStage(i,:), ...
        ' = matFinal_',nameStage(i,:),'(:,index_iMainExe)', ...
        ' + matFinal_',nameStage(i,:),'(:,index_iSecEXE);']);
    eval(['sumExe_',nameStage(i,:), ...
        ' = (matFinal_',nameStage(i,:),'(:,index_iMainExe)', ...
        ' + matFinal_',nameStage(i,:),'(:,index_iSecEXE))', ...
        ' + matFinal_',nameStage(i,:),'(:,index_iLoadStore).*loadBalanceALUs', ...
...        ' + matFinal_',nameStage(i,:),'(:,index_iDMB)', ...
...        ' + matFinal_',nameStage(i,:),'(:,index_iISB)', ...
...        ' + matFinal_',nameStage(i,:),'(:,index_iDSB)', ...
        ';']);

    % DVFS Technique will dynamically change the frequence of different modules within CPU
    eval(['dvfsALU_', nameStage(i,:),'=matFinal_', nameStage(i,:),'(:,index_clkIntCore)' ...
        './matFinal_',nameStage(i,:),'(:,index_Frequency).*1000;']);
    eval(['dvfsFP_',  nameStage(i,:),'=matFinal_', nameStage(i,:),'(:,index_clkDataE)' ...
        './matFinal_',nameStage(i,:),'(:,index_Frequency).*1000;']);
    eval(['dvfsNEON_',nameStage(i,:),'=matFinal_', nameStage(i,:),'(:,index_clkNEON)' ...
        './matFinal_',nameStage(i,:),'(:,index_Frequency).*1000;']);

    eval(['totalEXELatency_',nameStage(i,:), ...
        ' = max(sumExe_', nameStage(i,:), '.*avgALULatency.*dvfsALU_', nameStage(i,:), ...
        ' , matFinal_', nameStage(i,:), '(:,index_iNEON).*avgNEONLatency.*dvfsNEON_' nameStage(i,:), ...
        ' + matFinal_', nameStage(i,:), '(:,index_iFloat).*avgFPLatency.*dvfsFP_',nameStage(i,:), ');']);

    if TEST_PLOT
        subplot(3,3,i); eval(['plot(aluExe_',nameStage(i,:),');']); 
        hold on; eval(['plot(matFinal_',nameStage(i,:),'(:,index_iEXE), ''r'');']);
        hold on; eval(['plot(matFinal_',nameStage(i,:),'(:,index_iFloat), ''m'');']);
        hold on; eval(['plot(sumExe_',nameStage(i,:),', ''c'');']); 
        title(nameStage(i,:));
        eval(['avgAluExe = mean(aluExe_',nameStage(i,:),');']);
        eval(['avgSumExe = mean(sumExe_',nameStage(i,:),');']);
        eval(['avgFloat  = mean(matFinal_',nameStage(i,:),'(:,index_iFloat));']);
        eval(['avgiEXE   = mean(matFinal_',nameStage(i,:),'(:,index_iEXE));']);
        xlabel(strcat('Alu:',num2str(avgAluExe,'%10.2e'),',iEXE:',num2str(avgiEXE,'%10.2e'), ...
            ',FP:',num2str(avgFloat,'%10.2e'),',sum:',num2str(avgSumExe,'%10.2e')));
        ylabel('Instructions');
    end
end

if TEST_PLOT
    saveas(h, 'Instructions', 'fig');
    saveas(h, 'Instructions', 'pdf');
    saveas(h, 'Instructions', 'png');
    %export_fig h -pdf
end

mechName = [
    'mechP1_InstructionsLatency';
    'mechP2_L1ICacheMissLatency';
    'mechP3_L1IDmTLBMissLatency';
%    'mechP4_L1DMcTLBMissLatency';
%    'mechP5_DispInefficientRate';
%    'mechP6_L2ICacheMissLatency';
    'mechP7_L2DCacheMissLatency';
    'mechP8_BranchMissPdPenalty';
%    'mechP9_TolSystemIdleCycles';
    'mechPA_SysStructureHazards';
    'mechPB_DMBInstructionStall';
    'mechPC_L2IMcTLBMissLatency';
    'mechPD_L2DMcTLBMissLatency';
    'mechPE_L2DMemoWritePenalty';
];
[rMechName, ~] = size(mechName);
% 处理器dispatch级有三种情况：1）指令窗口为空；2）指令窗口不空但没有指令可以issue；3）指令窗口不空且可以正常issue指令到执行级。对于指令窗口为空的情况而言，可能是由如下2种原因导致的：其一，指令流供给不上来（指令cache层次缺失或者指令TLB层次缺失）；其二，出现了分支预测失败，导致流水线刷新，正确路径指令尚未从fetch级达到指令窗口。对于指令窗口不空但没有指令可以issue的情况而言，主要是2种原因导致：其一，长延时指令无法提交，堵住了指令窗口，没有新的指令进入窗口，且窗口内指令也都依赖于长延时指令。对于指令窗口不空且可以正常issue指令到执行级的情况而言，又要进一步分为2类：其一，能够以3+1的速率正常issue指令；其二，不能到达理想的issue速率。

% 基于上述分析，我们知道：如果希望能够较为精确的以机理方式构建处理器的解析模型，就需要考虑上述各类情况。具体来说：一段时间内处理器执行的性能，是由这段时间执行的cycle数目以及被执行的有效指令条数确定的。其中执行的cycle数目应该是core没有被clock gating的cycle数目，并且考虑dvfs的影响；而被执行的有效指令条数就需要通过总的执行指令数目-分支预测失效/推测加载失效指令求得。

% 因此，我们可以列出机理模型的计算公式如下：
% 总cycle数目
%     = max(执行定点指令的数目*定点指令的平均延时*dvfsALU*不均衡度, 执行LOAD/STORE指令的数目*LOAD/STORE指令的平均延时*dvfsALU，执行浮点指令的数目*浮点指令的平均延时*dvfsFP+执行NEON指令的数目*NEON指令的平均延时*dvfsNEON)
%     + 指令cache层次惩罚+指令TLB层次惩罚
%     + 分支预测失败惩罚（分支预测失败指令数目*（流水线前端深度+分支确定时间））
%     + 结构冲突引入的额外延时
%     + 数据cache层次惩罚+数据TLB层次惩罚+数据写回引入的惩罚
%     + 串行指令引入的流水线延时

for i = 1:rNameStage
    eval(['mechP1_InstructionsLatency_',nameStage(i,:),'=totalEXELatency_',nameStage(i,:),'(:,:);']);
    eval(['mechP2_L1ICacheMissLatency_',nameStage(i,:),'=matFinal_',nameStage(i,:),'(:,index_ICdepStall);']);
    eval(['mechP3_L1IDmTLBMissLatency_',nameStage(i,:),'=matFinal_',nameStage(i,:),'(:,index_TLBStall);']);
%    eval(['mechP4_L1DMcTLBMissLatency_',nameStage(i,:),'=matFinal_',nameStage(i,:),'(:,index_dMicroTLBMiss);']);
%    eval(['mechP5_DispInefficientRate_',nameStage(i,:),'=matFinal_',nameStage(i,:),'(:,1);']);
%    eval(['mechP6_L2ICacheMissLatency_',nameStage(i,:),'=matFinal_',nameStage(i,:),'(:,1);']);
    eval(['mechP7_L2DCacheMissLatency_',nameStage(i,:),'=matFinal_',nameStage(i,:),'(:,index_DCdepStall);']);
    eval(['mechP8_BranchMissPdPenalty_',nameStage(i,:),'=matFinal_',nameStage(i,:), ...
        '(:,index_branchMissPred).*bpMissPenalty;']);
%    eval(['mechP9_TolSystemIdleCycles_',nameStage(i,:),'=matFinal_',nameStage(i,:),'(:,1);']);
    eval(['mechPA_SysStructureHazards_',nameStage(i,:),'=matFinal_',nameStage(i,:), ...
        '(:,index_issueStageNoDispatch)','-matFinal_',nameStage(i,:),'(:,index_issueStageEmpty);']);
    eval(['mechPB_DMBInstructionStall_',nameStage(i,:),'=matFinal_',nameStage(i,:),'(:,index_DMBStall);']);
    eval(['mechPC_L2IMcTLBMissLatency_',nameStage(i,:),'=matFinal_',nameStage(i,:),'(:,index_iMainTLBMiss);']);
    eval(['mechPD_L2DMcTLBMissLatency_',nameStage(i,:),'=matFinal_',nameStage(i,:),'(:,index_dMainTLBMiss);']);
    eval(['mechPE_L2DMemoWritePenalty_',nameStage(i,:),'=matFinal_',nameStage(i,:),'(:,index_memWrite);']);


    eval(['mechTotal_', nameStage(i,:), '=zeros(length(matFinal_',nameStage(i,:),'),1);']);
    eval(['mechStack_', nameStage(i,:), '=[];']);

    for j = 1:rMechName
        eval(['mechTotal_',nameStage(i,:),'=mechTotal_',nameStage(i,:), ...
            '+',mechName(j,:),'_',nameStage(i,:),';']);
        eval(['mechStack_',nameStage(i,:),'=[mechStack_',nameStage(i,:), ...
            ',',mechName(j,:),'_',nameStage(i,:),'];']);
    end
end
    
h = figure();
set(h,'Position',[20,100,1600,1050]);
set(h, 'name', 'Mechanics Model, CPI Stack based on PMU Counters');
suptitle('Mechanics Model, CPI Stack based on PMU Counters, Meched(Blue), PMUed(Red)');

for i = 1:rNameStage
    subplot(3,3,i);
    for j = 1:rMechName
%        eval(['bar(mechStack_',nameStage(i,:),',''stacked'');']);
        eval(['plot(mechTotal_',nameStage(i,:),',''b'');']);
        hold on; eval(['plot(matFinal_',nameStage(i,:),'(:,index_clkCycle)./1000,''r'')']);
        hold on; eval(['plot(matFinal_',nameStage(i,:),'(:,index_clkIntCore),''m'')']);
        hold on; eval(['plot(matFinal_',nameStage(i,:),'(:,index_clkDataE),''c'')']);
%        hold on; eval(['plot(matFinal_',nameStage(i,:),'(:,index_clkNEON),''g'')']);
    end
    title(nameStage(i,:));
    eval(['coef=corrcoef(mechTotal_',nameStage(i,:),',matFinal_',nameStage(i,:),'(:,index_clkCycle));']);
    eval(['adsErr =(mechTotal_',nameStage(i,:),'-matFinal_',nameStage(i,:),'(:,index_clkCycle));']);
    eval(['avgErr =mean(adsErr./', 'mechTotal_',nameStage(i,:),');']);
    xlabel(strcat('avgErr=', num2str(avgErr),';coef=', num2str(coef(1,2))));
    ylabel('Meched/PMUed Cycles');
end

if CASE_RTIME
matFinal_RtimeP = matFinal_Rtime(1:100,:);
mechTotal_RtimeP = mechTotal_Rtime(1:100,:);
h = figure();
set(h,'Position',[20,100,1600,1050]);
set(h, 'name', 'CASE Study: Runtime, based on PMU Counter');
suptitle('Case Study: Runtime, based on PMU Counter');
subplot(2,2,1);
plot( matFinal_RtimeP(:,index_issueStageEmpty), 'b'); hold on;
plot( matFinal_RtimeP(:,index_ICdepStall) ...
    + matFinal_RtimeP(:,index_TLBStall) ...
    + matFinal_RtimeP(:,index_iMainTLBMiss) ...
    + matFinal_RtimeP(:,index_branchMissPred).*7, 'r'); hold on;
plot( matFinal_RtimeP(:,index_issueStageEmpty)+matFinal_RtimeP(:,index_clkIntCore), 'c'); hold on;
plot( matFinal_RtimeP(:,index_clkCycle)./1000, 'm');
title('issEmpty:Blue, ICdepStall+TLBStall+iMainTLBMiss+bMissPred*7:Red, issEmpty+clkInt:Cyan');

% 在各类缺失事件减少发生的情况下：
% 1）P37:当指令的平均延时较长，或ILP较少时，issue级出现NoDispatch的几率就会变大，而有效执行指令数目减少，定点核cycle数减少；
% 2）P61:当指令的平均延时较短，或ILP较高时，issue级出现NoDispatch的几率就会减少，而有效执行指令数目增加，定点核cycle数增加；
% 因此：定点核cycle数与执行指令数的比值，可以用于确定这段时间的ILP或者平均指令延时。比值越高，说明ILP越小，延时越长。
% ILP越高时，NoDispatch的几率就会越少
matAllInst  = matFinal_RtimeP(:,index_iMainExe) ...
            + matFinal_RtimeP(:,index_iSecEXE) ...
            + matFinal_RtimeP(:,index_iLoadStore);

corrcoef_clkCycle_clkInt = corrcoef(matFinal_RtimeP(:,index_clkCycle)./1000, ...
    matFinal_RtimeP(:,index_clkIntCore) ...
    )
corrcoef_clkCycle_issueNoDispatch = corrcoef(matFinal_RtimeP(:,index_clkCycle)./1000, ...
    matFinal_RtimeP(:,index_issueStageNoDispatch) ...
    )
corrcoef_clkInt_AllInst = corrcoef(matFinal_RtimeP(:,index_clkIntCore), ...
    matAllInst ...
    )

subplot(2,2,2);
plot( matFinal_RtimeP(:,index_clkIntCore), 'r'); hold on;
plot( matFinal_RtimeP(:,index_clkNEON), 'b'); hold on;
plot( matFinal_RtimeP(:,index_clkDataE), 'm'); hold on;
plot( matAllInst,'c'); hold on;
plot( matFinal_RtimeP(:,index_issueStageNoDispatch), 'b'); hold on;
title('clkInt:Red, clkDataE:m, clkNEON:Blue, iMainExe+iSecEXE+iLoadStore:Cyan, issNoDis:Blue');

subplot(2,2,3);
plot( matFinal_RtimeP(:,index_clkCycle)./1000, 'b'); hold on;
plot( matFinal_RtimeP(:,index_Frequency)./1000, 'r'); hold on;
title('clkCycle/1000:Blue, Frequncy/1000:Red');

subplot(2,2,4)
plot( mechTotal_RtimeP, 'c'); hold on; 
plot( matFinal_RtimeP(:,index_DMBStall), 'r'); hold on; 
plot( matFinal_RtimeP(:,index_TLBStall), 'b'); hold on;
plot( matFinal_RtimeP(:,index_ICdepStall), 'm'); hold on;
plot( matFinal_RtimeP(:,index_DCdepStall), 'k'); hold on;
title('mechTotal:Cyan, DMBStall:Red, TLBStall:Blue, ICStall:m, DCStall:black');

figure();
plot( matFinal_RtimeP(:,index_clkIntCore)./matAllInst,'r');  

end

if MECH_ANNet
h = figure();
set(h,'Position',[100,100,1600,1050]);
set(h, 'name', 'Mech+ANN, CPI Stack based on PMU Counters');
suptitle('Mech+ANN, CPI Stack based on PMU Counters, Mech_ANNet(Blue), PMUed(Red)');
definedEpochs = 5000;

for i = 1:rNameStage
    eval(['P = mechTotal_', nameStage(i,:),';']);
    eval(['T = [matFinal_',  nameStage(i,:),'(:,index_clkIntCore)];']);
    [rowP, colP] = size(P);
    m = max(max(P)); n = max(max(T));
    P=P'/m; T=T'/n;
    pr(1:colP,1) = 0; pr(1:colP,2) = 1;
    warning off all;

    % Construct the Neural Network Model
    tic;
    bpnet=newff(pr,[10 20 1],{'purelin', 'logsig', 'purelin'}, 'traingdx', 'trainbfg');
    bpnet.trainParam.epochs=definedEpochs;                                  % Set the max training step
    bpnet.trainParam.goal=1e-5;                                             % Set the target error
    bpnet.trainParam.show=100;                                              % Show results in xx step
    bpnet.trainParam.lr=0.05;                                               % Set the learning params
    bpnet.trainParam.showWindow = true;                                     % Open/Close the Graphics
    
    eval(['[bpnet', nameStage(i,:),']=train(bpnet,P,T);']);                 % Start training
    eval(['r=sim(bpnet',nameStage(i,:),',P); R=r''*n;']);                   % Simulation Process    
    T = T'*n;
    subplot(3,3,i); plot(R); hold on; plot(T,'r');
    a = corrcoef(R, T);
    title(strcat(nameStage(i,:), ',trainTime:', num2str(toc), 's'));
    xlabel(strcat('coef:', num2str(a(1,2)),';meanErr:',num2str(((mean(T)-mean(R))./mean(R)),'%10.1e')));
    ylabel('EXE cycles');
end
end

disp(strcat('Total execution time:',num2str(toc), 's'));
