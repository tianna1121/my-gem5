%% The whole Analytical Model of ARM processor 
clc;
clear all;
close all;

% Input params
format shortg;
sampleThread    = 1;
sRate           = 1;
ISPASS11_OK     = 0;
STD             = 0;

%FileName        = 'singleCore-a15-mica-bbench-large';   % OLD DATA
FileName        = 'singleCore-a15-mica-bbenchBranch';   % 

%FileName       = 'singleCore-a15-mica-weibo';          % OLD DATA
%FileName       = 'singleCore-a15-mica-ttpod-5s';       % OLD DATA
%FileName       = 'singleCore-a15-mica-wps';            % OLD DATA
%FileName       = 'singleCore-a15-mica-pdf';            % OLD DATA
%FileName       = 'singleCore-a15-mica-antutu';         % OLD DATA
%FileName       = 'singleCore-a15-mica-baidumap';       % OLD DATA

% Define some macroes
t1              = clock;
PreFix          = pwd;

% Open simulation output file, and importdata to Matlab workspace
strcat(PreFix, '/newStats-', FileName, '.txt');
rawData = importdata(strcat(PreFix, '/newStats-', FileName, '.txt'));

% Raw data processing
[rowRawData, colRawData] = size(rawData);                               % The size of rawData
i_PID = colRawData - 2;

% Add the duration ticks of consecutive threads into the rawData
durationTicks = 1000;                                                   % First thead exe 1000 ticks
for ii = 2:rowRawData 
    durationTicks(1,ii) = rawData(ii,end) - rawData(ii-1,end);          % Add the duration ticks
end
rawData(:,end+1) = durationTicks';

% Sort data based on thread's PIDs
sortedData = sortrows(rawData, [i_PID, i_PID+1]);                       % Sorting data
[~, I] = unique(sortedData(:, i_PID));                                  % Find the first row 
[rowI, ~] = size(I);

% Microarchitecture parameters, include L1 cache/Branch/TLB/structure hazards
paramMatC = { ...
    'L1ICacheHit'; 'L1ICacheMiss'; 'L1ICMSHRHit'; 'L1ICMSHRMiss';
    'L1DCacheHit'; 'L1DCacheMiss'; 'L1DCMSHRHit'; 'L1DCMSHRMiss';
    'CommittedInsts'; 'CommitSquashedInsts'; 
    'NumCycles'; 'IdleCycles'; 'IPC_Total';
    'BrMiss';
    'ITLBHit'; 'ITLBMiss'; 'DTLBHit'; 'DTLBMiss';
    'CommitLoads'; 'CommitMembars'; 'CommitBranches'; 'CommitFPInst'; 'CommitIntInsts';
    'IntRFReads'; 'IntRFWrites'; 'MISCRFReads'; 'MISCRFWrites';
    'RenameROBFull'; 'IQFull'; 'LQFull'; 'SQFull'; 'FullRegisterEvents'; 'SerialStall'
};

% FORMAT: i_L1ICacheHit
[m,n] = size(paramMatC);
for j=1:m
    % i_L1ICacheHit = find(not(cellfun('isempty',strfind(paramMatC, L1ICacheHit))));
    eval(['i_',paramMatC{j},'=find(not(cellfun(''isempty'',strfind(paramMatC,''',paramMatC{j},'''))));']);
end

% The penalty of event
pMatMicro = [ ...
    1, 12, 1, 1, 2, 12, 1, 1, ...   % L1ICacheHit, L1ICacheMiss, L1ICMSHRHit, L1ICMSHRMiss, L1DCacheHit, L1DCacheMiss, L1DCMSHRHit, L1DCMSHRMiss
    1, 1, 1, 1, 1, ...              % CommittedInst, CommitSquashedInsts, NumCycles, IdleCycles, IPC_Total
    1, ...                          % BrMiss
    1, 12, 1, 12, ...               % ITLBHit, ITLBMiss, DTLBHit, DTLBMiss
    1, 1, 1, 1, 1, ...              % CommitLoads, CommitMembars, CommitBranches, CommitFPInst, CommitIntInsts
    1, 1, 1, 1, ...                 % IntRFReads, IntRFWrites, MiscRFReads, MiscRFWrites
    1, 1, 1, 1, 1, 1, ...           % RenameROBFull, IQFull, LQFull, SQFull, FullRegisterEvents, SerialCycle
];
[~, colM] = size(pMatMicro);

% Generate the paramMatMICA
eval(['paramMatMICA={};']);
for i = 1:12  
    % paramMatMICA(i,1) = {'fetchAddrDist1'};
    eval(['paramMatMICA(i,1)={''fetchAddrDist', num2str(i-1), '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:11
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''fetchReuseDist', num2str(i-1), '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:7
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''dispatchRateDist', num2str(i-1), '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:11
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''lqGlobalDistance', num2str(i-1), '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:11
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''lqLocalDistance', num2str(i-1), '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:11
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''stGlobalDistance', num2str(i-1), '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:11
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''stLocalDistance', num2str(i-1), '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:9
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''BasicBlockSize', num2str(i-1), '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:20
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''depGraphDist', num2str(i), '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:1
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''bwBranches', '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:1
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''notTakenBranches', '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:1
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''fwBranches', '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:1
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''takenChanged', '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:20
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''cPathLength', num2str(i), '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:25
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''pCDRDist', num2str(i), '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:1
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''squashCycles', num2str(i), '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:40
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''serialDrainInst', num2str(i-1), '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:8
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''validInstsInROB', num2str(i), '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

for i = 1:8
    eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''exeInvalidInsts', num2str(i-1), '''};'])
end
[tRowMICA, tColMICA] = size(paramMatMICA);

[m,n] = size(paramMatMICA);
for j=1:m
    eval(['i_',paramMatMICA{j},'=find(not(cellfun(''isempty'',strfind(paramMatMICA,''', paramMatMICA{j},'''))))+colM;']);
end

pMICA = [ ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                             % fetchAddrDist
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                                % fetchReuseDist
    1, 1, 1, 1, 1, 1, 1,  ...                                           % dispatchRateDist
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                                % lqGlobalDistance
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                                % lqLocalDistance
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,...                                 % stGlobalDistance
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,...                                 % stLocalDistance
    1, 1, 1, 1, 1, 1, 1, 1, 1,  ...                                     % BasicBlockSize
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                                   % depGraphDist
    1,  ...                                                             % bwBranches
    1,  ...                                                             % notTakenBranches
    1,  ...                                                             % fwBranches
    1,  ...                                                             % takenChanged
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                                   % cPathLengthROBx1
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, ...                                                  % pCDRDist
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, ...                                                              % SquashCycles
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                                   % serialDrainInst
];

% Instruction parameters, include the latency of different instruction
paramMatI = { ...
    'No_OpClass'; 'IntAlu'; 'IntMult'; 'IntDiv';
    'FloatAdd'; 'FloatCmp'; 'FloatCvt'; 'FloatMult'; 'FloatDiv'; 'FloatSqrt';
    'SimdAdd'; 'SimdAddAcc'; 'SimdAlu'; 'SimdCmp'; 'SimdCvt'; 'SimdMisc';
    'SimdMult'; 'SimdMultAcc'; 'SimdShift'; 'SimdShiftAcc'; 'SimdSqrt';
    'SimdFloatAdd'; 'SimdFloatAlu'; 'SimdFloatCmp'; 'SimdFloatCvt'; 'SimdFloatDiv';
    'SimdFloatMisc'; 'SimdFloatMult'; 'SimdFloatMultAcc'; 'SimdFloatSqrt';
    'MemRead'; 'MemWrite'; 'IprAccess'; 'InstPrefetch'; 'serialInst'
};

[~, colC] = size(pMICA);
[m,n] = size(paramMatI);
for j=1:m
    eval(['i_',paramMatI{j},'=find(not(cellfun(''isempty'',strfind(paramMatI,''',paramMatI{j},'''))))+colM+colC;']);
end

pMI = [ ...
    1, 1, 3, 12, ...        % No_OpClass, IntAlu, IntMult, IntDiv
    5, 5, 5, 4, 9, 33, ...  % FloatAdd, FloatCmp, FloatCvt, FloatDiv, FloatSqrt
    4, 4, 4, 4, 3, 3, ...   % SimdAdd, SimdAddAcc, SimdAlu, SimdCmp, SimdCvt, SimdMisc
    5, 5, 3, 3, 9, ...      % SimdMult, SimdMultAcc, SimdShift, SimdShiftAcc, SimdSqrt
    5, 5, 3, 3, 3, ...      % SimdFloatAdd, SimdFloatAlu, SimdFloatCmp, SimdFloatCvt, SimdFloatDiv
    3, 3, 3, 9, ...         % SimdFloatMisc, SimdFloatMult, SimdFloatMultAcc, SimdFloatSqrt
    2, 2, 3, 1, 1, ...      % MemRead, MemWrite, IprAccess, InstPrefetch, serialInst
];
[~, colI] = size(pMI);

% System parameters include L2 caches and memory access
paramMatSys = { ...
    'L2CacheHit'; 'L2CacheMissAll'; 'L2CacheMissInst'; 'L2CacheMissData';
    'L2CacheMSHRMissInst'; 'L2CacheMSHRMissPre'; 'L2CacheMSHRMissData';
    'L2CacheMissILatency'; 'L2CacheMissDLatency';
    'PHYMEMRead'; 'PHYMEMWrite'
};

[m,n] = size(paramMatSys);
for j=1:m
    eval(['i_',paramMatSys{j},'=find(not(cellfun(''isempty'',strfind(paramMatSys,''',paramMatSys{j},'''))))+colM+colC+colI;']);
end

pMatSys = [ ...
1, 1, 1, 1, ...         % L2CacheHit, L2CacheMissAll, L2CacheMissInst, L2CacheMissData
1, 1, 1, ...            % L2CacheMSHRMissInst, L2CacheMSHRMissPre, L2CacheMSHRMissData
1, 1, ...               % L2CacheMissInstLatency, L2CacheMissDataLatency
1, 1, ...               % PHYMEMRead, PHYMEMWrite
];

pMat = [pMatMicro, pMICA, pMI, pMatSys];

if STD
    fetchAddrDistAll    = sum(sortedData(:, i_fetchAddrDist0:i_fetchAddrDist11), 2);
    fetchReuseDistAll   = sum(sortedData(:, i_fetchReuseDist0:i_fetchReuseDist10), 2);
    dispatchRateDistAll = sum(sortedData(:, i_dispatchRateDist0:i_dispatchRateDist6), 2);
    lqGlobalDistanceAll = sum(sortedData(:, i_lqGlobalDistance0:i_lqGlobalDistance10), 2);
    lqLocalDistanceAll  = sum(sortedData(:, i_lqLocalDistance0:i_lqLocalDistance10), 2);
    stGlobalDistanceAll = sum(sortedData(:, i_stGlobalDistance0:i_stGlobalDistance10), 2);
    stLocalDistanceAll  = sum(sortedData(:, i_stLocalDistance0:i_stLocalDistance10), 2);
    BasicBlockAll       = sum(sortedData(:, i_BasicBlockSize0:i_BasicBlockSize8), 2);
    depGraphDistAll     = sum(sortedData(:, i_depGraphDist1:i_depGraphDist20), 2);
    branchesAll         = sum(sortedData(:, [i_bwBranches, i_fwBranches, i_notTakenBranches]), 2);
    takenBranchesAll    = sum(sortedData(:, [i_bwBranches,i_fwBranches]), 2);
    cPathLengthROBx1All = sum(sortedData(:, i_cPathLength1:i_cPathLength20), 2);
    pCDRDistAll         = sum(sortedData(:, i_pCDRDist1:i_pCDRDist25), 2);
    serialDrainInstAll  = sum(sortedData(:, i_serialDrainInst0:i_serialDrainInst39), 2);

    sortedData(:, i_fetchAddrDist0:i_fetchAddrDist11)   = sortedData(:, i_fetchAddrDist0:i_fetchAddrDist11) ./ repmat(fetchAddrDistAll,1,12);
    sortedData(:, i_fetchReuseDist0:i_fetchReuseDist10)  = sortedData(:, i_fetchReuseDist0:i_fetchReuseDist10)./ repmat(fetchReuseDistAll,1,11);
    sortedData(:, i_dispatchRateDist0:i_dispatchRateDist6) =sortedData(:, i_dispatchRateDist0:i_dispatchRateDist6)./repmat(dispatchRateDistAll,1,7);
    sortedData(:, i_lqGlobalDistance0:i_lqGlobalDistance10) = sortedData(:, i_lqGlobalDistance0:i_lqGlobalDistance10)./repmat(lqGlobalDistanceAll,1,11);
    sortedData(:, i_lqLocalDistance0:i_lqLocalDistance10) = sortedData(:, i_lqLocalDistance0:i_lqLocalDistance10) ./repmat(lqLocalDistanceAll,1,11);
    sortedData(:, i_stGlobalDistance0:i_stGlobalDistance10) = sortedData(:, i_stGlobalDistance0:i_stGlobalDistance10) ./repmat(stGlobalDistanceAll,1,11);
    sortedData(:, i_stLocalDistance0:i_stLocalDistance10) = sortedData(:, i_stLocalDistance0:i_stLocalDistance10) ./repmat(stLocalDistanceAll,1,11);
    sortedData(:, i_BasicBlockSize0:i_BasicBlockSize8) = sortedData(:, i_BasicBlockSize0:i_BasicBlockSize8) ./repmat(BasicBlockAll,1,9);
    sortedData(:, i_depGraphDist1:i_depGraphDist20) = sortedData(:, i_depGraphDist1:i_depGraphDist20)./repmat(depGraphDistAll,1,20);
    sortedData(:, [i_bwBranches, i_fwBranches]) = sortedData(:, [i_bwBranches, i_fwBranches]) ./repmat(takenBranchesAll,1,2);
    sortedData(:, [i_takenChanged]) = sortedData(:, [i_takenChanged]) ./repmat(branchesAll,1,1);
    sortedData(:, i_cPathLength1:i_cPathLength20) = sortedData(:, i_cPathLength1:i_cPathLength20)./repmat(cPathLengthROBx1All,1,20);
    sortedData(:, i_pCDRDist1:i_pCDRDist25) = sortedData(:, i_pCDRDist1:i_pCDRDist25) ./repmat(pCDRDistAll,1,25);
    sortedData(:, i_serialDrainInst0:i_serialDrainInst39) = sortedData(:, i_serialDrainInst0:i_serialDrainInst39) ./ repmat(serialDrainInstAll,1,40);
end

% Split the raw data into different sMatsI, according to Thread's pid num.
threadContextSwitchNums = [];
for i = 1 : (rowI-1)
    threadContextSwitchNums(i,1:2) = [I(i+1)-I(i), sortedData(I(i), i_PID)];
end
threadContextSwitchNums(end+1,1:2) = [I(end)-I(i), sortedData(I(end), i_PID)];
sortedContextSwitch = sortrows(threadContextSwitchNums, [-1]);
selectedThread = sortedContextSwitch(1:sampleThread, :);

% Process the data from selected threads one by one
globalTag = 0;
for ii = 1:(rowI-1)
    if(find(I(ii+1)-I(ii) == selectedThread(:,1)))
        globalTag = globalTag + 1;
        sMatsI_raw = sortedData(I(ii):I(ii+1),:);                                       % Thread's matrix 

        % every plotNums points, plot
        [rowsMatsI, colsMatsI] = size(sMatsI_raw);

        if sRate == 1
            sMatsI = sMatsI_raw;
        else
            for tt = 0 : rowsMatsI/sRate-1
                sMatsI(tt+1, :) = sum(sMatsI_raw(tt*sRate+1:(tt+1)*sRate, :))./sRate;   % sMatsI: sampled Data
            end
        end

        PID                 = sMatsI(1, end-3);
        Duration            = sMatsI(:, end);
        [~, i_Duration]     = size(sMatsI);
        i_Ticks             = i_Duration - 1;

        fprintf('\n');
        % Declare the CPU Hardware Microarchitectural Parameters
        pFEDp = 6;                                                                      % Frontend Pipeline depth 
        pFEWd = 3;                                                                      % Backend Pipeline width
        pCDR = 10; MLP = 6; avgMem = 100;                                               % Estimated params

        fprintf(strcat('The Analytical Model, processing Thread: 1-', num2str(globalTag), '\n'));


        eval(['sMatsI_', num2str(globalTag),'=sMatsI;']);
        if (globalTag == 4)
            allsRateSMatsI = [sMatsI_1; sMatsI_2; sMatsI_3; sMatsI_4];
            %figure();
            %for tt = 1:4
            %subplot(1,4,tt);
            %eval(['sMatsI_',num2str(tt),'_SerialStall = sMatsI_',num2str(tt),'(:,i_SerialStall);']);
            %eval(['sMatsI_',num2str(tt),'_StructStall = sMatsI_',num2str(tt),'(:,i_RenameROBFull) + sMatsI_',num2str(tt),'(:,i_IQFull) + sMatsI_',num2str(tt),'(:,i_LQFull) + sMatsI_',num2str(tt),'(:,i_SQFull);']);
            %eval(['sMatsI_',num2str(tt),'_Others = sMatsI_',num2str(tt),'(:,i_NumCycles) - sMatsI_',num2str(tt),'(:,i_SerialStall) - sMatsI_',num2str(tt),'(:,i_RenameROBFull) - sMatsI_',num2str(tt),'(:,i_IQFull) - sMatsI_',num2str(tt),'(:,i_LQFull) - sMatsI_',num2str(tt),'(:,i_SQFull);']);

            %eval(['sMatsI_',num2str(tt),'_All = [sMatsI_',num2str(tt),'_SerialStall, sMatsI_',num2str(tt),'_StructStall, sMatsI_',num2str(tt),'_Others];']);
            %eval(['bar(sMatsI_',num2str(tt),'_All, ''stacked'');']);
            %eval(['xlswrite(strcat(FileName, ''_cycleBreakDown_',num2str(tt),'.xls''), sMatsI_',num2str(tt),'_All);']);
            %end

            sMatsI_All = [sMatsI_1_All]; 
            save(strcat(FileName, '_cycleBreakDown'), 'sMatsI_All');

            cPathSum = allsRateSMatsI(:, i_cPathLength4:i_cPathLength10);
            save(strcat(FileName, '_cpath.mat'), 'cPathSum');
        end

        % sMatsI_1_SerialStall = sMatsI_1(:,i_SerialStall);
        eval(['sMatsI_', num2str(globalTag),'_SerialStall = sMatsI_', num2str(globalTag),'(:,i_SerialStall);']);
        % sMatsI_1_StructStall = sMatsI_1(:,i_RenameROBFull)+sMatsI_1(:,i_IQFull)+sMatsI_1(:,i_LQFull)_sMatsI_1(:,SQFull);
        eval(['sMatsI_', num2str(globalTag),'_StructStall = sMatsI_', num2str(globalTag), ...
            '(:,i_RenameROBFull) + sMatsI_', num2str(globalTag),'(:,i_IQFull) + sMatsI_', num2str(globalTag),...
            '(:,i_LQFull) + sMatsI_', num2str(globalTag),'(:,i_SQFull);']);
        % sMatsI_1_Others = sMatsI_1(:,iNumCycles)-sMatsI_1(:,i_SerialStall)-sMatsI_1_StructStall;
        eval(['sMatsI_', num2str(globalTag),'_Others = sMatsI_', num2str(globalTag),'(:,i_NumCycles) - sMatsI_', ...
            num2str(globalTag),'(:,i_SerialStall) - sMatsI_',num2str(globalTag),'_StructStall;']);
        % sMatsI_1_All = [sMatsI_1_SerialStall, sMatsI_1_StructStall, sMatsI_1_Others];
        eval(['sMatsI_', num2str(globalTag),'_All = [sMatsI_', num2str(globalTag),'_SerialStall, sMatsI_',...
            num2str(globalTag),'_StructStall, sMatsI_', num2str(globalTag),'_Others];']);

        sMatsI_All = [sMatsI_1_All]; 
        save(strcat(FileName, '_cycleBreakDown'), 'sMatsI_All');
        cPathSum = sMatsI_1(:, i_cPathLength4:i_cPathLength10);
        save(strcat(FileName, '_cpath.mat'), 'cPathSum');
        pCDRSum  = sMatsI(:, i_pCDRDist1:i_pCDRDist9);
        save(strcat(FileName, '_pcdr.mat'), 'pCDRSum');
        fprintf('Step 0: save data for evaluating multiply Android applications\n');

        % Calculate the inst. mix
        iNum     = sum(sMatsI(:,i_No_OpClass : i_InstPrefetch),  2);                    % All Inst.
        iNumALU  = sum(sMatsI(:,i_IntAlu     : i_IntDiv),        2);                    % ALU Inst.
        iNumFP   = sum(sMatsI(:,i_FloatAdd   : i_FloatSqrt),     2);                    % FP Inst.
        iNumSIMD = sum(sMatsI(:,i_SimdAdd    : i_SimdFloatSqrt), 2);                    % SIMD Inst.
        iNumMEM  = sum(sMatsI(:,i_MemRead    : i_MemWrite),      2);                    % MEM Inst.
        iNumNOP  = sMatsI(:,i_No_OpClass);                                              % NOP Inst.
        iNumPRE  = sMatsI(:,i_InstPrefetch);                                            % PREFETCH Inst.
        iNumIPR  = sMatsI(:,i_IprAccess);                                               % IPRAcess Inst.
        instMix = [iNumALU, iNumFP, iNumSIMD, iNumMEM, iNumNOP, iNumPRE, iNumIPR];

        h1 = figure();
        bar(instMix,  0.5, 'stacked');
        legend('iNumALU', 'iNumFP', 'iNumSIMD', 'iNumMEM', 'iNumNOP', 'iNumPRE', 'iNumIPR');
        fprintf('Step 1: The Inst.Mix has been calculated\n');

        % Construct TOCS09Data dataSet
        TOCS09Data = [];
        TOCS09Data(:,end+1) = sum(sMatsI(:,i_No_OpClass:i_serialInst)*pMI',2)/pFEWd;    % Base1: weighted    
        TOCS09Data(:,end+1) = sMatsI(:, i_L1ICacheMiss) .* pMat(i_L1ICacheMiss);        % L1ICache miss penalty
        TOCS09Data(:,end+1) = sMatsI(:, i_ITLBMiss) .* pMat(i_ITLBMiss);                % L1ITLB miss penalty
        TOCS09Data(:,end+1) = sMatsI(:, i_DTLBMiss) .* pMat(i_DTLBMiss);                % D1ITLB miss penalty

        TOCS09Data(:,end+1) = (pFEWd-1)/2/pFEWd .*(sMatsI(:,i_L1ICacheMiss) ...
            + sMatsI(:, i_L2CacheMissData) ...
            + sMatsI(:, i_BrMiss) ...
            + sMatsI(:, i_L2CacheMissInst) / MLP);                                          % Base2：dispatch ineff
        TOCS09Data(:,end+1) = sMatsI(:, i_L2CacheMissInst)*avgMem/MLP;                  % L2ICache miss penalty
        TOCS09Data(:,end+1) = sMatsI(:, i_L2CacheMissData)*avgMem/MLP;                  % L2DCache miss penalty
        TOCS09Data(:,end+1) = sMatsI(:, i_BrMiss).*(pCDR+pFEDp);                        % Branch miss penalty
        TOCS09Data(:,end+1) = sMatsI(:, i_IdleCycles);                                  % Idle Cycle
        TOCS09Data(:,end+1) = sum(TOCS09Data(:,end-8:end),2);                           % Total Cycles

        % The TOCS09 results
        GEM5_CPI   = sMatsI(:,i_NumCycles)./sMatsI(:, i_CommittedInsts);
        TOCS09_CPI = TOCS09Data(:,end)./sMatsI(:, i_CommittedInsts);
        figure();
        suptitle(strcat('TOCS09, MLP=', num2str(MLP), ';pCDR=', num2str(pCDR), ';avgMem=', num2str(avgMem)));
        subplot(2,1,1);
        bar(TOCS09Data(:,1:end-1), 'stacked');
        subplot(2,1,2);
        plot(GEM5_CPI, 'b'); hold on; plot(TOCS09_CPI, 'r'); title('SIMU(b) vs TOCS09(r)');
        fprintf(strcat('Step 2: Constructing TOCS09Data completed: ', num2str(length(TOCS09Data)), ' entries\n'));


        ISPASS11 = [];
        if ISPASS11_OK
            avgMem_11 = 300;
            % The Regession-based Hybrid Model of ISPASS11
            % pCDR = b1 * (max(40, 1/mpubr)).^b2 * (1+b3*fp) * (1+b4*mpuDL1)
            pCDR_11 = ['b(1).*(max(40,1/(sMatsI(:,i_BrMiss)./iNum))).^b(2)*(1+b(3).*iNumFP./iNum)*' ...
            '(1+b(4).*sMatsI(:,i_L1DCacheMiss)./iNum)'];
            % MLP = b5 * (mpuDL2).^b6 * (mpuDTLB).^b7
            MLP_11 = '(b(5).*((sMatsI(:,i_L2CacheMissData)./iNum).^b(6)).*((sMatsI(:,i_DTLBMiss)./iNum).^b(7)))';
            % cStall = max(0, 1-cMiss./(iNum/D)+b8*(1+b9*fp)*(1+b10*mpuDL1))
            cM = '100';
            cStall_t = '(b(8).*(1+b(9).*iNumFP./iNum).*(1+b(10).*(sMatsI(:,i_L2CacheHit)./iNum)))';
            cStall =strcat('(max(0,1-',cM,'./(iNum/pFEDp+',cStall_t,')).*',cStall_t,')');

            % Use the anno function to avoid multiple m file
            beta0 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]';     % Init the params of b1 ~ b11
            pDispatch=['(pFEWd-1)/2/pFEWd.*((sMatsI(:,i_L1ICacheMiss))+sMatsI(:,i_L2CacheMissInst)'...
            '+sMatsI(:,i_BrMiss)+sMatsI(:, i_L2CacheMSHRMissData)./']; % MLP); % dispatch ineff
            pL2I = 'sMatsI(:,i_L2CacheMissInst).*avgMem_11./'; % MLP;
            pL2D = 'sMatsI(:,i_L2CacheMissData).*avgMem_11./'; % MLP;
            pBr =  'sMatsI(:,i_BrMiss).*(pFEDp+';           % pCDR);
            eval(['[b,r,j,covb,mse] = nlinfit(sMatsI,sMatsI(:,i_NumCycles), @(b,m)', ...
            pDispatch,MLP_11,')+',pL2I,MLP_11,'+',pL2D,MLP_11,'+',pBr,pCDR_11,'+',cStall,')',',beta0);']);
            b; % Show the regresioned b1 ~ b11

            % Calculate the CPI based on Regression Model
            MLP_11 = b(5).*((sMatsI(:,i_L2CacheMissData)./iNum).^b(6)).*((sMatsI(:,i_DTLBMiss)./iNum).^b(7));
            pCDR_11 = b(1).*(max(40,1/(sMatsI(:,i_BrMiss)./iNum))).^b(2)*(1+b(3).*iNumFP./iNum)* ...
            (1+b(4).*sMatsI(:,i_L1DCacheMiss)./iNum);

            ISPASS11 = [];
            %ISPASS11(:,end+1) = sum(sMatsI(:,i_No_OpClass:i_serialInst)*pMI',2)/pFEWd;  % Base1: weighted
            %ISPASS11(:,end+1) = sMatsI(:,i_L1ICacheMiss) * pMat(i_L1ICacheMiss);	    % L1ICache miss penalty
            %ISPASS11(:,end+1) = sMatsI(:,i_ITLBMiss) * avgMem_11./MLP_11;                     % L2ITLB miss penalty
            %ISPASS11(:,end+1) = sMatsI(:,i_DTLBMiss) * avgMem_11./MLP_11;                     % L2DTLB miss penalty

            ISPASS11(:,end+1) = (pFEWd-1)/2/pFEWd .* (sMatsI(:,i_L1ICacheMiss) ...
            + sMatsI(:,i_L2CacheMissInst) ...
            + sMatsI(:,i_BrMiss) ...
            + sMatsI(:, i_L2CacheMissData)./MLP_11);                               % Base2：dispatch ineff
            ISPASS11(:,end+1) = sMatsI(:,i_L2CacheMissData) * avgMem_11./MLP_11;              % L2DCache
            ISPASS11(:,end+1) = sMatsI(:,i_L2CacheMissInst) * avgMem_11./MLP_11;    	            % L2ICache
            ISPASS11(:,end+1) = sMatsI(:,i_BrMiss) .* (pCDR_11 + pFEDp);                   % Branch miss penalty
            ISPASS11(:,end+1) = eval(['(max(0,1-',cM,'./(iNum/pFEDp+',cStall_t,')).*',cStall_t,')']); % StructureStall
            ISPASS11(:,end+1) = sum(ISPASS11, 2);                                       % Total Cycles
            ISPASS11_CPI = ISPASS11(:,end) ./ sMatsI(:, i_CommittedInsts);
        else
            ISPASS11_CPI = [];
        end

        fprintf(strcat('Step 3: Compare the TOCS09/ISSPASS11 with gem5\n'));

        % Setup the useful MICAData for ANNs
        MICAData = sMatsI(:, [ ...
            i_cPathLength1:i_cPathLength12, ... 
            i_serialInst, ...
            i_lqGlobalDistance0:i_lqGlobalDistance10, ...
            i_lqLocalDistance0:i_lqLocalDistance10, ...
            i_stGlobalDistance0:i_stGlobalDistance10, ...
            i_stLocalDistance0:i_stLocalDistance10 ...
        ]);
        % MICAData(:, 13) = MICAData(:, 13) ./ sMatsI(:, i_NumCycles);
        [~,colMICAData] = size(MICAData);
        %targetCol = sum(sMatsI(:, [i_RenameROBFull, i_IQFull, i_SerialStall]), 2) ./ sMatsI(:,i_NumCycles);
        aPCDR = sMatsI(:, i_pCDRDist1:i_pCDRDist9);
        bPCDR = aPCDR.*repmat([1.5,3,6,12,24,48,96,196,384], length(aPCDR),1);
        cPCDR = sum(bPCDR,2)./sMatsI(:,i_BrMiss);
        targetCol = cPCDR;


        if (globalTag == 1)
            eval(['P', num2str(globalTag),' = [MICAData];']);
            eval(['T', num2str(globalTag),' = targetCol;']);
        else
            eval(['P', num2str(globalTag), '= [P',num2str(globalTag-1),'; MICAData];']);
            eval(['T', num2str(globalTag), '= [T',num2str(globalTag-1),'; targetCol];']);
        end
        fprintf(strcat('Step 4: Constructing MICAData completed: ', num2str(colMICAData), ' characters\n'));

        % for ANN Models
        eval(['[Ptrain',num2str(globalTag),'] = (mapminmax(P',num2str(globalTag),''',0,1))'';']);
        eval(['[Ttrain',num2str(globalTag),'] = T',num2str(globalTag),''';']);
        eval(['[Ptest',num2str(globalTag),'] = (mapminmax(P',num2str(globalTag),''',0,1))'';']);
        eval(['[Ttest',num2str(globalTag),'] = T',num2str(globalTag),''';']);

        % SOM clustering and choose the useful testing point
        eval(['x = Ptrain',num2str(globalTag),''';']);
        % Create a Self-Organizing Map
        dimension1 = 200;
        net = selforgmap(dimension1);
        [net,tr] = train(net,x);        % Train the Network
        y = net(x);                     % Test the Network
        sumofY = sum(y,2);
        % clustering the input entries
        z = [];
        for i = 1:length(y)
            z(:,i) = find(y(:,i)==1);
        end
        fprintf(strcat('Step 5: Clusting the sampling entries into:', num2str(dimension1), ' completed\n'));

        % Select the most representative sampling points 
        eval(['[rowTotal, ~]=size(Ptrain', num2str(globalTag), ');']);
        rateOfTrain = [];
        % Note: Ptrain1selected() = [];
        eval(['Ptrain',num2str(globalTag),'selected = [];']);
        eval(['Ttrain',num2str(globalTag),'selected = [];']);

        for k = 1:dimension1
            % Note: Ptrain1_1s = Ptrain1(find(z==1),:);
            eval(['Ptrain', num2str(globalTag), '_', num2str(k), 's=Ptrain',num2str(globalTag),'(find(z==',num2str(k),'),:);']);
            eval(['Ttrain', num2str(globalTag), '_', num2str(k), 's=Ttrain',num2str(globalTag),'(:,find(z==',num2str(k),'));']);
            % Note: [rowTrain1_1,~] = size(Ptrain1_1s);
            eval(['[rowTrain', num2str(globalTag), '_', num2str(k),', ~] = size(Ptrain',num2str(globalTag), '_',num2str(k),'s);']);
            eval(['rowRate = rowTrain', num2str(globalTag), '_' num2str(k), '/rowTotal;']);

            % Find "selectPoints" entries acroding to the centroid of kmeans
            if (eval(['rowTrain', num2str(globalTag), '_', num2str(k), '> 1']))

                % Note: [Centroid, C, sumd, D] = kmeans(Ptrain1_1s,1);
                eval(['[Centroid,C,sumd,D] = kmeans(Ptrain', num2str(globalTag), '_', num2str(k), 's, 1);']);
                [W, DI] = min(D);
                % Note: Ptrain1_1sed = Ptrain1_1s(DI,:);
                eval(['Ptrain', num2str(globalTag), '_', num2str(k), 'sed = Ptrain', num2str(globalTag), '_', ...
                num2str(k), 's(DI, :);']);
                eval(['Ttrain', num2str(globalTag), '_', num2str(k), 'sed = Ttrain', num2str(globalTag), '_', ...
                num2str(k), 's(:, DI);']);      
                %figure();               % for center of every cluster
                %subplot(10,10,k); hist(W); hold on; title(strcat('Cluster: ', num2str(k)));

                % Note: Ptrain1selected = [Ptrain1selected; Ptrain1_1sed];
                eval(['Ptrain', num2str(globalTag), 'selected = [Ptrain',num2str(globalTag),'selected;Ptrain', ...
                num2str(globalTag), '_', num2str(k),'sed];']);
                eval(['Ttrain', num2str(globalTag), 'selected = [Ttrain',num2str(globalTag),'selected,Ttrain', ...
                num2str(globalTag), '_', num2str(k),'sed];']);
            elseif(eval(['rowTrain', num2str(globalTag), '_', num2str(k), '== 1']))
                eval(['Ptrain', num2str(globalTag), '_', num2str(k), 'sed = Ptrain', num2str(globalTag), '_', ...
                num2str(k), 's;']);
                eval(['Ttrain', num2str(globalTag), '_', num2str(k), 'sed = Ttrain', num2str(globalTag), '_', ...
                num2str(k), 's;']);
                % Note: Ptrain1selected = [Ptrain1selected; Ptrain1_1sed];
                eval(['Ptrain', num2str(globalTag), 'selected = [Ptrain',num2str(globalTag),'selected;Ptrain', ...
                num2str(globalTag), '_', num2str(k),'sed];']);
                eval(['Ttrain', num2str(globalTag), 'selected = [Ttrain',num2str(globalTag),'selected,Ttrain', ...
                num2str(globalTag), '_', num2str(k),'sed];']);   
            end
        end

        fprintf(strcat('Step 6: Select representative points, total:', eval(['num2str(size(Ptrain', num2str(globalTag), ...
        'selected, 1))']), ' entries\n'));

        % Construct the ANN for 1/1+2/1+2+3/1+2+3+4/... threads
        % Note: bpnet = newff(Ptrain1selected', Train1selected, [15, 10], {'logsig', 'logsig', 'purelin'}, 'trainlm');
        eval(['bpnet = newff(Ptrain',num2str(globalTag),'selected'', Ttrain',num2str(globalTag), 'selected', ...
        ', [15 10], {''logsig'', ''logsig'', ''purelin''}, ''trainlm'');']);
        bpnet.trainParam.epochs=2000;                                           % Set the max training step
        bpnet.trainParam.goal=1e-6;                                             % Set the target error
        bpnet.trainParam.show=100;                                              % Show results in xx step
        bpnet.trainParam.lr=0.05;                                               % Set the learning params
        bpnet.trainParam.max_fail = 20;
        bpnet.divideFcn = '';
        % Train the ANN
        t1=clock;
        % Note: [bpnet1] = train(bpnet, Ptrain1', Train1);
        eval(['[bpnet',num2str(globalTag),'] = train(bpnet, Ptrain',num2str(globalTag),'selected'', Ttrain',num2str(globalTag),'selected);']);
        ExecutionTime = etime(clock, t1);
        fprintf(strcat('Step 7: Train ANN spend: ', num2str(ExecutionTime), 's\n'));

        if (globalTag == sampleThread)
            for j = 1:sampleThread
                h = figure();
                eval(['suptitle(strcat(''Trained by Thread 1-'',','num2str(j)','));']);
                % set(h,'Position',[400,100,1400,1000]);
                for i = 1:globalTag
                    subplot(2,2,i);
                    % Testing the ANN results
                    % Note: result1 = sim(bpnet1, Ptest1);
                    eval(['result',num2str(j),' =sim(bpnet',num2str(j),',Ptest', num2str(i),''');']); 
                    eval(['plot(result',num2str(j),');']);
                    hold on;
                    eval(['plot(Ttest',num2str(i),''', ''r'');']);
                    eval(['title(strcat(''Testing on Thread 1-'',','num2str(i)','));']);
                    % Note: xlabel(strcat('meanErr=', num2str(mean(result1-Ttest1)./Test1))));
                    eval(['xlabel(strcat(''meanErr='', num2str(mean(abs((result',num2str(j),'-Ttest',num2str(i),')./Ttest', ...
                    num2str(i),')))));']);
                    fprintf(strcat('Trained by Thread 1-', num2str(j),'\n'));
                    eval(['meanErr',num2str(i),'=mean(abs((result',num2str(j),'-Ttest',num2str(i),')./Ttest', num2str(i),'))']);
                end
            end
        end

        % Construct ANNData dataSet
        pCDR = 10; MLP = 6; avgMem = 100;                                               % Estimated params
        ANNData = [];
        ANNData(:,end+1) = sum(sMatsI(:,i_No_OpClass:i_serialInst)*pMI',2)/pFEWd;       % Base1: weighted    
        ANNData(:,end+1) = sMatsI(:, i_L1ICacheMiss) .* pMat(i_L1ICacheMiss);           % L1ICache miss penalty
        ANNData(:,end+1) = sMatsI(:, i_ITLBMiss) .* pMat(i_ITLBMiss);                   % L1ITLB miss penalty
        ANNData(:,end+1) = sMatsI(:, i_DTLBMiss) .* pMat(i_DTLBMiss);                   % D1ITLB miss penalty

        ANNData(:,end+1) = (pFEWd-1)/2/pFEWd .*(sMatsI(:,i_L1ICacheMiss) ...
            + sMatsI(:, i_L2CacheMissData) ...
            + sMatsI(:, i_BrMiss) ...
            + sMatsI(:, i_L2CacheMissInst) ./ MLP);                                         % Base2：dispatch ineff
        ANNData(:,end+1) = sMatsI(:, i_L2CacheMissInst)*avgMem./MLP;                    % L2ICache miss penalty
        ANNData(:,end+1) = sMatsI(:, i_L2CacheMissData)*avgMem./MLP;                    % L2DCache miss penalty
        ANNData(:,end+1) = sMatsI(:, i_BrMiss).*(pCDR+pFEDp);                           % Branch miss penalty
        ANNData(:,end+1) = sMatsI(:, i_IdleCycles);                                     % Idle Cycle
        ANNData(:,end+1) = eval(['result',num2str(globalTag),''' .*sMatsI(:, i_NumCycles);']);  % SH+SerialStall
        ANNData(:,end+1) = sum(ANNData,2);                          % Total Cycles

        ANN_CPI = ANNData(:,end)./sMatsI(:, i_CommittedInsts);


        % Compute the error
        tocs09Err = mean(abs(GEM5_CPI-TOCS09_CPI)./GEM5_CPI)*100;
        if ISPASS11_OK
            ispassErr = mean(abs(GEM5_CPI-ISPASS11_CPI)./GEM5_CPI)*100;
        end
        annErr = mean(abs(GEM5_CPI-ANN_CPI)./GEM5_CPI)*100;
        if ISPASS11_OK
            fprintf(strcat(FileName, ':tocs09Err=', num2str(tocs09Err), ',ispassErr=', num2str(ispassErr), ',annErr=', num2str(annErr),'\n'));
        else
            fprintf(strcat(FileName, ':tocs09Err=', num2str(tocs09Err), ',annErr=', num2str(annErr),'\n'));
        end

        figure();
        startp = 30;
        endp   = 200;
        plot(GEM5_CPI(startp:endp), 'b.-'); hold on; 
        plot(TOCS09_CPI(startp:endp), 'k'); hold on;
        if(ISPASS11_OK) 
            plot(ISPASS11_CPI(startp:endp), 'm'); hold on;
        end
        plot(ANN_CPI(startp:endp), 'r*-');
        if(ISPASS11_OK)
            legend('Gem5 Simulation', 'Mechanic-based', 'Regression-based', 'The Proposed Model');
        else
            legend('Gem5 Simulation', 'Mechanic-based', 'The Proposed Model');
        end
        set(gca,'FontName','Times New Roman','FontSize',14);
        ylabel('The Cycle Per Instruction (CPI)');
        xlabel('The Sampling points of BBench')

        fprintf(strcat('Step 8: The Analytical Model''s output\n'));
    end
end

