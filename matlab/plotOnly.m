%% The whole Analytical Model of ARM processor 
clc;
clear all;
close all;

%% Step 1: Load the data
FileNameBB = 'singleCore-a15-mica-bbench-large';
FileNameWPS = 'singleCore-a15-mica-wps';
FileNameAntutu = 'singleCore-a15-mica-antutu';
FileNameWB = 'singleCore-a15-mica-weibo';
FileNameTT = 'singleCore-a15-mica-ttpod-5s';
FileNamePDF = 'singleCore-a15-mica-pdf';
FileNameMAP = 'singleCore-a15-mica-baidumap';

%% Step 2: Plot the CPIStacks
h1=figure();
% BBench CPIStack
divider = 15;
load(strcat(FileNameBB, '_cycleBreakDown.mat'));
accuSampleSMatsI_All = [];
for i = 0:length(sMatsI_All)/divider-1
    accuSampleSMatsI_All(i+1, :) = sum(sMatsI_All(i*divider+1:(i+1)*divider, :));
end
subplot(1,6,1); bar(accuSampleSMatsI_All, 'stacked');
title('BBench'); xlabel('Samples'); ylabel('Cycle Breakdown');

% WPS CPIStack
divider = 30;
load(strcat(FileNameWPS, '_cycleBreakDown.mat'));
accuSampleSMatsI_All = [];
for i = 0:length(sMatsI_All)/divider-1
    accuSampleSMatsI_All(i+1, :) = sum(sMatsI_All(i*divider+1:(i+1)*divider, :));
end
subplot(1,6,2); bar(accuSampleSMatsI_All, 'stacked');
title('WPS'); xlabel('Samples'); ylabel('Cycle Breakdown');

% Weibo CPIStack
divider = 25;
load(strcat(FileNameWB, '_cycleBreakDown.mat'));
accuSampleSMatsI_All = [];
for i = 0:length(sMatsI_All)/divider-1
    accuSampleSMatsI_All(i+1, :) = sum(sMatsI_All(i*divider+1:(i+1)*divider, :));
end
subplot(1,6,3); bar(accuSampleSMatsI_All, 'stacked');
title('Weibo'); xlabel('Samples'); ylabel('Cycle Breakdown');

% TTPod CPIStack
divider = 20;
load(strcat(FileNameTT, '_cycleBreakDown.mat'));
accuSampleSMatsI_All = [];
for i = 0:length(sMatsI_All)/divider-1
    accuSampleSMatsI_All(i+1, :) = sum(sMatsI_All(i*divider+1:(i+1)*divider, :));
end
subplot(1,6,4); bar(accuSampleSMatsI_All, 'stacked');
title('TTPod'); xlabel('Samples'); ylabel('Cycle Breakdown');

% PDF CPIStack
divider = 10;
load(strcat(FileNamePDF, '_cycleBreakDown.mat'));
accuSampleSMatsI_All = [];
for i = 0:length(sMatsI_All)/divider-1
    accuSampleSMatsI_All(i+1, :) = sum(sMatsI_All(i*divider+1:(i+1)*divider, :));
end
subplot(1,6,5); bar(accuSampleSMatsI_All, 'stacked');
title('Adobe'); xlabel('Samples'); ylabel('Cycle Breakdown');

%MAP  CPIStack
divider = 30;
load(strcat(FileNameMAP, '_cycleBreakDown.mat'));
accuSampleSMatsI_All = [];
for i = 0:length(sMatsI_All)/divider-1
    accuSampleSMatsI_All(i+1, :) = sum(sMatsI_All(i*divider+1:(i+1)*divider, :));
end
subplot(1,6,6); bar(accuSampleSMatsI_All, 'stacked');
title('MAP'); xlabel('Samples'); ylabel('Cycle Breakdown');

%% Antutu CPIStack
%load(strcat(FileNameAntutu, '_cycleBreakDown.mat'));
%divider = 100;
%accuSampleSMatsI_All = [];
%for i = 0:length(sMatsI_All)/divider-1
%    accuSampleSMatsI_All(i+1, :) = sum(sMatsI_All(i*divider+1:(i+1)*divider, :));
%end
%subplot(1,7,7); bar(accuSampleSMatsI_All, 'stacked');
%title('Antutu'); xlabel('Samples'); ylabel('Cycle Breakdown');

legend('others', 'Structual Stall', 'Serialization');


%% Step3: The Critical Path Distribution

figure();
% BBench
load(strcat(FileNameBB, '_cpath.mat'));
divider = 15;
cPathSumWPS = [];
for i = 0:length(cPathSum)/divider-1
    cPathSumBB(i+1, :) = sum(cPathSum(i*divider+1:(i+1)*divider, :));
end
subplot(1,6,1); 
cPathSumBB = [cPathSumBB,max(sum(cPathSumBB, 2))-sum(cPathSumBB, 2)];
bar(cPathSumBB, 'stacked');
title('BBench'); xlabel('Samples'); ylabel('Distribution of Criticalpath Length');
%plot(cPathSum * [16; 32; 64; 128; 256; 512; 1024]./sum(cPathSum, 2))
%ylim([0 150])
%legend('2^4', '2^5', '2^6', '2^7', '2^8', '2^9');

% WPS
load(strcat(FileNameWPS, '_cpath.mat'));
divider = 30;
cPathSumWPS = [];
for i = 0:length(cPathSum)/divider-1
    cPathSumWPS(i+1, :) = sum(cPathSum(i*divider+1:(i+1)*divider, :));
end
subplot(1,6,2); 
cPathSumWPS = [cPathSumWPS,max(sum(cPathSumWPS, 2))-sum(cPathSumWPS, 2)];
bar(cPathSumWPS, 'stacked');
title('WPS'); xlabel('Samples'); ylabel('Distribution of Criticalpath Length');

% Weibo
load(strcat(FileNameWB, '_cpath.mat'));
divider = 25;
cPathSumWB = [];
for i = 0:length(cPathSum)/divider-1
    cPathSumWB(i+1, :) = sum(cPathSum(i*divider+1:(i+1)*divider, :));
end
subplot(1,6,3); 
cPathSumWB = [cPathSumWB,max(sum(cPathSumWB, 2))-sum(cPathSumWB, 2)];
bar(cPathSumWB, 'stacked');
title('Weibo'); xlabel('Samples'); ylabel('Distribution of Criticalpath Length');

% TTPod
load(strcat(FileNameTT, '_cpath.mat'));
divider = 20;
cPathSumTT = [];
for i = 0:length(cPathSum)/divider-1
    cPathSumTT(i+1, :) = sum(cPathSum(i*divider+1:(i+1)*divider, :));
end
subplot(1,6,4); 
cPathSumTT = [cPathSumTT,max(sum(cPathSumTT, 2))-sum(cPathSumTT, 2)];
bar(cPathSumTT, 'stacked');
title('TTPod'); xlabel('Samples'); ylabel('Distribution of Criticalpath Length');

% PDF
load(strcat(FileNamePDF, '_cpath.mat'));
divider = 10;
cPathSumPDF = [];
for i = 0:length(cPathSum)/divider-1
    cPathSumPDF(i+1, :) = sum(cPathSum(i*divider+1:(i+1)*divider, :));
end
subplot(1,6,5);
cPathSumPDF = [cPathSumPDF,max(sum(cPathSumPDF, 2))-sum(cPathSumPDF, 2)];
bar(cPathSumPDF, 'stacked');
title('PDF'); xlabel('Samples'); ylabel('Distribution of Criticalpath Length');

% MAP
load(strcat(FileNameMAP, '_cpath.mat'));
divider = 30;
cPathSumMAP = [];
for i = 0:length(cPathSum)/divider-1
    cPathSumMAP(i+1, :) = sum(cPathSum(i*divider+1:(i+1)*divider, :));
end
subplot(1,6,6); 
cPathSumMAP = [cPathSumMAP,max(sum(cPathSumMAP, 2))-sum(cPathSumMAP, 2)];
bar(cPathSumMAP, 'stacked');
title('MAP'); xlabel('Samples'); ylabel('Distribution of Criticalpath Length');

%% Antutu
%load(strcat(FileNameAntutu, '_cpath.mat'));
%divider = 100;
%cPathSumAntutu = [];
%for i = 0:length(cPathSum)/divider-1
%    cPathSumAntutu(i+1, :) = sum(cPathSum(i*divider+1:(i+1)*divider, :));
%end
%subplot(1,7,7);
%cPathSumAntutu = [cPathSumAntutu,max(sum(cPathSumAntutu, 2))-sum(cPathSumAntutu, 2)];
%bar(cPathSumAntutu, 'stacked');
%title('Antutu'); xlabel('Samples'); ylabel('Distribution of Criticalpath Length');

legend('8~16 cycles', '17~32 cycles', '33~64 cycles', '65~128 cycles', '129~256 cycles', '257~512 cycles', 'others');


