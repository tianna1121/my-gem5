#!/bin/sh
########## The name of benchmarks ###########
export BBENCH=0
export BASICMATH=0
export QSORT=0
export PAT=0

########## The coreNum of benchmarks ########
export SINGLE=1
export DUAL=0
export QUAL=0

######### The microArch #####################
export PREFETCH=0
export ROB80=0

######## check the Param ###################
if [ $(($BBENCH + $BASICMATH + $QSORT)) -gt 1 ]; then
	echo "Multiple benchmarks !"
	exit
elif [ $(($SINGLE + $DUAL +$QUAL)) -gt 1 ]; then
	echo "How many CPUs?"
	exit
fi

############# dedup ########################
./m5stats2streamline.py ./o3_config.ini ./ single-a15.apc
cp newStats.txt newStats-singleCore-a15-mica-wpsBranch.txt

############# BBENCH ########################
if [ $BBENCH -eq 1 ]; then
  if [ $SINGLE -eq 1 ]; then
    echo "BBENCH singlecore.a15"
    tar xvf ../m5out.bbench.single.a15.tgz
  elif [ $DUAL -eq 1 ]; then
    echo "BBENCH dualcore.a15"
    tar xvf ../m5out.bbench.dual.a15.tgz
  elif [ $QUAL -eq 1 ]; then
    echo "BBENCH qualcore.a15"
    tar xvf ../m5out.bbench.qual.a15.tgz
  fi
  cd m5out/
  cp ../m5stats2streamline.py .
  cp ../o3_config.ini .
  ./m5stats2streamline.py ./o3_config.ini ./ single-a15.apc
  if [ $SINGLE -eq 1 ]; then
    mv newStats.txt ../newStats-singleCore-a15-bbench.txt
  elif [ $DUAL -eq 1 ]; then
    mv newStats.txt ../newStats-dualCore-a15-bbench.txt
  elif [ $QUAL -eq 1 ]; then
    mv newStats.txt ../newStats-qualCore-a15-bbench.txt
  fi
  cd ..
  rm -rf m5out/
fi

############# BASICMATH ########################
if [ $BASICMATH -eq 1 ]; then
  if [ $SINGLE -eq 1 ]; then
     if [ $PREFETCH -eq 1 ]; then
       echo "BASICMATH singlecore.a15+prefetch"
       tar xvf ../m5out.basicmath.single.a15+prefetch.tgz
     else
       echo "BASICMATH singlecore.a15"
       tar xvf ../m5out.basicmath.single.a15.tgz
     fi
     cd m5out/
     cp ../m5stats2streamline.py .
     cp ../o3_config.ini .
     ./m5stats2streamline.py ./o3_config.ini ./ single-a15.apc
     if [ $PREFETCH -eq 1 ]; then
       mv newStats.txt ../newStats-singleCore-a15+prefetch-basicmath.txt
     else
       mv newStats.txt ../newStats-singleCore-a15-basicmath.txt
     fi
     cd ..
     rm -rf m5out/
  fi
  
  if [ $DUAL -eq 1 ]; then
    echo "BASICMATH dualcore.a15"
    tar xvf ../m5out.basicmath.dual.a15.tgz
    cd m5out/
    cp ../m5stats2streamline.py .
    cp ../o3_config.ini .
    ./m5stats2streamline.py ./o3_config.ini ./ single-a15.apc
    mv newStats.txt ../newStats-dualCore-a15-basicmath.txt
    cd ..
    rm -rf m5out/
  fi
  
  if [ $QUAL -eq 1 ]; then
    echo "BASICMATH qualcore.a15"
    tar xvf ../m5out.basicmath.qual.a15.tgz
    cd m5out/
    cp ../m5stats2streamline.py .
    cp ../o3_config.ini .
    ./m5stats2streamline.py ./o3_config.ini ./ single-a15.apc
    mv newStats.txt ../newStats-qualCore-a15-basicmath.txt
    cd ..
    rm -rf m5out/
  fi
fi


############# QSORT ###############################
if [ $QSORT -eq 1 ]; then
  if [ $SINGLE -eq 1 ]; then
    if [ $PREFETCH -eq 1 ]; then
      echo "QSORT singlecore.a15+prefetch"
      tar xvf ../m5out.qsort.single.a15+prefetch.tgz
    elif [ $ROB80 -eq 1 ]; then
      echo "QSORT singlecore.a15+rob80"
      tar xvf ../m5out.qsort.single.a15+rob80.tgz
    else 
      echo "QSORT singlecore.a15"
      tar xvf ../m5out.qsort.single.a15.tgz
    fi
    cd m5out/
    cp ../m5stats2streamline.py .
    cp ../o3_config.ini .
    ./m5stats2streamline.py ./o3_config.ini ./ single-a15.apc
    if [ $PREFETCH -eq 1 ]; then
      mv newStats.txt ../newStats-singleCore-a15+prefetch-qsort.txt
    elif [ $ROB80 -eq 1 ]; then
      mv newStats.txt ../newStats-singleCore-a15+rob80-qsort.txt
    else 
      mv newStats.txt ../newStats-singleCore-a15-qsort.txt
    fi
    cd ..
    rm -rf m5out/
  fi
fi

############# PAT ###############################
if [ $PAT -eq 1 ]; then
  if [ $SINGLE -eq 1 ]; then
    if [ $PREFETCH -eq 1 ]; then
      echo "PAT singlecore.a15+prefetch"
      tar xvf ../m5out.pat.single.a15+prefetch.tgz
    elif [ $ROB80 -eq 1 ]; then
      echo "PAT singlecore.a15+rob80"
      tar xvf ../m5out.pat.single.a15+rob80.tgz
    else 
      echo "PAT singlecore.a15"
      tar xvf ../m5out.pat.single.a15.tgz
    fi
    cd m5out/
    cp ../m5stats2streamline.py .
    cp ../o3_config.ini .
    ./m5stats2streamline.py ./o3_config.ini ./ single-a15.apc
    if [ $PREFETCH -eq 1 ]; then
      mv newStats.txt ../newStats-singleCore-a15+prefetch-pat.txt
    elif [ $ROB80 -eq 1 ]; then
      mv newStats.txt ../newStats-singleCore-a15+rob80-pat.txt
    else 
      mv newStats.txt ../newStats-singleCore-a15-pat.txt
    fi
    cd ..
    rm -rf m5out/
  fi
fi
