%% test k-means with matlab
clc;
clear all;
close all;

% Input params
MICA            = 1;
ModelName       = 1;
mostExecutedContext = 4;
accuracy        = 1;
ANNBatch        = 1;
bigDataSet      = 1;
if (bigDataSet == 0)
    FileName        = 'singleCore-a15-mica-bbench';
else
    FileName        = 'singleCore-a15-mica-bbench-large';
end

% Define some macroes
DEBUG           = 1;
ANN_ENABLED     = 1;
PLOT_ENABLED    = 1;



% Declear some global vars
pAdjust  = 10000;                                                                   % Exetime<1/pAdjust points
training = 1000;                                                                    % ANN training points
t1=clock;
format shortg;
PreFix = pwd;

% Open simulation output file, and importdata to Matlab workspace
if (~DEBUG)
  FilePath= strcat(PreFix, '/newStats-', FileName, '.mat');
  if (exist(FilePath, 'file'))
      load(FilePath);
  else
      strcat(PreFix, '/newStats-', FileName, '.txt')
      rawData = importdata(strcat(PreFix, '/newStats-', FileName, '.txt'));
      save(FilePath, 'rawData');
  end
else
  strcat(PreFix, '/newStats-', FileName, '.txt');
  rawData = importdata(strcat(PreFix, '/newStats-', FileName, '.txt'));
end

% Raw data processing
[rowRawData, colRawData] = size(rawData);                                           % The size of rawData
i_PID = colRawData - 2;

% Add the duration ticks of consecutive threads into the rawData
durationTicks = 10000;                                                              % First thead exe 1ticks
for ii = 2:rowRawData
    durationTicks(1,ii) = rawData(ii,end) - rawData(ii-1,end);                      % Add the duration ticks
    if(DEBUG)
        if(rawData(ii,end)-rawData(ii-1, end) < 0)
            fprintf('S');
        end
    end
end
rawData(:,end+1) = durationTicks';
if(DEBUG)
    for ii=2:rowRawData
        if (rawData(ii,end-1)-rawData(ii,end) > rawData(ii-1,end-1)) fprintf('O'); end
    end
end

% Sort data based on thread's PIDs
sortedData = sortrows(rawData, [i_PID, i_PID+1]);                                   % Sorting data
[~, I] = unique(sortedData(:, i_PID));                                              % Find the first row 
[rowI, ~] = size(I);

% Declare the CPU Hardware Microarchitectural Parameters
pFEDp = 6;                                                                          % Frontend Pipeline depth 
pFEWd = 3;                                                                          % Backend Pipeline width
pCDR = 20; MLP = 8; avgMem = 400; pSerial = 40;                                     % Estimated params
ROBEntry = 40;

% Microarchitecture parameters, include L1 cache/Branch/TLB/structure hazards
paramMatC = { ...
    'L1ICacheHit'; 'L1ICacheMiss'; 'L1ICMSHRHit'; 'L1ICMSHRMiss';
    'L1DCacheHit'; 'L1DCacheMiss'; 'L1DCMSHRHit'; 'L1DCMSHRMiss';
    'CommittedInsts'; 'CommitSquashedInsts'; 
    'NumCycles'; 'IdleCycles'; 'IPC_Total';
    'BrMiss';
    'ITLBHit'; 'ITLBMiss'; 'DTLBHit'; 'DTLBMiss';
    'CommitLoads'; 'CommitMembars'; 'CommitBranches'; 'CommitFPInst'; 'CommitIntInsts';
    'IntRFReads'; 'IntRFWrites'; 'MISCRFReads'; 'MISCRFWrites';
    'RenameROBFull'; 'IQFull'; 'LQFull'; 'SQFull'; 'FullRegisterEvents'; 'SerialStall'
    };

% FORMAT: i_L1ICacheHit
[m,n] = size(paramMatC);
for j=1:m
    eval(['i_', paramMatC{j}, ...
       '=find(not(cellfun(''isempty'', strfind(paramMatC,''', paramMatC{j},'''))));']);
end

pMatMicro = [ ...
    1, 12, 1, 1, 2, 12, 1, 1, ... % L1ICacheHit, L1ICacheMiss, L1ICMSHRHit, L1ICMSHRMiss, L1DCacheHit, L1DCacheMiss, L1DCMSHRHit, L1DCMSHRMiss
    1, 1, 1, 1, 1, ...      % CommittedInst, CommitSquashedInsts, NumCycles, IdleCycles, IPC_Total
    1, ...                  % BrMiss
    1, 12, 1, 12, ...       % ITLBHit, ITLBMiss, DTLBHit, DTLBMiss
    1, 1, 1, 1, 1, ...      % CommitLoads, CommitMembars, CommitBranches, CommitFPInst, CommitIntInsts
    1, 1, 1, 1, ...         % IntRFReads, IntRFWrites, MiscRFReads, MiscRFWrites
    1, 1, 1, 1, 1, 1, ...      % RenameROBFull, IQFull, LQFull, SQFull, FullRegisterEvents, SerialCycle
    ];
[~, colM] = size(pMatMicro);

%% Generate the paramMatMICA
if (bigDataSet == 0)
    eval(['paramMatMICA={};']);
    for i = 1:12  
        eval(['paramMatMICA(i,1)={''fetchAddrDist', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    for i = 1:19
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''cPathLength', num2str(i), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
else
    eval(['paramMatMICA={};']);
    for i = 1:12  
        eval(['paramMatMICA(i,1)={''fetchAddrDist', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:11
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''fetchReuseDist', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:7
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''dispatchRateDist', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:11
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''lqGlobalDistance', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:11
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''lqLocalDistance', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:11
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''stGlobalDistance', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:11
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''stLocalDistance', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:9
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''BasicBlockSize', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:20
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''depGraphDist', num2str(i), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:1
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''bwBranches', '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:1
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''notTakenBranches', '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:1
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''fwBranches', '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:1
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''takenChanged', '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:20
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''cPathLength', num2str(i), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:25
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''pCDRDist', num2str(i), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
    
    for i = 1:40
        eval(['paramMatMICA(', num2str(i+tRowMICA), ',1)={''serialDrainInst', num2str(i-1), '''};'])
    end
    [tRowMICA, tColMICA] = size(paramMatMICA);
end


%[~, colC] = size(pMatMicro);
[m,n] = size(paramMatMICA);
for j=1:m
    eval(['i_', paramMatMICA{j}, ...
       '=find(not(cellfun(''isempty'', strfind(paramMatMICA,''', paramMatMICA{j},'''))))+colM;']);
end

if (bigDataSet == 0)
    pMICA = [ ...
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
        1, 1, 1, ...
    ];
else
    pMICA = [ ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                            % fetchAddrDist
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                               % fetchReuseDist
    1, 1, 1, 1, 1, 1, 1,  ...                                          % dispatchRateDist
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                               % lqGlobalDistance
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                               % lqLocalDistance
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,...                                % stGlobalDistance
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,...                                % stLocalDistance
    1, 1, 1, 1, 1, 1, 1, 1, 1,  ...                                    % BasicBlockSize
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                                  % depGraphDist
    1,  ...                                                            % bwBranches
    1,  ...                                                            % notTakenBranches
    1,  ...                                                            % fwBranches
    1,  ...                                                            % takenChanged
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                                  % cPathLengthROBx1
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, ...                                                 % pCDRDist
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ...                                  % serialDrainInst
];
end

% Instruction parameters, include the latency of different instruction
paramMatI = { ...
    'No_OpClass'; 'IntAlu'; 'IntMult'; 'IntDiv';
    'FloatAdd'; 'FloatCmp'; 'FloatCvt'; 'FloatMult'; 'FloatDiv'; 'FloatSqrt';
    'SimdAdd'; 'SimdAddAcc'; 'SimdAlu'; 'SimdCmp'; 'SimdCvt'; 'SimdMisc';
    'SimdMult'; 'SimdMultAcc'; 'SimdShift'; 'SimdShiftAcc'; 'SimdSqrt';
    'SimdFloatAdd'; 'SimdFloatAlu'; 'SimdFloatCmp'; 'SimdFloatCvt'; 'SimdFloatDiv';
    'SimdFloatMisc'; 'SimdFloatMult'; 'SimdFloatMultAcc'; 'SimdFloatSqrt';
    'MemRead'; 'MemWrite'; 'IprAccess'; 'InstPrefetch'; 'serialInst'
    };

[~, colC] = size(pMICA);
[m,n] = size(paramMatI);
for j=1:m
    eval(['i_', paramMatI{j}, ...
       '=find(not(cellfun(''isempty'', strfind(paramMatI,''', paramMatI{j},'''))))+colM+colC;']);
end

pMI = [ ...
    1, 1, 3, 12, ...        % No_OpClass, IntAlu, IntMult, IntDiv
    5, 5, 5, 4, 9, 33, ...  % FloatAdd, FloatCmp, FloatCvt, FloatDiv, FloatSqrt
    4, 4, 4, 4, 3, 3, ...   % SimdAdd, SimdAddAcc, SimdAlu, SimdCmp, SimdCvt, SimdMisc
    5, 5, 3, 3, 9, ...      % SimdMult, SimdMultAcc, SimdShift, SimdShiftAcc, SimdSqrt
    5, 5, 3, 3, 3, ...      % SimdFloatAdd, SimdFloatAlu, SimdFloatCmp, SimdFloatCvt, SimdFloatDiv
    3, 3, 3, 9, ...         % SimdFloatMisc, SimdFloatMult, SimdFloatMultAcc, SimdFloatSqrt
    2, 2, 3, 1, 1, ...      % MemRead, MemWrite, IprAccess, InstPrefetch, serialInst
    ];
[~, colI] = size(pMI);

% System parameters include L2 caches and memory access
paramMatSys = { ...
    'L2CacheHit'; 'L2CacheMissAll'; 'L2CacheMissInst'; 'L2CacheMissData';
    'L2CacheMSHRMissInst'; 'L2CacheMSHRMissPre'; 'L2CacheMSHRMissData';
    'L2CacheMissILatency'; 'L2CacheMissDLatency';
    'PHYMEMRead'; 'PHYMEMWrite'
    };

[m,n] = size(paramMatSys);
for j=1:m
    eval(['i_', paramMatSys{j}, ...
       '=find(not(cellfun(''isempty'', strfind(paramMatSys,''', paramMatSys{j},'''))))+colM+colC+colI;']);
end

pMatSys = [ ...
    1, 1, 1, 1, ...         % L2CacheHit, L2CacheMissAll, L2CacheMissInst, L2CacheMissData
    1, 1, 1, ...            % L2CacheMSHRMissInst, L2CacheMSHRMissPre, L2CacheMSHRMissData
    1, 1, ...               % L2CacheMissInstLatency, L2CacheMissDataLatency
    1, 1, ...               % PHYMEMRead, PHYMEMWrite
    ];

pMat = [pMatMicro, pMICA, pMI, pMatSys];

% Split the raw data into different sMatsI, according to thread's pid num.
threadContextSwitchNums = [];
for i = 1 : (rowI-1)
    threadContextSwitchNums(i,1:2) = [I(i+1)-I(i), sortedData(I(i), i_PID)];
end
threadContextSwitchNums(end+1,1:2) = [I(end)-I(i), sortedData(I(end), i_PID)];      % Need fix this

sortedContextSwitch = sortrows(threadContextSwitchNums, [-1]);
selectedThread = sortedContextSwitch(1:mostExecutedContext, :);

globalTag = 0;
for ii = 1:(rowI-1)
if(find(I(ii+1)-I(ii) == selectedThread(:,1)))
    globalTag = globalTag + 1;
    sMatsI = sortedData(I(ii):I(ii+1),:);                                             % Thread's matrix 

    PID                 = sMatsI(1, end-3);
    Duration            = sMatsI(:, end);
    [~, i_Duration]     = size(sMatsI);
    i_Ticks             = i_Duration - 1;
    
    switch ModelName
        case ANNBatch
            fprintf('test ANN Batch\n');
            % Setup the MICAData and target data
            if(bigDataSet == 0)
                MICAData = sMatsI(:, i_cPathLength1:i_cPathLength12);
            else
                MICAData = sMatsI(:, i_fetchAddrDist1:i_serialDrainInst39);
            end
            % targetCol = 'i_RenameROBFull';
            targetCol = 'i_IPC_Total';

            if (globalTag == 1)
                eval(['P', num2str(globalTag),' = [MICAData];']);
                eval(['T', num2str(globalTag),' = sMatsI(:, ', targetCol, ');']);
            else
                eval(['P', num2str(globalTag), '= [P',num2str(globalTag-1),'; MICAData];']);
                eval(['T', num2str(globalTag), '= [T',num2str(globalTag-1),'; sMatsI(:, ',targetCol,')];']);
            end
            
        otherwise
            fprintf('Unknown Model! Check your input params\n');
        end

        % for ANN Models
        eval(['[Ptrain',num2str(globalTag),', sP',num2str(globalTag),'] = mapminmax(P',num2str(globalTag),');']);
        eval(['[Ttrain',num2str(globalTag),', sT',num2str(globalTag),'] = mapminmax(T',num2str(globalTag),''');']);
        eval(['[Ptest',num2str(globalTag),', sP',num2str(globalTag),'] = mapminmax(P',num2str(globalTag),');']);
        eval(['[Ttest',num2str(globalTag),', sT',num2str(globalTag),'] = mapminmax(T',num2str(globalTag),''');']);

        % SOM clustering and choose the useful testing point
        eval(['x = Ptrain',num2str(globalTag),''';']);

        % Create a Self-Organizing Map
        dimension1 = 8;
        net = selforgmap(dimension1);
        [net,tr] = train(net,x);        % Train the Network
        y = net(x);                     % Test the Network
        sumofY = sum(y,2);

        z = [];
        for i = 1:length(y)
            z(:,i) = find(y(:,i)==1);
        end
        % shajiang.... waiting for the cluster center....
        eval(['[rowTotal, ~]=size(Ptrain', num2str(globalTag), ');']);
        for k = 1:dimension1
            % Note: Ptrain1_1s = Ptrain1(find(z==1),:);
            eval(['Ptrain', num2str(globalTag), '_', num2str(k), 's=Ptrain',num2str(globalTag),'(find(z==',num2str(k),'),:);']);
            eval(['Ttrain', num2str(globalTag), '_', num2str(k), 's=Ttrain',num2str(globalTag),'(:,find(z==',num2str(k),'));']);
            % Note: [rowTrain1_1,~] = size(Ptrain1_1s);
            eval(['[rowTrain', num2str(globalTag), '_', num2str(k),', ~] = size(Ptrain',num2str(globalTag), '_',num2str(k),'s);']);
            eval(['rowRate = rowTrain', num2str(globalTag), '_' num2str(k), '/rowTotal;']);
            % Adjust the trainning ratio based on rowRate
            if rowRate >= 0.20
                trainRatio = 0.1;
            elseif (rowRate >= 0.10) && (rowRate < 0.2)
                    trainRatio = 0.3;
            else
                trainRatio = 1.0;
            end
            % Note: Ptrain1_1sed = Ptrain1_1s(1:round(0.7*rowTrain1_1),:);
            eval(['Ptrain', num2str(globalTag),'_',num2str(k),'sed' '=Ptrain', num2str(globalTag), '_', ...
                num2str(k), 's(1:round(trainRatio*rowTrain', num2str(globalTag),'_', num2str(k),'),:);']);
            eval(['Ttrain', num2str(globalTag),'_',num2str(k),'sed' '=Ttrain', num2str(globalTag), '_', ...
                num2str(k), 's(1:round(trainRatio*rowTrain', num2str(globalTag),'_', num2str(k),'));']);
        end
        % Note: Ptrain1selected() = [];
        eval(['Ptrain',num2str(globalTag),'selected = [];']);
        eval(['Ttrain',num2str(globalTag),'selected = [];']);
        for k = 1:dimension1
            % Note: Ptrain1selected = [Ptrain1selected; Ptrain1_1sed];
            eval(['Ptrain', num2str(globalTag), 'selected = [Ptrain',num2str(globalTag),'selected;Ptrain', ...
                num2str(globalTag), '_', num2str(k),'sed];']);
            eval(['Ttrain', num2str(globalTag), 'selected = [Ttrain',num2str(globalTag),'selected,Ttrain', ...
                num2str(globalTag), '_', num2str(k),'sed];']);
        end

        % Construct the ANN for 1/1+2/1+2+3/1+2+3+4/... threads
        % Note: bpnet = newff(Ptrain1selected', Train1selected, [15, 10], {'logsig', 'logsig', 'purelin'}, 'trainlm');
        eval(['bpnet = newff(Ptrain',num2str(globalTag),'selected'', Ttrain',num2str(globalTag), 'selected', ...
            ', [15 10], {''logsig'', ''logsig'', ''purelin''}, ''trainlm'');']);
        bpnet.trainParam.epochs=1000;                                           % Set the max training step
        bpnet.trainParam.goal=3e-5;                                             % Set the target error
        bpnet.trainParam.show=100;                                              % Show results in xx step
        bpnet.trainParam.lr=0.05;                                               % Set the learning params
        bpnet.trainParam.max_fail = 20;
        bpnet.divideFcn = '';
        % Train the ANN
        % Note: [bpnet1] = train(bpnet, Ptrain1', Train1);
        eval(['[bpnet',num2str(globalTag),'] = train(bpnet, Ptrain',num2str(globalTag),''', Ttrain',num2str(globalTag),');']);

        if (globalTag == mostExecutedContext)
            for j = 1:mostExecutedContext
                h = figure();
                eval(['suptitle(strcat(''Trained by Thread 1-'',','num2str(j)','));']);
                set(h,'Position',[400,100,1400,1000]);
                for i = 1:globalTag
                    subplot(2,2,i);
                    eval(['result',num2str(j),' =sim(bpnet',num2str(j),',Ptest', num2str(i),''');']); 
                    eval(['plot(result',num2str(j),');']);
                    hold on;
                    eval(['plot(Ttest',num2str(i),''', ''r'');']);
                    eval(['title(strcat(''Testing on Thread 1-'',','num2str(i)','));']);
                    % Note: xlabel(strcat('meanErr=', num2str(mean(result1-Ttest1)./Test1))));
                    eval(['xlabel(strcat(''meanErr='', num2str(mean(result',num2str(j),'-Ttest',num2str(i),'))));']);
                end
            end
        end
    end
end

