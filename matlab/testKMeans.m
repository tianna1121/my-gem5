%% test k-means with matlab
clc;
clear all;
close all;

% Input params
FileName        = 'singleCore-a15-bbench';
ModelName       = 1;
mostExecutedContext =2;
accuracy        = 1;
startPoints     = 10;
points          = 2000;

KMEANS2D_2C     = 0;
KMEANS2D_3C     = 0;
KMEANS3D_3C     = 0;
CPUPMU          = 0;
PCACLUSTER      = 0;
DATAMINING      = 0;
MECHDATAMINING  = 1;
  ROBAnn        = 0;
  IQAnn         = 0;
  LSQAnn        = 0;
  REGAnn        = 0;
  SERIALAnn     = 0;
  MLPAnn        = 1;

% Define some macroes
DEBUG           = 1;
ANN_ENABLED     = 1;
PLOT_ENABLED    = 1;


% Declear some global vars
pAdjust  = 10000;                                                                   % Exetime<1/pAdjust points
training = 1000;                                                                    % ANN training points
t1=clock;
format shortg;
PreFix = pwd;
sp = startPoints;
ep = startPoints+points;

%set(0, 'defaultUicontrolFontName', 'Tomano');
%set(0, 'defaultUitableFontName',   'Tomano');
%set(0, 'defaultAxesFontName',      'Tomano');
%set(0, 'defaultTextFontName',      'Tomano');
%set(0, 'defaultUipanelFontName',   'Tomano');

% Open simulation output file, and importdata to Matlab workspace
if (~DEBUG)
  FilePath= strcat(PreFix, '/newStats-', FileName, '.mat');
  if (exist(FilePath, 'file'))
      load(FilePath);
  else
      strcat(PreFix, '/newStats-', FileName, '.txt')
      rawData = importdata(strcat(PreFix, '/newStats-', FileName, '.txt'));
      save(FilePath, 'rawData');
  end
else
  strcat(PreFix, '/newStats-', FileName, '.txt');
  rawData = importdata(strcat(PreFix, '/newStats-', FileName, '.txt'));
end

% Raw data processing
[rowRawData, colRawData] = size(rawData);                                           % The size of rawData
i_PID = colRawData - 2;

% Add the duration ticks of consecutive threads into the rawData
durationTicks = 10000;                                                              % First thead exe 1ticks
for ii = 2:rowRawData
    durationTicks(1,ii) = rawData(ii,end) - rawData(ii-1,end);                      % Add the duration ticks
    if(DEBUG)
        if(rawData(ii,end)-rawData(ii-1, end) < 0)
            fprintf('S');
        end
    end
end
rawData(:,end+1) = durationTicks';
if(DEBUG)
    for ii=2:length(rawData)
        if (rawData(ii,end-1)-rawData(ii,end) > rawData(ii-1,end-1)) fprintf('O'); end
    end
end

% Sort data based on thread's PIDs
sortedData = sortrows(rawData, [i_PID, i_PID+1]);                                   % Sorting data
[~, I] = unique(sortedData(:, i_PID));                                              % Find the first row 
[rowI, ~] = size(I);

% Declare the CPU Hardware Microarchitectural Parameters
pFEDp = 6;                                                                          % Frontend Pipeline depth 
pFEWd = 3;                                                                          % Backend Pipeline width
pCDR = 20; MLP = 8; avgMem = 400; pSerial = 40;                                     % Estimated params
ROBEntry = 40;

% Microarchitecture parameters, include L1 cache/Branch/TLB/structure hazards
paramMatC = { ...
    'L1ICacheHit'; 'L1ICacheMiss'; 'L1ICMSHRHit'; 'L1ICMSHRMiss';
    'L1DCacheHit'; 'L1DCacheMiss'; 'L1DCMSHRHit'; 'L1DCMSHRMiss';
    'CommittedInsts'; 'CommitSquashedInsts'; 
    'NumCycles'; 'IdleCycles'; 'IPC_Total';
    'BrMiss';
    'ITLBHit'; 'ITLBMiss'; 'DTLBHit'; 'DTLBMiss';
    'CommitLoads'; 'CommitMembars'; 'CommitBranches'; 'CommitFPInst'; 'CommitIntInsts';
    'IntRFReads'; 'IntRFWrites'; 'MISCRFReads'; 'MISCRFWrites';
    'RenameROBFull'; 'IQFull'; 'LQFull'; 'SQFull'; 'FullRegisterEvents'; 'SerialStall'
    };

% FORMAT: i_L1ICacheHit
[m,n] = size(paramMatC);
for j=1:m
    eval(['i_', paramMatC{j}, ...
       '=find(not(cellfun(''isempty'', strfind(paramMatC,''', paramMatC{j},'''))));']);
end

pMatMicro = [ ...
    1, 12, 1, 1, 2, 12, 1, 1, ... % L1ICacheHit, L1ICacheMiss, L1ICMSHRHit, L1ICMSHRMiss, L1DCacheHit, L1DCacheMiss, L1DCMSHRHit, L1DCMSHRMiss
    1, 1, 1, 1, 1, ...      % CommittedInst, CommitSquashedInsts, NumCycles, IdleCycles, IPC_Total
    1, ...                  % BrMiss
    1, 12, 1, 12, ...       % ITLBHit, ITLBMiss, DTLBHit, DTLBMiss
    1, 1, 1, 1, 1, ...      % CommitLoads, CommitMembars, CommitBranches, CommitFPInst, CommitIntInsts
    1, 1, 1, 1, ...         % IntRFReads, IntRFWrites, MiscRFReads, MiscRFWrites
    1, 1, 1, 1, 1, 1, ...      % RenameROBFull, IQFull, LQFull, SQFull, FullRegisterEvents, SerialCycle
    ];
[~, colM] = size(pMatMicro);

% Instruction parameters, include the latency of different instruction
paramMatI = { ...
    'No_OpClass'; 'IntAlu'; 'IntMult'; 'IntDiv';
    'FloatAdd'; 'FloatCmp'; 'FloatCvt'; 'FloatMult'; 'FloatDiv'; 'FloatSqrt';
    'SimdAdd'; 'SimdAddAcc'; 'SimdAlu'; 'SimdCmp'; 'SimdCvt'; 'SimdMisc';
    'SimdMult'; 'SimdMultAcc'; 'SimdShift'; 'SimdShiftAcc'; 'SimdSqrt';
    'SimdFloatAdd'; 'SimdFloatAlu'; 'SimdFloatCmp'; 'SimdFloatCvt'; 'SimdFloatDiv';
    'SimdFloatMisc'; 'SimdFloatMult'; 'SimdFloatMultAcc'; 'SimdFloatSqrt';
    'MemRead'; 'MemWrite'; 'IprAccess'; 'InstPrefetch'; 'serialInst'
    };

[m,n] = size(paramMatI);
for j=1:m
    eval(['i_', paramMatI{j}, ...
       '=find(not(cellfun(''isempty'', strfind(paramMatI,''', paramMatI{j},'''))))+colM;']);
end

pMI = [ ...
    1, 1, 3, 12, ...        % No_OpClass, IntAlu, IntMult, IntDiv
    5, 5, 5, 4, 9, 33, ...  % FloatAdd, FloatCmp, FloatCvt, FloatDiv, FloatSqrt
    4, 4, 4, 4, 3, 3, ...   % SimdAdd, SimdAddAcc, SimdAlu, SimdCmp, SimdCvt, SimdMisc
    5, 5, 3, 3, 9, ...      % SimdMult, SimdMultAcc, SimdShift, SimdShiftAcc, SimdSqrt
    5, 5, 3, 3, 3, ...      % SimdFloatAdd, SimdFloatAlu, SimdFloatCmp, SimdFloatCvt, SimdFloatDiv
    3, 3, 3, 9, ...         % SimdFloatMisc, SimdFloatMult, SimdFloatMultAcc, SimdFloatSqrt
    2, 2, 3, 1, 1, ...      % MemRead, MemWrite, IprAccess, InstPrefetch, serialInst
    ];
[~, colI] = size(pMI);

% System parameters include L2 caches and memory access
paramMatSys = { ...
    'L2CacheHit'; 'L2CacheMissAll'; 'L2CacheMissInst'; 'L2CacheMissData';
    'L2CacheMSHRMissInst'; 'L2CacheMSHRMissPre'; 'L2CacheMSHRMissData';
    'L2CacheMissILatency'; 'L2CacheMissDLatency';
    'PHYMEMRead'; 'PHYMEMWrite'
    };

[m,n] = size(paramMatSys);
for j=1:m
    eval(['i_', paramMatSys{j}, ...
       '=find(not(cellfun(''isempty'', strfind(paramMatSys,''', paramMatSys{j},'''))))+colM+colI;']);
end

pMatSys = [ ...
    1, 1, 1, 1, ...         % L2CacheHit, L2CacheMissAll, L2CacheMissInst, L2CacheMissData
    1, 1, 1, ...            % L2CacheMSHRMissInst, L2CacheMSHRMissPre, L2CacheMSHRMissData
    1, 1, ...               % L2CacheMissInstLatency, L2CacheMissDataLatency
    1, 1, ...               % PHYMEMRead, PHYMEMWrite
    ];

pMat = [pMatMicro, pMI, pMatSys];

% Split the raw data into different sMatsI, according to thread's pid num.
threadContextSwitchNums = [];
for i = 1 : (rowI-1)
    threadContextSwitchNums(i,1:2) = [I(i+1)-I(i), sortedData(I(i), i_PID)];
end
threadContextSwitchNums(end+1,1:2) = [I(end)-I(i), sortedData(I(end), i_PID)];      % Need fix this

sortedContextSwitch = sortrows(threadContextSwitchNums, [-1]);
selectedThread = sortedContextSwitch(1:mostExecutedContext, :);

for ii = 1:(rowI-1)
if(find(I(ii+1)-I(ii) == selectedThread(:,1)))
    sMatsI = sortedData(I(ii):I(ii+1),:);                                             % Thread's matrix 

    PID                 = sMatsI(1, end-3);
    Duration            = sMatsI(:, end);
    [~, i_Duration]     = size(sMatsI);
    i_Ticks             = i_Duration - 1;

    iNum     = sum(sMatsI(:,i_No_OpClass : i_InstPrefetch),  2);                    % All Inst.
    iNumALU  = sum(sMatsI(:,i_IntAlu     : i_IntDiv),        2);                    % ALU Inst.
    iNumFP   = sum(sMatsI(:,i_FloatAdd   : i_FloatSqrt),     2);                    % FP Inst.
    iNumSIMD = sum(sMatsI(:,i_SimdAdd    : i_SimdFloatSqrt), 2);                    % SIMD Inst.
    iNumMEM  = sum(sMatsI(:,i_MemRead    : i_MemWrite),      2);                    % MEM Inst.
    iNumNOP  = sMatsI(:,i_No_OpClass);                                              % NOP Inst.
    iNumPRE  = sMatsI(:,i_InstPrefetch);                                            % PREFETCH Inst.
    iNumIPR  = sMatsI(:,i_IprAccess);                                               % IPRAcess Inst.

    
    switch ModelName
        case KMEANS2D_2C
            fprintf('KMEANS 2D_2C Model!\n');
            X = [randn(200,2)+ones(200,2); randn(200,2)-ones(200,2)];
            opts = statset('Display','final');
            [cidx, ctrs] = kmeans(X, 2, 'Distance','city', 'Replicates',5, 'Options',opts);
            size(ctrs)
            figure;
            plot(X(:,1),X(:,2), 'r.'); 
            figure;
            plot(   X(cidx==1,1),X(cidx==1,2),'r.', ...
                    X(cidx==2,1),X(cidx==2,2),'b.', ...
                    ctrs(:,1),ctrs(:,2),'kx');

        case KMEANS2D_3C
            fprintf('KMEANS 2D_3C Model!\n');
            X = [randn(200,2)+ones(200,2); randn(200,2)-ones(200,2)];
            opts = statset('Display','final');
            [cidx, ctrs] = kmeans(X, 3, 'Distance','city', 'Replicates',5, 'Options',opts);
            size(ctrs)
            figure;
            plot(X(:,1),X(:,2), 'r.'); 
            figure;
            plot(   X(cidx==1,1),X(cidx==1,2),'r.', ...
                    X(cidx==2,1),X(cidx==2,2),'b.', ...
                    X(cidx==3,1),X(cidx==3,2),'k.', ...
                    ctrs(:,1),ctrs(:,2),'kx');

        case KMEANS3D_3C
            fprintf('KMEANS 3D_3C Model!\n');
            X = [randn(200,3)+ones(200,3); randn(200,3)-ones(200,3)];
            opts = statset('Display','final');
            [cidx, ctrs] = kmeans(X, 3, 'Distance','city', 'Replicates',5, 'Options',opts);
            size(ctrs)
            figure;
            plot3(X(:,1),X(:,2),X(:,3),'r.'); 
            figure;
            plot3(  X(cidx==1,1),X(cidx==1,2),X(cidx==1,3),'r.', ...
                    X(cidx==2,1),X(cidx==2,2),X(cidx==2,3),'b.', ...
                    X(cidx==3,1),X(cidx==3,2),X(cidx==3,3),'k.', ...
                    ctrs(:,1),ctrs(:,2),ctrs(:,3),'kx');

        case CPUPMU
            CN = 8;
            fprintf('CPU PMU Data Model!\n');
            X = sMatsI(:,1:50);
            opts = statset('Display','final');
            [cidx, ctrs] = kmeans(X, CN, 'Distance','city', 'Replicates', 5, 'Options', opts);
            fprintf(strcat('size of X:', num2str(length(X)), '\n'));
            fprintf(strcat('size of ctrs:', num2str(length(ctrs)), '\n'));
            for i=1:CN
                length(find(cidx==i))
            end

        case PCACLUSTER
            fprintf('PCA And Clustering analysis of CPU PMU Data!\n');
            % the PCA analysis
            X = sMatsI(:,1:30);
            stdr = std(X);
            X = X./repmat(stdr,length(X),1);
            [coeffX, scoreX, latentX, tsquaredX, explainedX] = princomp(X);
            latentX1 = 100*(latentX)./sum(latentX);
            pareto(latentX1);
            useful = find(cumsum(latentX1)>95,1,'first');
            fprintf(strcat('Useful PCA Dimention:', num2str(useful), '\n'));
            X = scoreX(:,6);

            % the Clustering processing
            CN = 8;
            opts = statset('Display','final');
            [cidx, ctrs] = kmeans(X, CN, 'Distance','city', 'Replicates', 5, 'Options', opts);
            fprintf(strcat('size of X:', num2str(length(X)), '\n'));
            fprintf(strcat('size of ctrs:', num2str(length(ctrs)), '\n'));
            for i=1:CN
                length(find(cidx==i))
            end

        case DATAMINING
            fprintf('DataMining of CPU PMU Data!\n');
            % data preprocessing
            X = sMatsI(:,i_L1ICacheHit);
            X(:,end+1) = sMatsI(:,i_L1ICacheMiss);
            X(:,end+1) = sMatsI(:,i_L1DCacheHit);
            X(:,end+1) = sMatsI(:,i_L1DCacheMiss);
            X(:,end+1) = sMatsI(:,i_CommittedInsts);
            X(:,end+1) = sMatsI(:,i_BrMiss);
            X(:,end+1) = sMatsI(:,i_ITLBHit);
            X(:,end+1) = sMatsI(:,i_ITLBMiss);
            X(:,end+1) = sMatsI(:,i_DTLBHit);
            X(:,end+1) = sMatsI(:,i_DTLBMiss);
            X(:,end+1) = sMatsI(:,i_CommitLoads);
            X(:,end+1) = sMatsI(:,i_CommitMembars);
            X(:,end+1) = sMatsI(:,i_CommitBranches);
            X(:,end+1) = sMatsI(:,i_CommitFPInst);
            X(:,end+1) = sMatsI(:,i_CommitIntInsts);
            % X(:,end+1) = sMatsI(:,i_L2CacheHit);
            % X(:,end+1) = sMatsI(:,i_L2CacheMissAll);
            % X(:,end+1) = sMatsI(:,i_L2CacheMissILatency);
            % X(:,end+1) = sMatsI(:,i_L2CacheMissDLatency);
            % X = mapminmax(X);
            sizeofX = size(X)

            % the PCA analysis
            stdr = std(X);
            X = X./repmat(stdr,length(X),1);
            [coeffX, scoreX, latentX, tsquaredX, explainedX] = princomp(X);
            latentX1 = 100*(latentX)./sum(latentX);
            pareto(latentX1);
            useful = find(cumsum(latentX1)>95,1,'first');
            fprintf(strcat('Useful PCA Dimention:', num2str(useful), '\n'));
            X = scoreX(:,1:6);

            % the Clustering process
            maxCN = 9;
            clusterID = zeros(maxCN,maxCN);
            for CN=3:maxCN-1
                figure();
                opts = statset('Display','final');
                [cidx, ctrs] = kmeans(X, CN, 'Distance','city', 'Replicates', 5, 'Options', opts);
                for i=1:CN
                    clusterID(CN,i) = length(find(cidx==i));
                    eval(['cluster_',num2str(CN),'_',num2str(i),'=find(cidx==i);']);
                    % subplot(3,3,i);
                    R = 0.9; G=0.1+i/12; B=0.7-i/15;
                    eval(['plot3(X(cluster_',num2str(CN),'_',num2str(i), ',1)', ...
                        ', X(cluster_',num2str(CN),'_',num2str(i),',2)', ...
                        ', X(cluster_',num2str(CN),'_',num2str(i),',3)', ...
                        ',''.'', ''Color'',[',num2str(R),',', num2str(G), ',', num2str(B),']);']);
                    grid on;
                    hold on;
                end

            end
            figure(); 
            bar(sort(clusterID,2),'stacked');

            % the Data Mining process
            P = X;
            Ptest = P(1:200,:);
            T = mapminmax(sMatsI(:,11));
            Ttest = T(1:200,:);

            [~, Pcol] = size(P);
            m=max(max(P)); n=max(max(T));                                           % Find the max P/T
            P=P'/m; T=T'/n;                                                         % Normalize the P/T
            mt=max(max(Ptest)); nt=max(max(Ttest));                                 % Find the max P/T
            Ptest=Ptest'/mt; Ttest=Ttest'/nt;                                       % Normalize the P/T


            pr(1:Pcol,1)=0; pr(1:Pcol,2)=1;                                         % the input params scope
            warning off all                                                         % Turnoff the waring info

            % Construct the Neural Network Model
            bpnet = newff(pr,[20 20 1],{'logsig', 'logsig', 'purelin'}, 'trainlm');
            bpnet.trainParam.epochs     = 10000;                                    % Set max training step
            bpnet.trainParam.goal       = 1e-4;                                     % Set the target error
            bpnet.trainParam.show       = 100;                                      % Show results in xx step
            bpnet.trainParam.lr         = 0.15;                                     % Set the learning params
            %bpnet.trainParam.showWindow = false                                    % Open/Close the graphics
   
            bpnet=train(bpnet,P,T);                                                 % Start training

            r=sim(bpnet,Ptest);                                                     % Simulation Process
            R=r'*nt; Ttest=Ttest'.*nt;                                              % The simulation results

            figure();
            plot(R, 'c'); hold on;
            plot(Ttest, 'r');
            corrCoef_ANN = corrcoef(Ttest, R);
        case MECHDATAMINING
            fprintf('Mech Model with DataMining!\n');
            % 1. construct the mechanics model based on TOCS'09 and idle/serial/shazard
            MLP = 3; avgMem = 400; pCDR = 10;
            X = []; X_TOCS09 = []; X_TOTAL = []; X_Others = [];
            X(:,end+1) = sum(sMatsI(:,i_No_OpClass:i_serialInst)*pMI',2)/pFEWd; % Base1: weighted    
            X(:,end+1) = sMatsI(:, i_L1ICacheMiss) .* pMat(i_L1ICacheMiss);     % L1ICache miss penalty
            X(:,end+1) = sMatsI(:, i_ITLBMiss) .* pMat(i_ITLBMiss);             % L1ITLB miss penalty
            X(:,end+1) = sMatsI(:, i_DTLBMiss) .* pMat(i_DTLBMiss);             % D1ITLB miss penalty
            
            X(:,end+1) = (pFEWd-1)/2/pFEWd .*((sMatsI(:,i_L1ICacheMiss)) ...
                + sMatsI(:, i_L2CacheMissData) ...
                + sMatsI(:, i_BrMiss) ...
                + sMatsI(:, i_L2CacheMissInst) / MLP);                          % Base2：dispatch ineff
            X(:,end+1) = sMatsI(:, i_L2CacheMissInst)*avgMem/MLP;               % L2ICache miss penalty
            X(:,end+1) = sMatsI(:, i_L2CacheMissData)*avgMem/MLP;               % L2DCache miss penalty
            X(:,end+1) = sMatsI(:, i_BrMiss).*(pCDR+pFEDp);                     % Branch miss penalty
            X_TOCS09(:,end+1) = sum(X(:,end-7:end),2);                         % TOCS09 Cycles
            X(:,end+1) = sMatsI(:, i_IdleCycles);                               % Idle Cycle
            X(:,end+1) = sMatsI(:, i_SerialStall);                              % Serial Cycle
            X(:,end+1) = sum(sMatsI(:,i_RenameROBFull:i_SQFull),2);             % Stucture hazards
            X_TOTAL(:,end+1) = sum(X(:,end-10:end),2);                          % TOTAL Cycles
            X_Others = X(:,end-2:end);

            sizeofX = size(X);
            Gem5_CPI = sMatsI(:,i_NumCycles)./iNum;
            TOCS09_CPI = X_TOCS09(:,end)./iNum;
            MECH_CPI = X_TOTAL(:,end)./iNum;
            Others_CPI = X_Others./repmat(iNum,1,3);
            figure();
            subplot(2,1,1);
            plot(Gem5_CPI, 'b'); 
            hold on; plot(MECH_CPI, 'r');
            hold on; plot(TOCS09_CPI, 'k');
            coef = corrcoef(Gem5_CPI,MECH_CPI); coefVal = coef(1,2);
            avgErr = mean((MECH_CPI-Gem5_CPI)./MECH_CPI);
            title(strcat('Simu(b),TOCS09(k),Mech(r),corrcoef=', num2str(coefVal),',avgErr=', num2str(avgErr)));
            subplot(2,1,2);
            bar(Others_CPI, 'stacked');
            % 2. data mining based on pmu data
            % Cortex-A15 Can provide these pmu data: 
            % L1ICacheRefill, L1ITLBRefill, L1DCacheRefill, L1DCacheAccess, L1DTLBRefill
            % InstRetired, ExcTaken, ExcReturn, CIDWriteRetired, BRMissPred, BRPred, CPUCycles
            % MemAccess, L1ICacheAcess, L1DCacheWB, L2DCacheAcess, L2DCacheRefill, L2DCacheWB
            % BusAccess, MemoryError, InstSpecExe, BusCycles, L1DCacheLD, L1DCacheST
            % L1DCacheRefillLD, L1DCacheRefillST, L1DCacheWBVictim, L1DCacheWBClean, L1DCacheInval
            % L1DTLBRefillLD, L1DTLBRefillST, L2DCacheLD, L2DCacheST, L2DCacheRefillLD, L2DCacheRefillST
            % L2DCacheWBVictim, L2DCacheWBClean, L2DCacheInval
            % BusAccessLD, BusAccessST, BusAccessShared, BusAccessNotShared, BusAccessNormal, BusAccessPeriph
            % MemAccessLD, MemAccessST, UnalignedLD, UnalignedST, UnalignedLDST
            % LDREXSpecExe, STREXPassSpecExe, STREXFailSpecExe, LDSpecExe, STSpecExe, LDSTSpecExe,
            % DPSpecExe, DPSpecExe, ASESpecExe, VFPSpecExe, PCWriteSpecExe, BRImmedSpecExe, 
            % BRReturnSpecExe, BRIndirectSpecExe, ISBSpecExe, DSBSpecExe, DMBSpecExe
            % ========== 2.1 ROB Full ==================
            if ROBAnn
                P=[sMatsI(:,i_No_OpClass:i_serialInst)];
                T=sMatsI(:,i_RenameROBFull);
                [~, Pcol] = size(P);
                m=max(max(P)); n=max(max(T));                                           % Find the max P/T
                P=P'/m; T=T'/n;                                                         % Normalize the P/T
                pr(1:Pcol,1)=0; pr(1:Pcol,2)=1;                                         % the input params scope
                warning off all                                                         % Turnoff the waring info            
                
                % Construct the Neural Network Model
                bpnet=newff(pr,[20 20 1],{'logsig', 'logsig', 'purelin'}, 'trainlm');
                bpnet.trainParam.epochs=200;                                          % Set the max training step
                bpnet.trainParam.goal=1e-5;                                             % Set the target error
                bpnet.trainParam.show=100;                                              % Show results in xx step
                bpnet.trainParam.lr=0.05;                                               % Set the learning params
                bpnet.trainParam.showWindow = false;                                    % Open/Close the Graphics
        
                [bpnet]=train(bpnet,P,T);                                               % Start training
                r=sim(bpnet,P); R=r'*n;                                                 % Simulation Process
                % Plot the results
                figure(); plot(R); hold on; plot(sMatsI(:,i_RenameROBFull),'r');
                corrCoef_ROBFull = corrcoef(R, sMatsI(:,i_RenameROBFull))
            end

            % ========== 2.2 IQ Full ==================
            if IQAnn
                P=[sMatsI(:,i_No_OpClass:i_serialInst)];
                T=sMatsI(:,i_IQFull);
                [~, Pcol] = size(P);
                m=max(max(P)); n=max(max(T));                                           % Find the max P/T
                P=P'/m; T=T'/n;                                                         % Normalize the P/T
                pr(1:Pcol,1)=0; pr(1:Pcol,2)=1;                                         % the input params scope
                warning off all                                                         % Turnoff the waring info            
                
                % Construct the Neural Network Model
                bpnet=newff(pr,[20 20 1],{'logsig', 'logsig', 'purelin'}, 'trainlm');
                bpnet.trainParam.epochs=200;                                          % Set the max training step
                bpnet.trainParam.goal=1e-5;                                             % Set the target error
                bpnet.trainParam.show=100;                                              % Show results in xx step
                bpnet.trainParam.lr=0.05;                                               % Set the learning params
                %bpnet.trainParam.showWindow = false;                                    % Open/Close the Graphics
        
                [bpnet]=train(bpnet,P,T);                                               % Start training
                r=sim(bpnet,P); R=r'*n;                                                 % Simulation Process
                % Plot the results
                figure(); plot(R); hold on; plot(sMatsI(:,i_IQFull),'r');
                corrCoef_IQFull = corrcoef(R, sMatsI(:,i_IQFull))
            end

            % ========== 2.3 LSQ Full ==================
            if LSQAnn
                P=[sMatsI(:,i_No_OpClass:i_serialInst)];
                T=sMatsI(:,i_LQFull)+sMatsI(:,i_SQFull);
                [~, Pcol] = size(P);
                m=max(max(P)); n=max(max(T));                                           % Find the max P/T
                P=P'/m; T=T'/n;                                                         % Normalize the P/T
                pr(1:Pcol,1)=0; pr(1:Pcol,2)=1;                                         % the input params scope
                warning off all                                                         % Turnoff the waring info            
                
                % Construct the Neural Network Model
                bpnet=newff(pr,[20 20 1],{'logsig', 'logsig', 'purelin'}, 'trainlm');
                bpnet.trainParam.epochs=200;                                          % Set the max training step
                bpnet.trainParam.goal=1e-5;                                             % Set the target error
                bpnet.trainParam.show=100;                                              % Show results in xx step
                bpnet.trainParam.lr=0.05;                                               % Set the learning params
                %bpnet.trainParam.showWindow = false;                                    % Open/Close the Graphics
        
                [bpnet]=train(bpnet,P,T);                                               % Start training
                r=sim(bpnet,P); R=r'*n;                                                 % Simulation Process
                % Plot the results
                figure(); plot(R); hold on; plot(sMatsI(:,i_LQFull)+sMatsI(:,i_SQFull),'r');
                corrCoef_IQFull = corrcoef(R, sMatsI(:,i_LQFull)+sMatsI(:,i_SQFull))
            end

            % ========== 2.4 REG Full ==================
            if REGAnn
                P=[sMatsI(:,i_No_OpClass:i_serialInst)];
                T=sMatsI(:,i_FullRegisterEvents);
                [~, Pcol] = size(P);
                m=max(max(P)); n=max(max(T));                                           % Find the max P/T
                P=P'/m; T=T'/n;                                                         % Normalize the P/T
                pr(1:Pcol,1)=0; pr(1:Pcol,2)=1;                                         % the input params scope
                warning off all                                                         % Turnoff the waring info            
                
                % Construct the Neural Network Model
                bpnet=newff(pr,[20 20 1],{'logsig', 'logsig', 'purelin'}, 'trainlm');
                bpnet.trainParam.epochs=200;                                          % Set the max training step
                bpnet.trainParam.goal=1e-5;                                             % Set the target error
                bpnet.trainParam.show=100;                                              % Show results in xx step
                bpnet.trainParam.lr=0.05;                                               % Set the learning params
                %bpnet.trainParam.showWindow = false;                                    % Open/Close the Graphics
        
                [bpnet]=train(bpnet,P,T);                                               % Start training
                r=sim(bpnet,P); R=r'*n;                                                 % Simulation Process
                % Plot the results
                figure(); plot(R); hold on; plot(sMatsI(:,i_FullRegisterEvents),'r');
                corrCoef_IQFull = corrcoef(R, sMatsI(:,i_FullRegisterEvents))
            end

            % ========== 2.5 SERIAL Overhead ==================
            if SERIALAnn
                P=[sMatsI(:,i_No_OpClass:i_serialInst)];
                T=sMatsI(:,i_SerialStall);
                [~, Pcol] = size(P);
                m=max(max(P)); n=max(max(T));                                           % Find the max P/T
                P=P'/m; T=T'/n;                                                         % Normalize the P/T
                pr(1:Pcol,1)=0; pr(1:Pcol,2)=1;                                         % the input params scope
                warning off all                                                         % Turnoff the waring info            
                
                % Construct the Neural Network Model
                bpnet=newff(pr,[20 20 1],{'logsig', 'logsig', 'purelin'}, 'trainlm');
                bpnet.trainParam.epochs=200;                                          % Set the max training step
                bpnet.trainParam.goal=1e-5;                                             % Set the target error
                bpnet.trainParam.show=100;                                              % Show results in xx step
                bpnet.trainParam.lr=0.05;                                               % Set the learning params
                %bpnet.trainParam.showWindow = false;                                    % Open/Close the Graphics
        
                [bpnet]=train(bpnet,P,T);                                               % Start training
                r=sim(bpnet,P); R=r'*n;                                                 % Simulation Process
                % Plot the results
                figure(); plot(R); hold on; plot(sMatsI(:,i_SerialStall),'r');
                corrCoef_IQFull = corrcoef(R, sMatsI(:,i_SerialStall))
            end

            % ========== 2.6 MLP ==================
            if MLPAnn
                
               % P=[sMatsI(:,i_No_OpClass:i_serialInst)];
               % T=sMatsI(:,i_SerialStall);
               % [~, Pcol] = size(P);
               % m=max(max(P)); n=max(max(T));                                           % Find the max P/T
               % P=P'/m; T=T'/n;                                                         % Normalize the P/T
               % pr(1:Pcol,1)=0; pr(1:Pcol,2)=1;                                         % the input params scope
               % warning off all                                                         % Turnoff the waring info            
               % 
               % % Construct the Neural Network Model
               % bpnet=newff(pr,[20 20 1],{'logsig', 'logsig', 'purelin'}, 'trainlm');
               % bpnet.trainParam.epochs=200;                                          % Set the max training step
               % bpnet.trainParam.goal=1e-5;                                             % Set the target error
               % bpnet.trainParam.show=100;                                              % Show results in xx step
               % bpnet.trainParam.lr=0.05;                                               % Set the learning params
               % %bpnet.trainParam.showWindow = false;                                    % Open/Close the Graphics
        
               % [bpnet]=train(bpnet,P,T);                                               % Start training
               % r=sim(bpnet,P); R=r'*n;                                                 % Simulation Process
               % % Plot the results
               % figure(); plot(R); hold on; plot(sMatsI(:,i_SerialStall),'r');
               % corrCoef_IQFull = corrcoef(R, sMatsI(:,i_SerialStall))
            end


        otherwise
            fprintf('Unknown Model! Check your input params\n');
        end
    end
end

