clc
clear

I = importdata('timing.txt');
curTick = I.data(:,1);
recvTime = I.data(:,2);
startpoint = 100;
endpoint = 200;
times = 20;
for i=startpoint*times:endpoint*times
    line([curTick(i),recvTime(i)],[i,i]);
end
