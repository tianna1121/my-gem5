#!/bin/sh

#### for test with 3-entry stats.txt
rm -rf statsFiles/

mkdir statsFiles/
cd statsFiles/
ln -s ../stats.txt stats.txt
csplit stats.txt /Begin/ -n2 -s {*} -f stats -b "%05d.log"; rm stats00000.log 
cp ../connect.txt .
cp ../inputXML.xml .

for i in *.log
do
	echo $i
	perl ../m5-mcpat.pl $i connect.txt inputXML.xml
	../hpca2015_release/v1.2/mcpat -print_level 5 -infile xml_out_file.xml > $i
    awk '/Area/ {if($3=="=")print 0;else print $3}' ${i} > temp
    echo "222222222" >> temp
    awk '/Peak Dynamic/ {print $4}' ${i} >> temp
    echo "222222222" >> temp
    awk '/Subthreshold/ {print $4}' ${i} >> temp
    echo "222222222" >> temp
    awk '/Gate Leakage/ {print $4}' ${i} >> temp
    echo "222222222" >> temp
    awk '/Runtime/ {print $4}' ${i} >> temp
    echo "222222222" >> temp
    cat temp |awk -v RS=''  '{gsub("\n"," "); print }' >> ../$1_pData.txt 
done
cd ..

#perl m5-mcpat.pl stats-3entries.txt connect.txt bbench-4-60.out.xml
#hpca2015_release/v1.2/mcpat -infile xml_out_file.xml

#for FileName in 'bbench-3-70' 'bbench-3-80' 'bbench-3-90' 'bbench-3-100' 
#do
#	echo "${FileName} processing... "
#	perl m5-mcpat.pl ../${FileName}/m5out/stats.txt ../${FileName}/m5out/config.ini hpca2015_release/v1.2/ProcessorDescriptionFiles/${FileName}_ARM_A9_2GHz.xml > ${FileName}.out.xml
#	hpca2015_release/v1.2/mcpat -infile ${FileName}.out.xml -print_level 5 > ${FileName}.mcpatout.txt 
#done
#
#mv bbench-3-70.mcpatout.txt BBench330.mcpatout.txt
#mv bbench-3-80.mcpatout.txt BBench330.mcpatout.txt
#mv bbench-3-90.mcpatout.txt BBench330.mcpatout.txt
#mv bbench-3-100.mcpatout.txt BBench330.mcpatout.txt

#for FileName in 'BBench330' 'BBench340' 'BBench350' 'BBench360' 'BBench370' 'BBench380' 'BBench390' 'BBench3100'
#do
#    awk '/Area/ {if($3=="=")print 0;else print $3}' ${FileName}.mcpatout.txt > ${FileName}_nn.txt
#    echo "222222222" >> ${FileName}_nn.txt
#    awk '/Peak Dynamic/ {print $4}' ${FileName}.mcpatout.txt >> ${FileName}_nn.txt
#    echo "222222222" >> ${FileName}_nn.txt
#    awk '/Subthreshold/ {print $4}' ${FileName}.mcpatout.txt >> ${FileName}_nn.txt
#    echo "222222222" >> ${FileName}_nn.txt
#    awk '/Gate Leakage/ {print $4}' ${FileName}.mcpatout.txt >> ${FileName}_nn.txt
#    echo "222222222" >> ${FileName}_nn.txt
#    awk '/Runtime/ {print $4}' ${FileName}.mcpatout.txt >> ${FileName}_nn.txt
#    echo "222222222" >> ${FileName}_nn.txt
#done

