#!/usr/bin/perl

use strict;
use POSIX;

my ($statstxt,$xmlstats,$mcpatxml) = @ARGV;

my $stats = &loadStats($statstxt);
my $xmlstats = &connect($xmlstats);
my $mcpatxml = &loadxml($mcpatxml);

$mcpatxml =~ s/name="(.*?)"\svalue="(.*?)"/'name="'.&remove_prefix($1).'" value="'.&subst($1,$2).'"'/ge;

#print $mcpatxml;
&write_to_file();

sub remove_prefix(){
	my ($e) = @_;
	my @xmlname = split(/\./,$e);
	my $num_name = scalar @xmlname;
	return $xmlname[$num_name-1];
}

sub write_to_file(){
	my ($file) = @_;
    my $result = "";
    open(WORKING,">./xml_out_file.xml") or die "Failed to open xml file $file\n";
	print WORKING "$mcpatxml";
	close(WORKING);
}

sub subst() {
    my ($e) = @_;
    my $f = $e;			#save param to recovery

	#print "@_[1]\t";

	$e = $xmlstats->{$e};	#get stats key in connect according to xml key

	#my $r = eval $e;
    if ($e eq "") {			#recovery if $e is empty
		$e = @_[1];
		#print "$f origin:$e\n";
    }else{
		my $judge_e = $e;
		my $num_minus = "";
		my @statskey = {};
		my $num_key = "";
		my $count = "";

		$judge_e =~ s/-/'#'.$num_minus++/ge;

		#print "num of num_minus is $num_minus\n";
		if($num_minus == 0){
			@statskey = split(/\+/,$e);
			$num_key = scalar @statskey;
			for($count=0; $count<=$num_key; $count++){
				$e += $stats->{$statskey[$count]};	
			}
		}else{
			@statskey = split(/-/,$e);			
			$num_key = scalar @statskey;
			$e = $stats->{$statskey[$count]};	
			for($count=1; $count<=$num_key; $count++){
				$e -= $stats->{$statskey[$count]};	
			}
		}
	}
	#print "$f:$e\n";
	return $e;
}

sub loadxml() {			#get xmltemplate to result, and return it
    my ($file) = @_;
    my $result = "";
    open(WORKING,"<$file") or die "Failed to open xml file $file\n";
    while(my $line = <WORKING>) {
	$result .= $line;
    }
    close(WORKING);
    return $result;
}

sub loadStats() {		#save info geted from statstxt to result, and return it
    my ($result,$file) = ({},@_);
    open(WORKING,"<$file") or die "Failed to open stats file $file\n";
    while(my $line = <WORKING>) {
		chomp($line);
		if ($line =~ /^(\S+)\s+([\d\.\-]+|nan|inf)\s/) {
	    	$result->{$1} = $2;
			#print "$1:\t$2\r\n";
		}
		elsif ($line =~ /(?:Begin|End) Simulation Statistics/) {}
		elsif ($line =~ /^\s*$/) {}
		else {
	    	die "Failed to parse stats $line\n";
		}
    }
    close(WORKING);
    return $result;
}

sub connect(){
	my ($result,$file) = ({},@_);
	open(WORKING,"<$file") or die "Failed to open connect file $file\n";
	while(my $line = <WORKING>){
		chomp($file);
		if($line =~ /^(\S+)\s+(.*?)\s/){
			$result->{$1} = $2;
			#print "$1:\t$2\r\n";
		}
	}
	close(WORKING);
	return $result;
}
