%% main.m
clc
clear all
close all

fPlotOnly       =   0;
fPlotMICA       =   1;
CPIModel        =   0;
pCDRModel       =   0;
DSEModel        =   0;
McPatModel      =   0;
powerCalModel   =   0;

sRate           =   1;
sampleThread    =   4;
Power           =   0;


%files = {'BBench330', 'BBench340', 'BBench350', 'BBench360', 'BBench370', 'BBench380', 'BBench390', 'BBench3100'};
%files = {'BBench360', 'BBench380', 'BBench3100', 'BBench3120', 'BBench3140'};
%files = {'BBench440', 'BBench450', 'BBench460'};
% files = {'BBench340','WPS','TTpod','FFT','BMath','QSORT','BLOWFISHE','BLOWFISHD','DIJK','PATRIC','SHA','SUSANS'};
%files = {'BBench380'};
files = {'BBench340','WPS','TTpod','basicmath_small','bitcnts_small','blowfish_d_small','blowfish_e_small','cjpeg_small','crc','dijkstra_small','djpeg_small','ispell','lout_small','patricia_small','qsort_small','rawcaudio','rawdaudio','rijndael_d_small','rijndael_e_small','say','search_small','sha_small','susan_smoothing_small','tiff2bw_small','tiff2rgba_small','tiffdither_small','tiffmedian_small','toast','untoast'};
%files = {'BMath','WPS','Adobe','TTpod','WeiBo','Bubble'};
%files = {'BBench','WPS','Adobe','TTpod','WeiBo','Bubble'};
%files = {'BBench', 'BBench330', 'BBench350', 'BBench360', 'BBench450', 'WPS', 'Adobe', 'TTpod', 'WeiBo'};

for i = 1:length(files)
    nn = files{i}
    [sMatsI1, sMatsI2, sMatsI3, sMatsI4]  = myPreProcess(nn, sRate, sampleThread, Power);
    
    eval([nn, '_sMatsI1', '=sMatsI1;'])
    eval([nn, '_sMatsI2', '=sMatsI2;'])
    eval([nn, '_sMatsI3', '=sMatsI3;'])
    eval([nn, '_sMatsI4', '=sMatsI4;'])
end

for i = 1:length(files)
    nn = files{i};
    eval(['sMatsI1 =', nn, '_sMatsI1;']); 
    eval(['sMatsI2 =', nn, '_sMatsI2;']); 
    eval(['sMatsI3 =', nn, '_sMatsI3;']); 
    eval(['sMatsI4 =', nn, '_sMatsI4;']); 
    if CPIModel
        CPIModelFunc(sMatsI1, sMatsI2, sMatsI3, sMatsI4, sampleThread);
    elseif pCDRModel
        pCDRModelFunc(sMatsI1, sMatsI2, sMatsI3, sMatsI4, sampleThread);
    elseif DSEModel
        DSEModelFunc(sMatsI1, sMatsI2, sMatsI3, sMatsI4, sampleThread);
    elseif powerCalModel
        powerCal(sMatsI1, sMatsI2, sMatsI3, sMatsI4, sampleThread);

    end
end

%% plotOnly is a script, not a function
if fPlotOnly
    plotOnly
end

if fPlotMICA
    plotMICA
end

if McPatModel
    newDSEModelFunc;
end


