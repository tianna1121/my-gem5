% plotCPIStack.m

function plotCPIStack(sMatsI, FileName, FileNum, maxFile)
defines
[rowMat, colMat] = size(sMatsI);
divider = ceil(rowMat / 20);

accuSampleSMatsI = [];
SerialStall = sMatsI(:,i_SerialStall);
StructStall = sMatsI(:,i_RenameROBFull)+sMatsI(:,i_IQFull)+sMatsI(:,i_LQFull)+sMatsI(:,i_SQFull);
Others      = sMatsI(:,i_NumCycles) -sMatsI(:,i_SerialStall) - StructStall;
AllCycles   = [SerialStall, StructStall, Others];

for i = 0:length(sMatsI)/divider-1
    accuAllCycles(i+1,:) = sum(AllCycles(i*divider+1:(i+1)*divider,:));
end
subplot(1,maxFile,FileNum); bar(accuAllCycles, 'stacked');
title(FileName); xlabel('Samples'); ylabel('Cycle Breakdown'); xlim([0 21]);

