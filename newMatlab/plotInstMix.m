% plotInstMix.m

function plotInstMix(sMatsI, FileName, FileNum, maxFile, sampleThread, ih, sampleNums)
defines
[rowMat, colMat] = size(sMatsI);
divider = ceil(rowMat / sampleNums);

accuSampleSMatsI = [];

instALUSimple       = sum(sMatsI(:,i_No_OpClass :i_IntAlu)          ,2);
instALUComplex      = sum(sMatsI(:,i_IntMult    :i_IntDiv)          ,2);
instFloadSimple     = sum(sMatsI(:,i_FloatAdd   :i_FloatCvt)        ,2);
instFloadComplex    = sum(sMatsI(:,i_FloatMult  :i_FloatSqrt)       ,2);
instSIMD            = sum(sMatsI(:,i_SimdAdd    :i_SimdFloatSqrt)   ,2);
instMem             = sum(sMatsI(:,i_MemRead    :i_MemWrite)        ,2);
instPref            = sMatsI(:,i_InstPrefetch);
instSerial          = sMatsI(:,i_serialInst);

AllInsts = [instALUSimple,instALUComplex,instFloadSimple,instFloadComplex,...
    instSIMD, instMem, instPref, instSerial];

for i = 0:rowMat/divider-1
    accuAllInsts(i+1,:) = sum(AllInsts(i*divider+1:(i+1)*divider,:));
end
subplot(sampleThread, maxFile+1, (ih-1)*(maxFile+1)+FileNum); 
bar(accuAllInsts, 'stacked');
title(FileName); xlabel('Samples'); ylabel('Instruction Mix'); 
xlim([0 sampleNums+1]);



