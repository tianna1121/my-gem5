%% The whole Analytical Model of ARM processor 

%% Step 2: Plot the CPIStacks
maxFiles = length(files);

h1=figure();
for FileNum = 1:maxFiles
    plotCPIStack(eval([files{FileNum},'_sMatsI1']),files{FileNum},FileNum,maxFiles);
end
legend('Serialization', 'Structual Stall', 'Others');

%% Plot the cPathLength
figure();

for FileNum = 1:maxFiles
    plotCriticalPath(eval([files{FileNum},'_sMatsI1']),files{FileNum},FileNum,maxFiles);
end
legend('8~16 cycles', '17~32 cycles', '33~64 cycles', '65~128 cycles', '129~256 cycles', '257~512 cycles', 'others');

if 0
cPathSumWPS = [];
for i = 0:length(cPathSum)/divider-1
    cPathSumBB(i+1, :) = sum(cPathSum(i*divider+1:(i+1)*divider, :));
end
subplot(1,6,1); 
cPathSumBB = [cPathSumBB,max(sum(cPathSumBB, 2))-sum(cPathSumBB, 2)];
bar(cPathSumBB, 'stacked');
title('BBench'); xlabel('Samples'); ylabel('Distribution of Criticalpath Length');
%plot(cPathSum * [16; 32; 64; 128; 256; 512; 1024]./sum(cPathSum, 2))
%ylim([0 150])
%legend('2^4', '2^5', '2^6', '2^7', '2^8', '2^9');
end


