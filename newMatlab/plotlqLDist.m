% plotlqLDist.m

function plotlqLDist(sMatsI, FileName, FileNum, maxFile, sampleThread, ih, sampleNums)
defines
[rowMat, colMat] = size(sMatsI);
divider = ceil(rowMat / sampleNums);

accuSampleSMatsI = [];

cycles0to8   = sum(sMatsI(:, i_lqLDist0 :i_lqLDist3 ),2);

AlllqLDist = [cycles0to8,sMatsI(:, i_lqLDist4:i_lqLDist10)];

for i = 0:rowMat/divider-1
    accuAlllqLDist(i+1,:) = sum(AlllqLDist(i*divider+1:(i+1)*divider,:));
end

subplot(sampleThread, maxFile+1, (ih-1)*(maxFile+1)+FileNum); 
bar(accuAlllqLDist, 'stacked');
title(FileName); xlabel('Samples'); ylabel('ld Local Reused Distance'); 
xlim([0 sampleNums+1]);


