% plotstGDist.m

function plotstGDist(sMatsI, FileName, FileNum, maxFile, sampleThread, ih, sampleNums)
defines
[rowMat, colMat] = size(sMatsI);
divider = ceil(rowMat / sampleNums);

accuSampleSMatsI = [];

cycles0to8   = sum(sMatsI(:, i_stGDist0 :i_stGDist3 ),2);

AllstGDist = [cycles0to8,sMatsI(:, i_stGDist4:i_stGDist10)];

for i = 0:rowMat/divider-1
    accuAllstGDist(i+1,:) = sum(AllstGDist(i*divider+1:(i+1)*divider,:));
end

subplot(sampleThread, maxFile+1, (ih-1)*(maxFile+1)+FileNum); 
bar(accuAllstGDist, 'stacked');
title(FileName); xlabel('Samples'); ylabel('st Global Reused Distance'); 
xlim([0 sampleNums+1]);


