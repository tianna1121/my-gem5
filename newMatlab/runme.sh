#!/bin/sh
#for FileName in "BBench3110" "BBench3120" "BBench3130" "BBench3140" 
#for FileName in "BMath" "BLOWFISHD" "BLOWFISHE" "DIJK" "PATRIC" "QSORT" "SHA" "SUSANS"
#for FileName in "MAP"
#for FileName in "basicmath_small" "bitcnts_small" "blowfish_d_small" "blowfish_e_small" "cjpeg_small" "crc" "dijkstra_small" "djpeg_small" "fft-4-4096" "fft-4-48192-i" "ispell" "lout_small" "patricia_small" "qsort_small" "rawcaudio" "rawdaudio" "rijndael_d_small" "rijndael_e_small" "say" "search_small" "sha_small" "susan_smoothing_small" "tiff2bw_small" "tiff2rgba_small" "tiffdither_small" "tiffmedian_small" "toast" "untoast"
#for FileName in "adpcm_decode" "adpcm_encode" "g721_decode" "g721_encode" "pegwit_d" "pegwit_e" "rasta"
for FileName in "BBench3110"
do
  echo "${FileName} is processing ..."
  rm stats.txt
  rm system.tasks.txt
  ln -s ../${FileName}/m5out/stats.txt .
  ln -s ../${FileName}/m5out/system.tasks.txt .
#  ./m5stats2streamline.py ./o3_config.ini ./ single-a15.apc
#  cp newStats.txt newStats-singleCore-a15-mica-${FileName}.txt
  rm inputXML.xml
  cp hpca2015_release/v1.2/ProcessorDescriptionFiles/${FileName}_ARM_A9_2GHz.xml inputXML.xml
  ./forMcpat.sh ${FileName}
  #perl m5-mcpat.pl ../${FileName}/m5out/stats.txt ../${FileName}/m5out/config.ini hpca2015_release/v1.2/ProcessorDescriptionFiles/${FileName}_ARM_A9_2GHz.xml > ${FileName}.out.xml
  #hpca2015_release/v1.2/mcpat -infile ${FileName}.out.xml -print_level 5 > ${FileName}.mcpatout.txt 
done

#mv newStats-singleCore-a15-mica-bbench-3-70.txt newStats-singleCore-a15-mica-BBench370.txt 
#mv newStats-singleCore-a15-mica-bbench-3-80.txt newStats-singleCore-a15-mica-BBench380.txt 
#mv newStats-singleCore-a15-mica-bbench-3-90.txt newStats-singleCore-a15-mica-BBench390.txt 
#mv newStats-singleCore-a15-mica-bbench-3-100.txt newStats-singleCore-a15-mica-BBench3100.txt 

#mv bbench-3-70.mcpatout.txt BBench370_nn.txt
#mv bbench-3-80.mcpatout.txt BBench380_nn.txt
#mv bbench-3-90.mcpatout.txt BBench390_nn.txt
#mv bbench-3-100.mcpatout.txt BBench3100_nn.txt



