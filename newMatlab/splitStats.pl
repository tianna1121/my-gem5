#语言:perl
#程序test.pl
#用法:test.pl  参数
#其中,参数为你要处理的文件路径
#输出文件都是className.txt,你也可以改后缀名，这个在程序中很容易看见
#!/usr/bin/perl

use strict;

if (!defined $ARGV[0]) {
	print "用法提示:test.pl [处理的文件路径]\n\t\teg.test.pl c:\\a.txt\n";
	exit(-1);
}
if (!-e "$ARGV[0]") {
	print "文件($ARGV[0])不存在!\n";
	exit(-1);
}

my $path = $ARGV[0];
my %Content;

if (!open FILE,"<$path") {
	print "无法打开权限!\n";
	exit(-1);
}
else{
	my $flag = 0;
	my $i;
	my ($temp,@line,$line);
	foreach (<FILE>) {
		chomp;
		$line = $_;
#		$_ = s/,$//;
		next if ($_ =~ /^\s+$/);
		@line = split /,/;

		if ($flag == 0) {
			$temp = $line."\n";
			for ($i = 0; $i <= $#line; $i++){
				#if ($line[$i] =~ /className/i){
				if($line[$i] =~ /(?:Begin) Simulation Statistics/){
					print "++++++++\n";
					$flag = 1;
					last;
				}
			}
		}
		else{
			$Content{$line[$i]} .= $temp.$line."\n";
			$flag = 0;
		}
	}
	close FILE;
}

foreach my $name (keys %Content) {
	if (!open TXT,">$name.txt") {
		print "无法写:$name.txt";
		next;
	}
	else{
		print TXT $Content{$name};
		close TXT;
	}
}

print "succeed!\n";
