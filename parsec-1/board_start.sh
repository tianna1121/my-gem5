#!/bin/bash
export M5_PATH=~/ws/my-gem5/img/
#./gem5.opt configs/example/fs.py  --mem-size=1024MB --kernel=vmlinux.arm.smp.fb.2.6.38.8 --disk-image=arm-ubuntu-natty-headless.img --caches --l1i_size=32kB --l1d_size=32kB --l2_size=1MB --l1d_assoc=2 --l1i_assoc=2 --l2_assoc=16 --l2cache  --num-l2caches=8 --cpu-type=AtomicSimpleCPU -n 1 --frame-capture
#./gem5.opt configs/example/fs.py  --mem-size=1024MB --kernel=vmlinux-gem5-android-dvfs --disk-image=ARMv7a-ICS-Android.SMP.Asimbench-v3.img --caches --l1i_size=32kB --l1d_size=32kB --l2_size=1MB --l1d_assoc=2 --l1i_assoc=2 --l2_assoc=16 --l2cache --l2_size=1MB --num-l2caches=8 --cpu-type=AtomicSimpleCPU -n 1 --machine-type=VExpress_EMM --dtb-filename=vexpress-v2p-ca15-tc1-gem5_dvfs_1cpus.dtb --frame-capture

#./gem5.opt configs/example/fs.py  --mem-size=1024MB --kernel=vmlinux-gem5-android-dvfs --disk-image=aarch32-ubuntu-natty-headless.img --caches --l1i_size=32kB --l1d_size=32kB --l2_size=1MB --l1d_assoc=2 --l1i_assoc=2 --l2_assoc=16 --l2cache --l2_size=1MB --num-l2caches=8 --cpu-type=AtomicSimpleCPU -n 1 --machine-type=VExpress_EMM --dtb-filename=vexpress-v2p-ca15-tc1-gem5_dvfs_1cpus.dtb --frame-capture


#./gem5.opt --debug-flags=O3PipeView,Rename configs/example/fs.py  --mem-size=1024MB --kernel=vmlinux-gem5-android-dvfs --disk-image=aarch32-ubuntu-natty-headless.img --caches --l1i_size=32kB --l1d_size=32kB --l1d_assoc=2 --l1i_assoc=2 --l2_assoc=16 --l2cache --l2_size=128kB --num-l2caches=8 --cpu-type=arm_detailed -n 1 --machine-type=VExpress_EMM --dtb-filename=vexpress-v2p-ca15-tc1-gem5_dvfs_1cpus.dtb --frame-capture --enable-context-switch-stats-dump -r 1
#valgrind --leak-check=yes ./gem5.opt --debug-flags=O3PipeView configs/example/fs.py  --mem-size=1024MB --kernel=vmlinux-gem5-android-dvfs --disk-image=aarch32-ubuntu-natty-headless.img --caches --l1i_size=32kB --l1d_size=32kB --l1d_assoc=2 --l1i_assoc=2 --l2_assoc=16 --l2cache --l2_size=128kB --num-l2caches=8 --cpu-type=arm_detailed -n 1 --machine-type=VExpress_EMM --dtb-filename=vexpress-v2p-ca15-tc1-gem5_dvfs_1cpus.dtb --frame-capture --enable-context-switch-stats-dump -r 1


#./gem5.opt --debug-flags=O3PipeView configs/example/fs.py  --mem-size=1024MB --kernel=vmlinux-gem5-android-dvfs --disk-image=aarch32-ubuntu-natty-headless.img --caches --l1i_size=32kB --l1d_size=32kB --l1d_assoc=2 --l1i_assoc=2 --l2_assoc=16 --l2cache --l2_size=128kB --num-l2caches=8 --cpu-type=arm_detailed -n 1 --machine-type=VExpress_EMM --dtb-filename=vexpress-v2p-ca15-tc1-gem5_dvfs_1cpus.dtb --frame-capture --enable-context-switch-stats-dump -r 1


#### android boot
./gem5.opt configs/example/fs.py  --mem-size=1024MB --kernel=vmlinux-gem5-android-dvfs --disk-image=ARMv7a-ICS-Android.SMP.Asimbench-v3.img --caches --l1i_size=32kB --l1d_size=32kB --l1d_assoc=2 --l1i_assoc=2 --l2_assoc=16 --l2cache --l2_size=128kB --num-l2caches=8 --cpu-type=AtomicSimpleCPU -n 1 --machine-type=VExpress_EMM --dtb-filename=vexpress-v2p-ca15-tc1-gem5_dvfs_1cpus.dtb --frame-capture


#./gem5.opt configs/example/fs.py  --mem-size=1024MB --kernel=vmlinux-gem5-android-dvfs --disk-image=aarch32-ubuntu-natty-headless.img --caches --l1i_size=32kB --l1d_size=32kB --l1d_assoc=2 --l1i_assoc=2 --l2_assoc=16 --l2cache --l2_size=128kB --num-l2caches=8 --cpu-type=arm_detailed -n 1 --machine-type=VExpress_EMM --dtb-filename=vexpress-v2p-ca15-tc1-gem5_dvfs_1cpus.dtb --frame-capture --enable-context-switch-stats-dump -r 1
#./gem5.opt configs/example/fs.py  --mem-size=1024MB --kernel=vmlinux-gem5-android-dvfs --disk-image=ARMv7a-ICS-Android.SMP.Asimbench-v3.img --caches --l1i_size=32kB --l1d_size=32kB --l1d_assoc=2 --l1i_assoc=2 --l2_assoc=16 --l2cache --l2_size=1MB --num-l2caches=8 --cpu-type=arm_detailed -n 1 --machine-type=VExpress_EMM --dtb-filename=vexpress-v2p-ca15-tc1-gem5_dvfs_1cpus.dtb --enable-context-switch-stats-dump --frame-capture -r 1


